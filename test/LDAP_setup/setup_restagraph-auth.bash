# Basic setup script to get Restagraph working as a test instance

# Where the LDAP server is
LDAP_HOST='192.0.2.1'
LDAP_PORT=1389
LDAP_URL="ldap://$LDAP_HOST:$LDAP_PORT"

# The top-level DN, from which everything else extends
BASE_DN='dc=onfire,dc=onice'

# Admin credentials
ADMIN_DN="cn=admin,$BASE_DN"
ADMIN_PASSWD=adminpassword


# Test function to check that it's working at all.
# Also good for inspecting the current state of the database.
#ldapsearch -H $LDAP_URL -x -D $ADMIN_DN -w $ADMIN_PASSWD -b $BASE_DN '(objectclass=*)'

# Create subdomain
ldapmodify -H $LDAP_URL -x -D $ADMIN_DN -w $ADMIN_PASSWD -a -f new_domain.ldif

# Create ou=users under the subdomain
ldapmodify -H $LDAP_URL -x -D $ADMIN_DN -w $ADMIN_PASSWD -a -f new_ou.ldif

# Create the new user
ldapmodify -H $LDAP_URL -x -D $ADMIN_DN -w $ADMIN_PASSWD -a -f new_user.ldif

# Change the new user's password
ldapmodify -H $LDAP_URL -x -D $ADMIN_DN -w $ADMIN_PASSWD -a -f change_passwd.ldif
