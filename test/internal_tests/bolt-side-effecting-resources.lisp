;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Test suite for side-effecting code pertaining mostly to resources.

(in-package #:syscat-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(fiveam:def-suite
  bolt-side-effecting-resources
  :description "Tests for side-effecting code pertaining mostly to resources."
  :in main)

(fiveam:in-suite bolt-side-effecting-resources)

(fiveam:test
  default-resources
  "Test installation of default resources."
  :depends-on 'process-filter
  (let* ((session (neo4cl:establish-bolt-session *bolt-server*))
         ;; Ensure there's a new schema-version to work from
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema into that new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Fetch the schema into memory
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::log-message :info "Installing default resources")
      (syscat::install-default-resources session resourcetype-schema)
      ;; Check that the resources are actually there
      (fiveam:is (equal "/People/ScSystem"
                        (gethash "ScCanonicalpath"
                                 (first
                                   (syscat::get-resources
                                     session
                                     resourcetype-schema
                                     (list "People" "ScSystem"))))))
      (fiveam:is (equal (format nil "/People/~A" *admin-user*)
                        (gethash "ScCanonicalpath"
                                 (first
                                   (syscat::get-resources
                                     session
                                     resourcetype-schema
                                     (list "People" *admin-user*)))))))
    ;; Remove the schema we added for this test
    (syscat::delete-schema-version session schema-version)
    ;; Clean up the session
    (neo4cl:disconnect session)))

(fiveam:test
  resource-exists-p
  "Checks the check for whether a resource is there."
  :depends-on 'install-core-schema
  (let* ((session (neo4cl:establish-bolt-session *bolt-server*))
         ;; Ensure there's a new schema-version to work from
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema into that new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema))
    ;; Does it fail to find a resource that should not be there?
    (fiveam:is (null (syscat::resource-exists-p session '("People" "Lord_Humungous"))))
    ;; Does it correctly find a resource that should be there?
    (fiveam:is (syscat::resource-exists-p session '("People" "ScAdmin")))
    ;; Remove the temporary schema-version
    (syscat::delete-schema-version session schema-version)
    ;; Clean up the session
    (neo4cl:disconnect session)))

(fiveam:test
  resources-basic
  "Basic operations on resources"
  :depends-on 'default-resources
  (let* ((restype "People")
         (uid "Cally")
         (org1 "LeagueOfTelepaths")
         (org2 "Shipmates")
         (invalid-type "interfaces")
         (invalid-uid "eth0")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         ;; Ensure there's a new schema-version to work from
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema into that new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Fetch the schema into memory
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Confirm the resource isn't already present
      (syscat::log-message :info ";TEST Confirm the resource isn't already present")
      (fiveam:is (null (syscat::get-resources session resourcetype-schema (list restype uid))))
      ;; Store the resource
      (syscat::log-message :info ";TEST Store the resource")
      (fiveam:is (equal (syscat::sanitise-uid uid)
                        (syscat::store-resource
                          session
                          resourcetype-schema
                          restype
                          `(("ScUID" . ,uid))
                          *admin-user*)))
      ;; Confirm it's there
      ;; (Test get-resources with mod/3 == 2)
      (syscat::log-message :info ";TEST Confirm the resource is present")
      (let ((result (syscat::get-resources session resourcetype-schema (list restype uid))))
        (fiveam:is (when result (gethash "ScCanonicalpath" (first result))))
        (fiveam:is (when result
                     (equal (format nil "/People/~A" (syscat::sanitise-uid uid))
                            (gethash "ScCanonicalpath" (first result))))))
      ;; Confirm we can't create a duplicate
      (syscat::log-message :info ";TEST Confirm refusal to create duplicate resources")
      (fiveam:signals
        (syscat::integrity-error "Path already exists; refusing to create a duplicate.")
        (syscat::store-resource session
                                    resourcetype-schema
                                    restype
                                    `(("ScUID" . ,uid))
                                    *admin-user*))
      ;; Confirm that there are two people (ScAdmin should be the other)
      ;; (Test get-resources with mod/3 == 1)
      (syscat::log-message :info ";TEST Confirm there are three People")
      (let ((people-list (syscat::get-resources session resourcetype-schema (list restype))))
        (syscat::log-message :info (format nil ";TEST-INFO fetched list of people: ~A" people-list))
        (fiveam:is (equal 3 (length people-list))))
      ;; Confirm that we get all resource at the end of a relationship
      (syscat::log-message :info ";TEST Fetch all resources at the end of a relationship")
      ;; First, create the organisations.
      (syscat::store-resource session resourcetype-schema "Organisations" `(("ScUID" . ,org1)) *admin-user*)
      (syscat::store-resource session resourcetype-schema "Organisations" `(("ScUID" . ,org2)) *admin-user*)
      (fiveam:is (equal (format nil "/Organisations/~A" org1)
                        (gethash "ScCanonicalpath"
                                 (first
                                   (syscat::get-resources
                                     session
                                     resourcetype-schema
                                     (list "Organisations" org1))))))
      (fiveam:is (equal (format nil "/Organisations/~A" org2)
                        (gethash "ScCanonicalpath"
                                 (first
                                   (syscat::get-resources
                                     session
                                     resourcetype-schema
                                     (list "Organisations" org2))))))
      ;; Connect the user to the organisations
      (syscat::log-message :debug ";TEST Connect the user to the orgs")
      (syscat::create-relationship-by-path session
                                               (format nil "/People/~A/MEMBER_OF_ORG" uid)
                                               (format nil "/Organisations/~A" org1)
                                               resourcetype-schema)
      (syscat::create-relationship-by-path session
                                               (format nil "/People/~A/MEMBER_OF_ORG" uid)
                                               (format nil "/Organisations/~A" org2)
                                               resourcetype-schema)
      ;; Confirm we get both orgs back
      (syscat::log-message :debug ";TEST Confirm they're a member of both organisations")
      (fiveam:is (= 2
                    (length (syscat::get-resources
                              session
                              resourcetype-schema
                              (list restype uid "MEMBER_OF_ORG")))))
      ;; Remove the _relationship_ to one of the orgs
      (syscat::log-message :debug ";TEST Detach one of the organisations")
      (fiveam:is (null (syscat::delete-relationship-by-path
                         session
                         resourcetype-schema
                         (format nil "/People/~A/MEMBER_OF_ORG" uid)
                         (format nil "/Organisations/~A" org1))))
      ;; Confirm we now only have one tag there
      (syscat::log-message :debug ";TEST Confirm there's now only one org")
      (fiveam:is (= 1 (length (syscat::get-resources
                                    session
                                    resourcetype-schema
                                    (list restype uid "MEMBER_OF_ORG")))))
      ;; Delete both organisations
      (syscat::log-message :debug ";TEST Delete both organisations")
      (syscat::delete-resource-by-path
        session
        (list "Organisations" org1)
        resourcetype-schema)
      (syscat::delete-resource-by-path
        session
        (list "Organisations" org2)
        resourcetype-schema)
      ;; Delete it
      (syscat::log-message :info ";TEST Delete the resource")
      (fiveam:is (null (syscat::delete-resource-by-path
                         session
                         (list restype uid)
                         resourcetype-schema)))
      ;; Confirm it's gone again
      (syscat::log-message :info ";TEST Confirm the resource is gone")
      (fiveam:is (null (syscat::get-resources
                         session
                         resourcetype-schema
                         (list restype uid))))
      ;; Ensure we can't create an invalid type
      (syscat::log-message :info ";TEST Ensure we can't create an invalid type")
      (fiveam:signals
        (syscat::client-error "No such resourcetype.")
        (syscat::store-resource session
                                    resourcetype-schema
                                    invalid-type
                                    `(("ScUID" . ,invalid-uid))
                                    *admin-user*))
      ;; Remove the schema we added for this test
      ;; Clean up the session
      (neo4cl:disconnect session))))

(fiveam:test
  errors-basic
  :depends-on 'resources-basic
  "Errors that can be triggered just by making a bad request"
  (let* ((session (neo4cl:establish-bolt-session *bolt-server*))
         (resourcetype-schema (car (syscat::fetch-current-schema session)))
         (invalid-resourcetype "IjustMadeThisUpNow")
         (valid-resourcetype "People"))
    ;; Create a resource of an invalid type
    (syscat::log-message :info ";TEST Creating a resource of an invalid type")
    (fiveam:signals (syscat::client-error
                      (format nil "Requested resource type ~A is not valid."
                              invalid-resourcetype))
                    (syscat::store-resource session
                                                resourcetype-schema
                                                invalid-resourcetype
                                                '((:foo . "bar"))
                                                *admin-user*))
    ;; Create a resource of a valid type, but without a UID
    (syscat::log-message :info ";TEST Creating a valid resource without a UID")
    (fiveam:signals (syscat::client-error "UID must be supplied")
                    (syscat::store-resource session
                                                resourcetype-schema
                                                valid-resourcetype
                                                '(("foo" . "bar"))
                                                *admin-user*))
    ;; Create a resource of a valid type, but with a UID that's only whitespace
    (syscat::log-message :info ";TEST Creating a valid resource with an all-whitespace UID")
    (fiveam:signals (syscat::client-error "UID must be supplied")
                    (syscat::store-resource session
                                                resourcetype-schema
                                                valid-resourcetype
                                                '(("ScUID" . " "))
                                                *admin-user*))
    (neo4cl:disconnect session)))

(fiveam:test
  resources-attributes-enums
  :depends-on '(resources-basic schema-versions)
  "Enumerated attributes"
  (let* ((attr1name "state")
         (attr1desc "Whether the port is up or down.")
         (attr1vals '("up" "down"))
         (restype (syscat::make-incoming-rtypes
                    :name "switchport"
                    :attributes (list (syscat::make-incoming-rtype-attrs
                                        (list
                                          :type "varchar"
                                          :NAME attr1name
                                          :DESCRIPTION attr1desc
                                          :VALUES attr1vals)))
                    :dependent nil))
         (uid "eth1")
         (attr1valgood "up")
         (attr1valbad "mal")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Set up the fixtures
    (syscat::log-message :info ";TEST Set up the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Install the additional resourcetype we need for this test
    (syscat::install-subschema-resourcetype session restype schema-version)
    ;; Fetch the schema from the database
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Compare the definition in the resourcetype-schema with what we intended it to be
      (syscat::log-message :info ";TEST Check the schema for the enum attribute")
      (let ((reference (make-instance 'syscat::schema-rtype-attr-varchar
                                      :NAME attr1name
                                      :DESCRIPTION attr1desc
                                      :ATTRVALUES attr1vals))
            (retrieved (elt (syscat::attributes (gethash (syscat::name restype)
                                                             resourcetype-schema)) 0)))
        (fiveam:is (equalp (syscat::name reference) (syscat::name retrieved)))
        (fiveam:is (equalp (syscat::description reference) (syscat::description retrieved)))
        (fiveam:is (equalp (syscat::attrvalues reference) (syscat::attrvalues retrieved))))
      ;; Make sure the test resource doesn't already exist
      (syscat::delete-resource-by-path session
                                           (list (syscat::name restype) uid)
                                           resourcetype-schema)
      ;; Fail to create a resource with an invalid value for the enum
      (syscat::log-message :info ";TEST Fail to create a resource with an invalid attribute")
      (fiveam:signals syscat::client-error
                      (syscat::store-resource session
                                                  resourcetype-schema
                                                  (syscat::name restype)
                                                  `(("ScUID" . ,uid)
                                                    (,attr1name . ,attr1valbad))
                                                  *admin-user*))
      ;; Create a resource with a valid value for the enum
      (syscat::log-message :info ";TEST Create a resource with a valid attribute")
      (fiveam:is
        (equal
          uid
          (syscat::store-resource session
                                      resourcetype-schema
                                      (syscat::name restype)
                                      `(("ScUID" . ,uid) (,attr1name . ,attr1valgood))
                                      *admin-user*)))
      ;; Confirm that we now have this resource with the expected value
      (syscat::log-message :info ";TEST Check that the resource was created")
      (let ((attr-test (first (syscat::get-resources
                                session
                                resourcetype-schema
                                (list (syscat::name restype) uid)
                                :attributes (list attr1name)))))
        (fiveam:is (equal (format nil "/~A/~A" (syscat::name restype) uid)
                          (gethash "ScCanonicalpath" attr-test)))
        (fiveam:is (equal attr1valgood
                          (gethash attr1name attr-test))))
      ;; Remove it again
      (syscat::delete-resource-by-path session
                                           (list (syscat::name restype) uid)
                                           resourcetype-schema)
      ;; Create it without the attribute
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name restype)
                                  `(("ScUID" . ,uid))
                                  *admin-user*)
      ;; Add the attribute
      (let ((this-path (list (syscat::name restype) uid)))
        (fiveam:is (integerp (syscat::add-resource-version
                               session
                               resourcetype-schema
                               this-path
                               `((,attr1name . ,attr1valgood)))))
        ;;Confirm that the attribute is present
        (let ((attr-test (first (syscat::get-resources session
                                                           resourcetype-schema
                                                           this-path
                                                           :attributes (list attr1name )))))
          (fiveam:is (equal attr1valgood
                            (gethash attr1name attr-test))))
        ;; Remove the resource again
        (syscat::delete-resource-by-path session
                                             (list (syscat::name restype) uid)
                                             resourcetype-schema))
        ;; Confirm it's gone
        (fiveam:is (null (syscat::get-resources session
                                                    resourcetype-schema
                                                    (list (syscat::name restype) uid))))
      ;; Remove the fixtures
      (syscat::log-message :info ";TEST Remove the fixtures")
      (syscat::delete-schema-version session schema-version))
    ;; Clean up the db session
    (neo4cl:disconnect session)))

(fiveam:test
  resources-dependent-simple
  :depends-on 'resources-basic
  "Basic operations on dependent resources"
  (let* ((child-type1 (syscat::make-incoming-rtypes :name "Interfaces"
                                                        :dependent t))
         (child2-attr1-name "text")
         (child-type2 (syscat::make-incoming-rtypes
                        :name "Comments"
                        :dependent t
                        :attributes (list (syscat::make-incoming-rtype-attrs
                                            (list :name child2-attr1-name
                                                  :type "text"
                                                  :description "The text of the comment.")))))
         (parent-type (syscat::make-incoming-rtypes :name "Routers"))
         (parent-rel1 (syscat::make-incoming-rels :source-types (list (syscat::name
                                                                            parent-type))
                                                      :name "INTERFACES"
                                                      :reltype "dependent"
                                                      :cardinality "1:many"
                                                      :target-types (list (syscat::name
                                                                            child-type1))))
         (parent-rel2 (syscat::make-incoming-rels :source-types '("any")
                                                      :name "HAS_COMMENT"
                                                      :reltype "dependent"
                                                      :cardinality "1:many"
                                                      :target-types (list (syscat::name
                                                                            child-type2))))
         (parent-uid "trinity")
         (child1-uid "eth0")
         (child2-uid "comment1")
         (child2-attr1-val "This is a comment.")
         (invalid-child-type "routers")
         (invalid-child-uid "whitesands")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Create the fixtures
    (syscat::log-message :info ";TEST Create the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Install the new resourcetypes and relationship
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "resources-dependent-simple"
        :resourcetypes (list parent-type child-type1 child-type2)
        :relationships (list parent-rel1 parent-rel2))
      schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Fail to create a dependent resourcetype in isolation
      (syscat::log-message :info ";TEST Fail to create a dependent resource in isolation.")
      (fiveam:signals syscat::integrity-error
                      (syscat::store-resource session
                                                  resourcetype-schema
                                                  (syscat::name child-type1)
                                                  `(("ScUID" . ,child1-uid))
                                                  *admin-user*))
      ;; Create the parent resource
      (syscat::log-message :info ";TEST Create the parent resource.")
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name parent-type)
                                  `(("ScUID" . ,parent-uid))
                                  *admin-user*)
      ;; Create the dependent resource
      (syscat::log-message :info (format nil ";TEST Create the dependent resource /~A/~A/~A/~A"
                                             (syscat::name parent-type)
                                             parent-uid
                                             (syscat::name parent-rel1)
                                             (syscat::name child-type1)))
      (fiveam:is (equal child1-uid
                        (syscat::store-dependent-resource
                          session
                          resourcetype-schema
                          (list (syscat::name parent-type)
                                parent-uid
                                (syscat::name parent-rel1)
                                (syscat::name child-type1))
                          `(("ScUID" . ,child1-uid))
                          *admin-user*)))
      ;; Confirm it's the only member of the parent's dependents
      (syscat::log-message :info ";TEST confirm this is an only child")
      (fiveam:is (equal `((,(syscat::name parent-rel1)
                            ,(syscat::name child-type1)
                            ,child1-uid))
                        (syscat::get-dependent-resources
                          session
                          resourcetype-schema
                          (list (syscat::name parent-type) parent-uid))))
      ;; Confirm we get the type when asking for all things with that relationship
      (syscat::log-message :info ";TEST Confirm listing of types with all things with this relationship")
      (let ((result (first (syscat::get-resources
                             session
                             resourcetype-schema
                             (list (syscat::name parent-type)
                                   parent-uid
                                   (syscat::name parent-rel1))))))
        (fiveam:is (not (null (gethash "ScType" result))))
        (fiveam:is (equal (syscat::sanitise-uid (syscat::name child-type1))
                          (gethash "ScType" result)))
        (fiveam:is (gethash "ScCanonicalpath" result))
        (fiveam:is (equal (format nil "/~A/~A/~A/~A/~A"
                                  (syscat::name parent-type)
                                  parent-uid
                                  (syscat::name parent-rel1)
                                  (syscat::name child-type1)
                                  (syscat::sanitise-uid child1-uid))
                          (gethash "ScCanonicalpath" result))))
      ;; Delete the dependent resource
      (syscat::log-message :info ";TEST Delete the dependent resource")
      (let ((child-path (list (syscat::name parent-type)
                              parent-uid
                              (syscat::name parent-rel1)
                              (syscat::name child-type1)
                              child1-uid)))
        (syscat::log-message :debug (format nil "Using path ~{/~A~}" child-path))
        (fiveam:is (null (syscat::delete-resource-by-path
                           session
                           child-path
                           resourcetype-schema)))
        ;; Confirm the dependent resource is gone
        (syscat::log-message :info ";TEST Confirm the dependent resource is gone")
        (fiveam:is (null (syscat::resource-exists-p session child-path))))
      ;; Attempt to create a child resource that isn't of a dependent type
      (syscat::log-message
        :info
        (format nil ";TEST Fail to create a non-dependent child resource /~A/~A/~A/~A"
                (syscat::name parent-type)
                parent-uid
                (syscat::name parent-rel1)
                invalid-child-type))
      (fiveam:signals (syscat::client-error "This is not a dependent resource type")
                      (syscat::store-dependent-resource
                        session
                        resourcetype-schema
                        (list (syscat::name parent-type)
                              parent-uid
                              (syscat::name parent-rel1)
                              invalid-child-type)
                        `(("ScUID" . ,invalid-child-uid))
                        *admin-user*))
      ;; Create the dependent resource yet again
      (syscat::log-message :info ";TEST Sucessfully re-create the dependent resource")
      (syscat::store-dependent-resource
        session
        resourcetype-schema
        (list (syscat::name parent-type)
              parent-uid
              (syscat::name parent-rel1)
              (syscat::name child-type1))
        `(("ScUID" . ,child1-uid))
        *admin-user*)
      ;; Create the second dependent resource, to confirm any-to-* rels work
      (syscat::log-message :info ";TEST Create an any-to-* dependent resource")
      (syscat::store-dependent-resource
        session
        resourcetype-schema
        (list (syscat::name parent-type)
              parent-uid
              (syscat::name parent-rel2)
              (syscat::name child-type2))
        `(("ScUID" . ,child2-uid)
          (,child2-attr1-name . ,child2-attr1-val))
        *admin-user*)
      ;; Confirm it's there
      (syscat::log-message :info ";TEST Confirm the any-to-* dependent resource is present")
      (let ((result (first (syscat::get-resources
                             session
                             resourcetype-schema
                             (list (syscat::name parent-type)
                                   parent-uid
                                   (syscat::name parent-rel2)
                                   (syscat::name child-type2)
                                   child2-uid)
                             :attributes (list child2-attr1-name)))))
        (fiveam:is (string= child2-attr1-val
                            (gethash child2-attr1-name result)))
        (fiveam:is (string= (format nil "~{/~A~}"
                                    (list (syscat::name parent-type)
                                          parent-uid
                                          (syscat::name parent-rel2)
                                          (syscat::name child-type2)
                                          child2-uid))
                            (gethash "ScCanonicalpath" result))))
      ;; Delete the parent resource
      (syscat::log-message :info ";TEST Recursively deleting the parent resource")
      (syscat::delete-resource-by-path
        session
        (list (syscat::name parent-type) parent-uid)
        resourcetype-schema
        :recursive t)
      ;; Confirm the dependent resource was recursively deleted with it
      (syscat::log-message :info ";TEST Confirm the parent resource is gone")
      (fiveam:is (null (syscat::get-resources
                         session
                         resourcetype-schema
                         (list (syscat::name parent-type) parent-uid))))
      (syscat::log-message :info ";TEST Confirm the dependent resource is gone")
      (fiveam:is (null (syscat::get-resources
                         session
                         resourcetype-schema
                         (list (syscat::name child-type1) child1-uid))))
      ;; Remove the newly-created schema-version
      (syscat::delete-schema-version session schema-version))
    ;; Clean up the Bolt session
    (neo4cl:disconnect session)))

(fiveam:test
  resources-dependent-single-parent
  :depends-on 'resources-dependent-simple
  "Ensure dependent resources can only have a single parent."
  (let* ((child1-type (syscat::make-incoming-rtypes :name "Models"
                                                        :dependent t))
         (child1-uid "Synthetics")
         (child2-uid "Tricycles")
         (parent1-type (syscat::make-incoming-rtypes :name "Makes"))
         (parent1-rel (syscat::make-incoming-rels :name "PRODUCES"
                                                      :source-types (list (syscat::name
                                                                            parent1-type))
                                                      :target-types (list (syscat::name
                                                                            child1-type))
                                                      :reltype "dependent"
                                                      :cardinality "1:1"))
         (parent1-uid "Weyland-Yutani")
         (parent2-uid "Tyrell")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    (syscat::log-message :info ";TEST Create the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Add the test-specific resources and relationships
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "resources-dependent-single-parent"
        :resourcetypes (list parent1-type child1-type)
        :relationships (list parent1-rel))
      schema-version)
    ;; Fetch the updated schema definition and start the tests
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Create the feasible parent
      (syscat::log-message :info ";TEST Create the feasible parent")
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name parent1-type)
                                  `(("ScUID" . ,parent1-uid))
                                  *admin-user*)
      ;; Create the child
      (syscat::log-message :info ";TEST Create the child")
      (syscat::store-dependent-resource
        session
        resourcetype-schema
        (list (syscat::name parent1-type)
              parent1-uid
              (syscat::name parent1-rel)
              (syscat::name child1-type))
        `(("ScUID" . ,child1-uid))
        *admin-user*)
      ;; Confirm the child is there
      (syscat::log-message :info ";TEST Confirm the child is there")
      (let ((result (syscat::get-resources
                      session
                      resourcetype-schema
                      (list (syscat::name parent1-type)
                            parent1-uid
                            (syscat::name parent1-rel)
                            (syscat::name child1-type)
                            child1-uid))))
        (syscat::log-message :info (format nil "Success-check on aisle 5: child = ~A" result))
        (fiveam:is (not (null result))))
      ;; Fail to create a duplicate
      (syscat::log-message :info ";TEST Fail to create a duplicate child")
      (fiveam:signals
        syscat::integrity-error
        (syscat::store-dependent-resource
          session
          resourcetype-schema
          (list (syscat::name parent1-type)
                parent1-uid
                (syscat::name parent1-rel)
                (syscat::name child1-type))
          `(("ScUID" . ,child1-uid))
          *admin-user*))
      ;; Fail to create a second child with that 1:1 dependent relationship
      (syscat::log-message :info ";TEST Fail to create a second 1:1 child")
      (fiveam:signals
        syscat::integrity-error
        (syscat::store-dependent-resource
          session
          resourcetype-schema
          (list (syscat::name parent1-type)
                parent1-uid
                (syscat::name parent1-rel)
                (syscat::name child1-type))
          `(("ScUID" . ,child2-uid))
          *admin-user*))
      ;; Create the new parent
      (syscat::log-message :info ";TEST Create the second parent")
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name parent1-type)
                                  `(("ScUID" . ,parent2-uid))
                                  *admin-user*)
      ;; Fail to add a second parent-child relationship
      (syscat::log-message :info ";TEST Fail to create an additional parent-child relationship")
      (fiveam:signals
        syscat::integrity-error
        (syscat::create-relationship-by-path
          session
          (format nil "/~A/~A/~A"
                  (syscat::name parent1-type)
                  parent2-uid
                  (syscat::name parent1-rel))
          (format nil "/~A/~A/~A/~A/~A"
                  (syscat::name parent1-type)
                  parent1-uid
                  (syscat::name parent1-rel)
                  (syscat::name child1-type)
                  child1-uid)
          resourcetype-schema))
      ;; Move the child to the new parent
      (syscat::log-message :info ";TEST Move the child to the new parent")
      (fiveam:is
        (null
          (syscat::move-dependent-resource
            session
            resourcetype-schema
            (format nil "/~A/~A/~A/~A/~A"
                    (syscat::name parent1-type)
                    parent1-uid
                    (syscat::name parent1-rel)
                    (syscat::name child1-type)
                    child1-uid)
            (format nil "/~A/~A/~A"
                    (syscat::name parent1-type)
                    parent2-uid
                    (syscat::name parent1-rel)))))
      ;; Confirm the child is now reachable under the new parent
      (fiveam:is (syscat::resource-exists-p
                   session
                   (list (syscat::name parent1-type)
                         parent2-uid
                         (syscat::name parent1-rel)
                         (syscat::name child1-type)
                         child1-uid)))
      ;; Confirm the child is no longer reachable under the previous parent
      (fiveam:is
        (null
          (syscat::resource-exists-p
            session
            (list (syscat::name parent1-type)
                  parent1-uid
                  (syscat::name parent1-rel)
                  (syscat::name child1-type)
                  child1-uid))))
      ;; Clean up
      (syscat::log-message :info ";TEST Delete the fixtures")
      (syscat::delete-resource-by-path
        session
        (list (syscat::name parent1-type) parent1-uid)
        resourcetype-schema
        :recursive t)
      (syscat::delete-resource-by-path
        session
        (list (syscat::name parent1-type) parent2-uid)
        resourcetype-schema
        :recursive t)
      (syscat::delete-schema-version session schema-version)
      (neo4cl:disconnect session))))

(fiveam:test
  resources-dependent-cardinality
  :depends-on 'resources-dependent-simple
  "Behaviour around cardinality of dependent resources."
  (let* ((Parent1-type "People")
         (parent1-uid "Anne")
         ;; We only need one child-type for this test,
         ;; remembering that we can define multiple types of relationship
         ;; between any pair of resourcetypes.
         (child-type-1 (syscat::make-incoming-rtypes :name "Pets"
                                                         :dependent t))
         ;; Fully-specify 1:many cardinality
         (child1-rel (syscat::make-incoming-rels :name "HAS"
                                                     :source-types '("People")
                                                     :target-types '("Pets")
                                                     :reltype "dependent"
                                                     :cardinality "1:many"))
         (child1-uid "Cats")
         ;; Leave cardinality unspecified, to check default behaviour
         (child2-rel (syscat::make-incoming-rels :name "WANTS"
                                                     :source-types '("People")
                                                     :target-types '("Pets")
                                                     :reltype "dependent"))
         (child2-uid "ItalianGreyhound")
         ;; Fully-specify 1:1 cardinality
         (child3-rel (syscat::make-incoming-rels :name "EMOTIONAL_SUPPORT"
                                                     :source-types '("People")
                                                     :target-types '("Pets")
                                                     :reltype "dependent"
                                                     :cardinality "1:1"))
         (child3-uid "FluffyChicken")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Test make-incoming-rels, because now it'll cause the `let` statement fail
    (fiveam:signals error (syscat::make-incoming-rels :name "NEEDS"
                                                          :source-types '("People")
                                                          :target-types '("Pets")
                                                          :reltype "dependent"
                                                          :cardinality "many:1"))
    (syscat::log-message :info ";TEST Create the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Add the test-specific resources and relationships
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "resources-dependent-cardinality"
        :resourcetypes (list child-type-1)
        :relationships (list child1-rel child2-rel child3-rel))
      schema-version)
    ;; Fetch the updated schema definition and start the tests
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Create the feasible parent
      (syscat::store-resource session
                                  resourcetype-schema
                                  parent1-type
                                  `(("ScUID" . ,parent1-uid))
                                  *admin-user*)
      ;; Successfully create an explicitly 1:many relationship
      (fiveam:is (equal child1-uid
                        (syscat::store-dependent-resource
                          session
                          resourcetype-schema
                          (list parent1-type
                                parent1-uid
                                (syscat::name child1-rel)
                                (syscat::name child-type-1))
                          `(("ScUID" . ,child1-uid))
                          *admin-user*)))
      ;; Confirm that resource is present
      (fiveam:is (syscat::get-resources
                   session
                   resourcetype-schema
                   (list parent1-type
                         parent1-uid
                         (syscat::name child1-rel)
                         (syscat::name child-type-1)
                         child1-uid)))
      ;; Successfully create an implicitly 1:many relationship
      (fiveam:is (equal child2-uid
                        (syscat::store-dependent-resource
                          session
                          resourcetype-schema
                          (list parent1-type
                                parent1-uid
                                (syscat::name child2-rel)
                                (syscat::name child-type-1))
                          `(("ScUID" . ,child2-uid))
                          *admin-user*)))
      ;; Confirm that resource is present
      (fiveam:is (syscat::get-resources
                   session
                   resourcetype-schema
                   (list parent1-type
                         parent1-uid
                         (syscat::name child2-rel)
                         (syscat::name child-type-1)
                         child2-uid)))
      ;; Successfully create an explicitly 1:1 relationship
      (fiveam:is (equal child3-uid
                        (syscat::store-dependent-resource
                          session
                          resourcetype-schema
                          (list parent1-type
                                parent1-uid
                                (syscat::name child3-rel)
                                (syscat::name child-type-1))
                          `(("ScUID" . ,child3-uid))
                          *admin-user*)))
      ;; Confirm that resource is present
      (fiveam:is (syscat::get-resources
                   session
                   resourcetype-schema
                   (list parent1-type
                         parent1-uid
                         (syscat::name child3-rel)
                         (syscat::name child-type-1)
                         child3-uid)))
      ;; Clean up
      (syscat::log-message :info ";TEST Delete the fixtures")
      ;; We only have to recursively delete the parent,
      ;; because all the other resources in this test depend on it.
      (syscat::delete-resource-by-path
        session
        (list parent1-type parent1-uid)
        resourcetype-schema
        :recursive t)
      ;; Remove the temporary schema-version
      (syscat::delete-schema-version session schema-version)
      ;; Clean up the session
      (neo4cl:disconnect session))))

(fiveam:test
  resources-dependent-errors
  :depends-on 'resources-dependent-cardinality
  "Error conditions around creating/moving dependent resources"
  (let* ((child1-type (syscat::make-incoming-rtypes :name "Models"
                                                        :dependent t))
         (child1-uid "Synthetics")
         (parent1-type (syscat::make-incoming-rtypes :name "Makes"))
         (parent1-rel (syscat::make-incoming-rels :name "PRODUCES"
                                                      :source-types (list (syscat::name
                                                                            parent1-type))
                                                      :target-types (list (syscat::name
                                                                            child1-type))
                                                      :reltype "dependent"
                                                      :cardinality "1:many"))
         (parent1-uid "Weyland-Yutani")
         (bad-parent1-type (syscat::make-incoming-rtypes :name "NewGroups"))
         (bad-parent1-uid "Replicants")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    (syscat::log-message :info ";TEST Create the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Add the test-specific resources and relationships
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "resources-dependent-errors"
        :resourcetypes (list parent1-type child1-type bad-parent1-type)
        :relationships (list parent1-rel))
      schema-version)
    ;; Fetch the updated schema definition and start the tests
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Create the feasible parent
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name parent1-type)
                                  `(("ScUID" . ,parent1-uid))
                                  *admin-user*)
      ;; Create the child
      (syscat::store-dependent-resource
        session
        resourcetype-schema
        (list (syscat::name parent1-type)
              parent1-uid
              (syscat::name parent1-rel)
              (syscat::name child1-type))
        `(("ScUID" . ,child1-uid))
        *admin-user*)
      ;; Confirm the child is there
      (fiveam:is (syscat::get-resources
                   session
                   resourcetype-schema
                   (list (syscat::name parent1-type)
                         parent1-uid
                         (syscat::name parent1-rel)
                         (syscat::name child1-type)
                         child1-uid)))
      ;; Create the infeasible parent
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name bad-parent1-type)
                                  `(("ScUID" . ,bad-parent1-uid))
                                  *admin-user*)
      (syscat::log-message :info ";TEST Try to move the child to an invalid parent")
      (fiveam:signals
        syscat::bad-argument
        (syscat::move-dependent-resource
          session
          resourcetype-schema
          (format nil "/~A/~A/~A/~A/~A"
                  (syscat::name parent1-type)
                  parent1-uid
                  (syscat::name parent1-rel)
                  (syscat::name child1-type)
                  child1-uid)
          (format nil "/~A/~A"
                  (syscat::name bad-parent1-type)
                  bad-parent1-uid)))
      ;; Clean up
      (syscat::log-message :info ";TEST Delete the fixtures")
      (syscat::delete-resource-by-path
        session
        (list (syscat::name parent1-type) parent1-uid)
        resourcetype-schema
        :recursive t)
      (syscat::delete-resource-by-path
        session
        (list (syscat::name bad-parent1-type) bad-parent1-uid)
        resourcetype-schema)
      (syscat::delete-schema-version session schema-version)
      (neo4cl:disconnect session))))

(fiveam:test
  resources-dependent-compound
  :depends-on 'resources-dependent-errors
  "Basic operations on 2-layered dependent resources"
  (let* ((parent-type (syscat::make-incoming-rtypes :NAME "Routers"))
         (child-type (syscat::make-incoming-rtypes :NAME "Interfaces"
                                                       :DEPENDENT t))
         (grandchild-type (syscat::make-incoming-rtypes :NAME "Ipv4Addresses"
                                                            :DEPENDENT t))
         (relationship (syscat::make-incoming-rels
                         :name "INTERFACES"
                         :reltype "dependent"
                         :source-types (list (syscat::name parent-type))
                         :target-types (list (syscat::name child-type))
                         :cardinality "1:many"))
         (child-relationship (syscat::make-incoming-rels
                               :name "ADDRESSES"
                               :reltype "dependent"
                               :source-types (list (syscat::name child-type))
                               :target-types (list (syscat::name grandchild-type))
                               :cardinality "1:many"))
         (parent-uid "marshall")
         (child-uid "eth0")
         (grandchild-uid "192.168.24.1")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Create the fixtures
    (syscat::log-message :info ";TEST Create the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Install the new resourcetypes and relationship
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "resources-dependent-compound"
        :resourcetypes (list parent-type child-type grandchild-type)
        :relationships (list relationship child-relationship))
      schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Create the parent resource
      (syscat::log-message :info ";TEST Create the parent resource")
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name parent-type)
                                  `(("ScUID" . ,parent-uid))
                                  *admin-user*)
      ;; Create the child resource
      (syscat::log-message :info ";TEST Create the child resource")
      (syscat::store-dependent-resource session
                                            resourcetype-schema
                                            (list (syscat::name parent-type)
                                                  parent-uid
                                                  (syscat::name relationship)
                                                  (syscat::name child-type))
                                            `(("ScUID" . ,child-uid))
                                            *admin-user*)
      ;; Create the grandchild resource
      (syscat::log-message :info ";TEST Create the grandchild resource")
      (syscat::store-dependent-resource
        session
        resourcetype-schema
        (list (syscat::name parent-type)
              parent-uid
              (syscat::name relationship)
              (syscat::name child-type)
              child-uid
              (syscat::name child-relationship)
              (syscat::name grandchild-type))
        `(("ScUID" . ,grandchild-uid))
        *admin-user*)
      ;; Delete the parent resource
      (syscat::log-message :info ";TEST Recursively deleting the parent resource")
      (syscat::delete-resource-by-path
        session
        (list (syscat::name parent-type) parent-uid)
        resourcetype-schema
        :recursive t)
      ;; Confirm the dependent resources were recursively deleted with it
      (syscat::log-message :info ";TEST Confirm the dependent resource is gone")
      (fiveam:is (null (syscat::get-resources
                         session
                         resourcetype-schema
                         (list (syscat::name parent-type)
                               parent-uid
                               (syscat::name relationship)
                               (syscat::name child-type)
                               child-uid))))
      (syscat::log-message :info ";TEST Confirm the grandchild resource is gone")
      (fiveam:is (null
                   (syscat::get-resources
                     session
                     resourcetype-schema
                     (list (syscat::name parent-type)
                           parent-uid
                           (syscat::name relationship)
                           (syscat::name child-type)
                           child-uid
                           (syscat::name child-relationship)
                           (syscat::name grandchild-type)
                           grandchild-uid))))
      ;; Delete the temporary schema-version
      (syscat::delete-schema-version session schema-version))
    ;; Clean up the Bolt session
    (neo4cl:disconnect session)))

(fiveam:test
  resources-dependent-moving
  :depends-on 'resources-dependent-compound
  "Moving a dependent resource to a new parent"
  (let* ((target-type (syscat::make-incoming-rtypes :NAME "Ipv4Addresses"
                                                        :DEPENDENT t))
         (p2-type (syscat::make-incoming-rtypes :name "Interfaces"
                                                    :dependent t))
         (p1-type (syscat::make-incoming-rtypes :name "Routers"))
         (p1-target-rel (syscat::make-incoming-rels
                          :name "ADDRESSES"
                          :reltype "dependent"
                          :source-types (list (syscat::name p1-type)
                                              (syscat::name p2-type))
                          :target-types (list (syscat::name target-type))
                          :cardinality "1:many"))
         (p1-p2-rel (syscat::make-incoming-rels
                      :name "INTERFACES"
                      :reltype "dependent"
                      :source-types (list (syscat::name p1-type))
                      :target-types (list (syscat::name p2-type))
                      :cardinality "1:many"))
         (p1-uid "woomera")
         (p2-uid "eth2")
         (target-uid "172.20.0.1")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Create the fixtures
    (syscat::log-message :info ";TEST Create the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Install the new resourcetypes and relationship
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "resources-dependent-moving"
        :resourcetypes (list p1-type p2-type target-type)
        :relationships (list p1-target-rel p1-p2-rel))
      schema-version)
    ;; Get the complete schema
    (syscat::log-message :info ";TEST Fetch updated schema")
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::log-message :info ";TEST Install default resources")
      (syscat::install-default-resources session resourcetype-schema)
      ;; Create initial parent
      (syscat::log-message :info ";TEST Create initial parent resource")
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name p1-type)
                                  `(("ScUID" . ,p1-uid))
                                  *admin-user*)
      ;; Create second parent as dependent on the initial
      (syscat::store-dependent-resource
        session
        resourcetype-schema
        (list (syscat::name p1-type)
              p1-uid
              (syscat::name p1-p2-rel)
              (syscat::name p2-type))
        `(("ScUID" . ,p2-uid))
        *admin-user*)
      ;; Create the dependent resource to be moved
      (syscat::store-dependent-resource
        session
        resourcetype-schema
        (list (syscat::name p1-type)
              p1-uid
              (syscat::name p1-target-rel)
              (syscat::name target-type))
        `(("ScUID" . ,target-uid))
        *admin-user*)
      ;; Move the resource
      (syscat::log-message
        :info
        (format nil ";TEST Move dependent resource /~A/~A/~A/~A/~A to new parent /~A/~A/~A/~A/~A/~A"
                (syscat::name p1-type)
                p1-uid
                (syscat::name p1-target-rel)
                (syscat::name target-type)
                target-uid
                (syscat::name p1-type)
                p1-uid
                (syscat::name p1-p2-rel)
                (syscat::name p2-type)
                p2-uid
                (syscat::name p1-target-rel)))
      (let ((result (syscat::move-dependent-resource
                      session
                      resourcetype-schema
                      ;; URI
                      (format nil "/~A/~A/~A/~A/~A"
                              (syscat::name p1-type)
                              p1-uid
                              (syscat::name p1-target-rel)
                              (syscat::name target-type)
                              target-uid)
                      ;; New parent
                      (format nil "/~A/~A/~A/~A/~A/~A"
                              (syscat::name p1-type)
                              p1-uid
                              (syscat::name p1-p2-rel)
                              (syscat::name p2-type)
                              p2-uid
                              (syscat::name p1-target-rel)))))
        (syscat::log-message :info (format nil "Result was: ~A" result))
        (fiveam:is (null result)))
      ;; Confirm the target resource is now at the new target path
      (let ((result (first (syscat::get-resources
                             session
                             resourcetype-schema
                             (list (syscat::name p1-type)
                                   p1-uid
                                   (syscat::name p1-p2-rel)
                                   (syscat::name p2-type)
                                   p2-uid
                                   (syscat::name p1-target-rel)
                                   (syscat::name target-type)
                                   target-uid)))))
        (fiveam:is (gethash "ScUID" result))
        (fiveam:is (equal (syscat::sanitise-uid target-uid)
                          (gethash "ScUID" result)))
        (fiveam:is (string= (format nil "~{/~A~}"
                              (list (syscat::name p1-type)
                                    p1-uid
                                    (syscat::name p1-p2-rel)
                                    (syscat::name p2-type)
                                    p2-uid
                                    (syscat::name p1-target-rel)
                                    (syscat::name target-type)
                                    target-uid))
                            (gethash "ScCanonicalpath" result))))
      (let ((result (first (syscat::get-resources
                             session
                             resourcetype-schema
                             (list (syscat::name p1-type)
                                   p1-uid
                                   (syscat::name p1-p2-rel)
                                   (syscat::name p2-type)
                                   p2-uid
                                   (syscat::name p1-target-rel)
                                   (syscat::name target-type)
                                   target-uid)))))
        (fiveam:is (equal target-uid
                          (gethash "ScUID" result))))
      ;; Confirm the target resource is no longer present at the original path
      (fiveam:is
        (null
          (syscat::get-resources
            session
            resourcetype-schema
            (list (syscat::name p1-type)
                  p1-uid
                  (syscat::name p1-target-rel)
                  (syscat::name target-type)
                  target-uid))))
      ;; Delete the parent resource
      (syscat::log-message :debug ";TEST Delete the parent resource")
      (syscat::delete-resource-by-path
        session
        (list (syscat::name p1-type) p1-uid)
        resourcetype-schema
        :recursive t)
      ;; Confirm stuff is gone
      (syscat::log-message :debug ";TEST Confirm the parent resource is gone")
      (fiveam:is (null
                   (syscat::get-resources
                     session
                     resourcetype-schema
                     (list (syscat::name p1-type) p1-uid))))
      ;; Remove the temporary schema-version
      (syscat::delete-schema-version session schema-version))
    ;; Clean up the session
    (neo4cl:disconnect session)))

(fiveam:test
  canonical-subpath
  "Extracting the canonical subpath of a given path."
  :depends-on 'install-core-schema
  (let* ((session (neo4cl:establish-bolt-session *bolt-server*))
         ;; Ensure there's a new schema-version to work from
         (schema-version (syscat::create-new-schema-version session))
         (type1 (syscat::make-incoming-rtypes :name "Teams" :dependent t))
         (type2 (syscat::make-incoming-rtypes :name "Mascots" :dependent t))
         (rel-parent-t1 (syscat::make-incoming-rels :name "CONTAINS"
                                                        :source-types '("Organisations")
                                                        :target-types '("Teams")
                                                        :reltype "dependent"))
         (rel-t1-t2 (syscat::make-incoming-rels :name "HAS_MASCOT"
                                                    :source-types '("Teams")
                                                    :target-types '("Mascots")
                                                    :reltype "dependent"))
         (rel-people-t1 (syscat::make-incoming-rels :name "MEMBER_OF"
                                                        :source-types '("People")
                                                        :target-types '("Teams")))
         (rel-people-t2 (syscat::make-incoming-rels :name "LIKES"
                                                        :source-types '("People")
                                                        :target-types '("Mascots")))
         (parent1-uid "Org1"))
    ;; Install the core schema into that new schema-version
    (syscat::log-message :info ";TEST Set up the schema")
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Set up the test schema
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "canonical-subpath"
        :resourcetypes (list type1 type2)
        :relationships (list rel-parent-t1 rel-t1-t2 rel-people-t1 rel-people-t2))
      schema-version)
    ;; Fetch the schema into memory
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Simplest tests
      (syscat::log-message :debug ";TEST End-of-list")
      (multiple-value-bind (canonical remainder)
        ;; It doesn't check the validity of the path at this stage,
        ;; which is why this path succeeds here in this form,
        ;; but fails in a later test.
        (syscat::canonical-subpath resourcetype-schema nil '("People" "ScAdmin" "EMPLOYS" "People" "Me"))
        (fiveam:is (equal '("People" "ScAdmin" "EMPLOYS" "People" "Me")
                          canonical))
        (fiveam:is (null remainder)))
      (syscat::log-message :debug ";TEST strings-p")
      (fiveam:signals syscat::bad-argument
                      (syscat::canonical-subpath resourcetype-schema '("foo" 4 5) '("bar" "baz")))
      ;; Primary resource of an invalid type
      (syscat::log-message :debug ";TEST Invalid primary resource")
      (fiveam:signals syscat::undefined-resourcetype
                      (syscat::canonical-subpath resourcetype-schema '("Un" "Real")))
      ;; Primary resource and nothing more
      (syscat::log-message :debug ";TEST Valid primary resource")
      (multiple-value-bind (canonical remainder)
        (syscat::canonical-subpath resourcetype-schema '("People" "ScAdmin") '())
        (fiveam:is (equal '("People" "ScAdmin") canonical))
        (fiveam:is (null remainder)))
      ;; Invalid path-length
      (syscat::log-message :debug ";TEST Invalid path lengths")
      (fiveam:signals syscat::bad-argument
                      (syscat::canonical-subpath resourcetype-schema '("In-valid-length")))
      (fiveam:signals syscat::bad-argument
                      (syscat::canonical-subpath resourcetype-schema '("In" "valid" "length")))
      (fiveam:signals syscat::bad-argument
                      (syscat::canonical-subpath resourcetype-schema '("In" "valid" "path" "length")))
      ;; Undefined target-type
      (syscat::log-message :debug ";TEST Undefined target type")
      (fiveam:signals syscat::undefined-resourcetype
                      (syscat::canonical-subpath
                        resourcetype-schema
                        '("People" "ScAdmin" "HAS" "invalid" "resourcetype")))
      ;; Undefined parent resourcetype
      (syscat::log-message :debug ";TEST Undefined parent resourcetype")
      (fiveam:signals
        syscat::undefined-resourcetype
        (syscat::canonical-subpath resourcetype-schema
                                       '("Notty" "Zactly-right" "EMPLOYS")
                                       '("People" "ScAdmin")))
      ;; Invalid relationship
      (syscat::log-message :debug ";TEST Invalid relationship")
      (fiveam:signals
        syscat::undefined-relationship
        (syscat::canonical-subpath resourcetype-schema
                                       '("People" "ScAdmin" "EMPLOYS")
                                       '("People" "Me")))
      ;; Simple canonical paths; single-level edition
      (syscat::log-message :debug ";TEST One-level dependent resource; simple canonical path.")
      (multiple-value-bind (canonical remainder)
        (syscat::canonical-subpath resourcetype-schema
                                       `("Organisations" ,parent1-uid "CONTAINS"
                                         "Teams" "Team1"))
        (fiveam:is (equal `("Organisations" ,parent1-uid "CONTAINS"
                            "Teams" "Team1") canonical))
        (fiveam:is (null remainder)))
      ;; Simple canonical paths; dual-level edition
      (syscat::log-message :debug ";TEST Two-level dependent resource; simple canonical path.")
      (multiple-value-bind (canonical remainder)
        (syscat::canonical-subpath resourcetype-schema `("Organisations" ,parent1-uid "CONTAINS"
                                                             "Teams" "Team1" "HAS_MASCOT"
                                                             "Mascots" "Fred"))
        (fiveam:is (equal `("Organisations" ,parent1-uid "CONTAINS"
                            "Teams" "Team1" "HAS_MASCOT"
                            "Mascots" "Fred")
                          canonical))
        (fiveam:is (null remainder)))
      ;; Indirect path to a primary resource
      (syscat::log-message :debug ";TEST Non-dependent path to a primary resource.")
      (multiple-value-bind (canonical remainder)
        (syscat::canonical-subpath resourcetype-schema
                                       `("People" "ScAdmin" "MEMBER_OF_ORG"
                                         "Organisations" ,parent1-uid))
        (fiveam:is (equal `("Organisations" ,parent1-uid) canonical))
        (fiveam:is (equal '("People" "ScAdmin" "MEMBER_OF_ORG") remainder)))
      ;; Indirect path to a dependent resource; two-level edition
      (syscat::log-message
        :debug ";TEST Non-dependent path to a dependent resource - two levels down.")
      (multiple-value-bind (canonical remainder)
        (syscat::canonical-subpath resourcetype-schema
                                       '("People" "ScAdmin" "LIKES" "Mascots" "Fred"))
        (fiveam:is (equal '("Mascots" "Fred") canonical))
        (fiveam:is (equal '("People" "ScAdmin" "LIKES") remainder)))
      ;; Remove the schema we added for this test
      (syscat::delete-schema-version session schema-version)
      ;; Clean up the session
      (neo4cl:disconnect session))))

(fiveam:test
  canonical-subpath-verification
  "Verify the functioning of canonical-subpath-p"
  :depends-on 'canonical-subpath
  ;; Install the test schema
  (let* ((session (neo4cl:establish-bolt-session *bolt-server*))
         (parsed-schema (syscat::parse-schema-from-alist
                          (json:decode-json-from-source #P"../test/internal_tests/test_schema.json")))
         (schema-version (syscat::create-new-schema-version session)))
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    (syscat::install-subschema session parsed-schema schema-version)
    ;; Fetch the test schema
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Remove the schema, now that we've round-tripped it.
      (syscat::delete-schema-version session schema-version)
      ;; Clean up the session, because it's no longer needed for this test.
      (neo4cl:disconnect session)
      ;; Start testing paths
      ;; Begin with valid canonical paths
      (fiveam:is (syscat::canonical-path-p '("Buildings" "Office") resourcetype-schema))
      (fiveam:is (syscat::canonical-path-p '("Buildings" "Office"
                                                 "CONTAINS_FLOORS" "Floors" "First")
                                               resourcetype-schema))
      (fiveam:is (syscat::canonical-path-p '("Buildings" "Office"
                                                 "CONTAINS_FLOORS" "Floors" "First"
                                                 "CONTAINS_ROOMS" "Rooms" "Lobby")
                                               resourcetype-schema))
      ;; Now test invalid ones
      (fiveam:is (null (syscat::canonical-path-p '("Organisations" "Bigcorp"
                                                       "TENANT_IN" "Buildings" "Office")
                                                     resourcetype-schema)))
      (fiveam:is (null (syscat::canonical-path-p '("Teams" "Management"
                                                       "TENANT_IN" "Buildings" "Office")
                                                     resourcetype-schema)))
      (fiveam:is (null (syscat::canonical-path-p '("Organisations" "Bigcorp"
                                                       "TENANT_IN" "Floors" "First")
                                                     resourcetype-schema)))
      (fiveam:is (null (syscat::canonical-path-p '("Teams" "Management"
                                                       "TENANT_IN" "Floors" "First")
                                                     resourcetype-schema)))
      (fiveam:is (null (syscat::canonical-path-p '("Organisations" "Bigcorp"
                                                       "TENANT_IN" "Rooms" "Lobby")
                                                     resourcetype-schema)))
      (fiveam:is (null (syscat::canonical-path-p '("Teams" "Management"
                                                       "TENANT_IN" "Rooms" "Lobby")
                                                     resourcetype-schema))))))

(fiveam:test
  resources-dependent-canonicalised
  :depends-on 'canonical-subpath
  "Confirm that canonicalisation works as expected."
  (let* ((res1type (syscat::make-incoming-rtypes :name "Factories"))
         (res1uid "Fac1")
         (res2typename "People")
         (res2uid "George")
         (rel1 (syscat::make-incoming-rels :source-types '("People")
                                               :name "OWNS"
                                               :target-types '("Factories")
                                               :reltype "any"
                                               :cardinality "many:many"))
         (depres1type (syscat::make-incoming-rtypes :name "Robots"
                                                        :dependent t))
         (depres1uid "Bender")
         (deprel1 (syscat::make-incoming-rels :source-types '("Factories")
                                                  :name "CONTAINS"
                                                  :target-types '("Robots")
                                                  :reltype "dependent"
                                                  :cardinality "1:many"))
         (depres2type (syscat::make-incoming-rtypes :name "Components"
                                                        :dependent t))
         (depres2uid "PowerCable")
         (deprel2 (syscat::make-incoming-rels :source-types '("Robots")
                                                  :name "HAS"
                                                  :target-types '("Components")
                                                  :reltype "dependent"
                                                  :cardinality "1:many"))
         (session (the neo4cl::bolt-session (neo4cl:establish-bolt-session *bolt-server*)))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Set up the fixtures
    (syscat::log-message :info ";TEST Set up the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Install the new resourcetypes and relationship
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "resources-dependent-canonicalised"
        :resourcetypes (list res1type depres1type depres2type)
        :relationships (list rel1 deprel1 deprel2 ))
      schema-version)
    (let ((resourcetype-schema (the hash-table (car (syscat::fetch-current-schema session)))))
      ;; Create a resource, then succeed in getting its path
      (syscat::log-message :info ";TEST Create the primary resource")
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name res1type)
                                  `(("ScUID" . ,res1uid))
                                  *admin-user*)
      (syscat::log-message :info ";TEST Succeed in canonicalising a primary resource")
      (fiveam:is
        (string= (format nil "/~A/~A" (syscat::name res1type) res1uid)
                 (gethash "ScCanonicalpath" (first (syscat::get-resources
                                                     session
                                                     resourcetype-schema
                                                     (list (syscat::name res1type) res1uid))))))
      ;; Create a dependent resource, then succeed in getting its path
      (syscat::log-message :info ";TEST Store dependent resource")
      (syscat::store-dependent-resource session
                                            resourcetype-schema
                                            (list (syscat::name res1type)
                                                  res1uid
                                                  (syscat::name deprel1)
                                                  (syscat::name depres1type))
                                            `(("ScUID" . ,depres1uid))
                                            *admin-user*)
      (syscat::log-message :info ";TEST Fetch dependent resource")
      (let ((this-path (list (syscat::name res1type)
                             res1uid
                             (syscat::name deprel1)
                             (syscat::name depres1type)
                             depres1uid)))
        ;; There is only one on this path, yes?
        (fiveam:is (= 1 (length (syscat::get-resources session
                                                           resourcetype-schema
                                                           this-path))))
        ;; OK, but does it also come back with the expected canonical path?
        (fiveam:is
          (string= (format nil "~{/~A~}"
                           (list
                             (syscat::name res1type)
                             res1uid
                             (syscat::name deprel1)
                             (syscat::name depres1type)
                             depres1uid))
                   (gethash "ScCanonicalpath" (first (syscat::get-resources session
                                                                                resourcetype-schema
                                                                                this-path))))))
      ;; Create a doubly-dependent resource, then succeed in getting its path
      (syscat::log-message :info ";TEST Store doubly-dependent resource")
      (syscat::store-dependent-resource session
                                            resourcetype-schema
                                            (list (syscat::name res1type)
                                                  res1uid
                                                  (syscat::name deprel1)
                                                  (syscat::name depres1type)
                                                  depres1uid
                                                  (syscat::name deprel2)
                                                  (syscat::name depres2type))
                                            `(("ScUID" . ,depres2uid))
                                            *admin-user*)
      (syscat::log-message :info ";TEST Fetch doubly-dependent resource")
      (let ((this-path (list (syscat::name res1type)
                             res1uid
                             (syscat::name deprel1)
                             (syscat::name depres1type)
                             depres1uid
                             (syscat::name deprel2)
                             (syscat::name depres2type)
                             depres2uid)))
        (fiveam:is (= 1 (length (syscat::get-resources session
                                                           resourcetype-schema
                                                           this-path))))
        (fiveam:is
          (string= (format nil "~{/~A~}"
                           (list
                             (syscat::name res1type)
                             res1uid
                             (syscat::name deprel1)
                             (syscat::name depres1type)
                             depres1uid
                             (syscat::name deprel2)
                             (syscat::name depres2type)
                             depres2uid))
                   (gethash "ScCanonicalpath" (first (syscat::get-resources session
                                                                                resourcetype-schema
                                                                                this-path))))))
      ;; Clean up the resources we created
      (syscat::log-message :debug ";TEST Clean up the resources for this test")
      (syscat::delete-resource-by-path session
                                           (list (syscat::name res1type) res1uid)
                                           resourcetype-schema
                                           :recursive t)
      (syscat::delete-resource-by-path session
                                           (list res2typename res2uid)
                                           resourcetype-schema
                                           :recursive t)
      (syscat::delete-resource-by-path session
                                           (list (syscat::name res1type)
                                                 res1uid
                                                 (syscat::name deprel1)
                                                 (syscat::name depres1type)
                                                 depres1uid)
                                           resourcetype-schema
                                           :recursive t)
      ;; Remove the temporary schema-version
      (syscat::delete-schema-version session schema-version))
    ;; Clean up the session
    (neo4cl:disconnect session)))

(fiveam:test
  resources-multiple
  :depends-on 'resources-basic
  "Confirm we can retrieve all resources of a given type"
  (let* ((resourcetype (syscat::make-incoming-rtypes :name "Routers"))
         (res1uid "amchitka")
         (res2uid "bikini")
         (res3uid "mururoa")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Set up the fixtures
    (syscat::log-message :info ";TEST Set up the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Install the new resourcetypes and relationship
    (syscat::install-subschema-resourcetype session resourcetype schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Confirm we have no instances of that resource in place now
      (fiveam:is (null (syscat::get-resources
                         session
                         resourcetype-schema
                         (list (syscat::name resourcetype)))))
      ;; Add one of that kind of resource
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name resourcetype)
                                  `(("ScUID" . ,res1uid))
                                  *admin-user*)
      ;; Confirm we now get a list containing exactly that resource
      (let ((result (car (syscat::get-resources
                           session
                           resourcetype-schema
                           (list (syscat::name resourcetype))))))
        (fiveam:is (= 5 (hash-table-count result)))
        (fiveam:is (not (null (gethash "ScCreateddate" result))))
        (fiveam:is (integerp (gethash "ScCreateddate" result)))
        (fiveam:is (not (null (gethash "ScUID" result))))
        (fiveam:is (equal res1uid (gethash "ScUID" result))))
      ;; Add a second of that kind of resource
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name resourcetype)
                                  `(("ScUID" . ,res2uid))
                                  *admin-user*)
      ;; Confirm we now get a list containing both resources
      (let ((result (syscat::get-resources
                      session
                      resourcetype-schema
                      (list (syscat::name resourcetype)))))
        (fiveam:is (equal (list (syscat::sanitise-uid res1uid)
                                (syscat::sanitise-uid res2uid))
                          (sort (mapcar #'(lambda (res)
                                            (gethash "ScUID" res))
                                        result)
                                #'string<)))
        (fiveam:is (equal (syscat::sanitise-uid res1uid)
                          (gethash "ScUID" (first result))))
        (fiveam:is (equal (syscat::sanitise-uid res2uid)
                          (gethash "ScUID" (second result)))))
      ;; Add a third of that kind of resource
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name resourcetype)
                                  `(("ScUID" . ,res3uid))
                                  *admin-user*)
      ;; Confirm we now get a list containing all three resources
      (let ((result (syscat::get-resources
                      session
                      resourcetype-schema
                      (list (syscat::name resourcetype)))))
        (fiveam:is (equal (list (syscat::sanitise-uid res1uid)
                                (syscat::sanitise-uid res2uid)
                                (syscat::sanitise-uid res3uid))
                          (sort (mapcar #'(lambda (res)
                                            (gethash "ScUID" res))
                                        result)
                                #'string<))))
      ;; Delete all the resources we added
      (syscat::delete-resource-by-path
        session
        (list (syscat::name resourcetype) res1uid)
        resourcetype-schema)
      (syscat::delete-resource-by-path
        session
        (list (syscat::name resourcetype) res2uid)
        resourcetype-schema)
      (syscat::delete-resource-by-path
        session
        (list (syscat::name resourcetype) res3uid)
        resourcetype-schema)
      ;; Remove the schema-version we created for this test
      (syscat::delete-schema-version session schema-version))
    ;; Clean up the session
    (neo4cl:disconnect session)))

(fiveam:test
  resources-filtering
  :depends-on 'resources-multiple
  "Filtering while searching for resources"
  (let* ((r1type (syscat::make-incoming-rtypes :name "Routers"))
         (r2type (syscat::make-incoming-rtypes :name "Interfaces"
                                                   :dependent t))
         (rel
           (syscat::make-incoming-rels :name "INTERFACES"
                                           :reltype "dependent"
                                           :source-types (list (syscat::name r1type))
                                           :target-types (list (syscat::name r2type))
                                           :cardinality "1:many"))
         (r1uid "upshot")
         (r1partial "upsh.*")
         (r2uid "eth1/41")
         (r2partial "eth1.*")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Create the fixtures
    (syscat::log-message :info ";TEST Create the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Install the new resourcetypes and relationship
    (syscat::install-subschema
      session
      (syscat::make-incoming-subschema-version
        :name "resources-filtering"
        :resourcetypes (list r1type r2type)
        :relationships (list rel))
      schema-version)
    (syscat::log-message :info ";TEST Fetch the current schema")
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::log-message :info ";TEST Install default resources")
      (syscat::install-default-resources session resourcetype-schema)
      ;; Do the filters do what we expect?
      ;; Store a resource to check on
      (syscat::log-message :info ";TEST Creating the primary resource")
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name r1type)
                                  `(("ScUID" . ,r1uid))
                                  *admin-user*)
      ;; Search for it by type and exact UID
      (syscat::log-message :info ";TEST Searching for the primary resource by name")
      (let ((result (car (syscat::get-resources
                           session
                           resourcetype-schema
                           (list (syscat::name r1type))
                           :filters (syscat::process-filters
                                      `(("ScUID" . ,r1uid))
                                      resourcetype-schema
                                      (syscat::name r1type))))))
        (fiveam:is (= 5 (hash-table-count result)))
        (fiveam:is (not (null (gethash "ScUID" result))))
        (fiveam:is (equal r1uid (gethash "ScUID" result)))
        (fiveam:is (not (null (gethash "ScCreateddate" result)))))
      ;; Search for it by type and partial UID
      (syscat::log-message :info ";TEST Searching for the primary resource by type and partial UID")
      (let ((result (car (syscat::get-resources
                           session
                           resourcetype-schema
                           (list (syscat::name r1type))
                           :filters (syscat::process-filters
                                      `(("ScUID" . ,r1partial))
                                      resourcetype-schema
                                      (syscat::name r1type))))))
        (fiveam:is (= 5 (hash-table-count result)))
        (fiveam:is (not (null (gethash "ScCreateddate" result))))
        (fiveam:is (not (null (gethash "ScUID" result))))
        (fiveam:is (equal r1uid (gethash "ScUID" result))))
      ;; Add a dependent resource to search for
      (syscat::log-message :info ";TEST Creating the secondary resource")
      (syscat::store-dependent-resource
        session
        resourcetype-schema
        (list (syscat::name r1type)
              r1uid
              (syscat::name rel)
              (syscat::name r2type))
        `(("ScUID" . ,r2uid))
        *admin-user*)
      ;; Confirm it's actually there
      (fiveam:is (equal `((,(syscat::name rel)
                            ,(syscat::name r2type)
                            ,(syscat::sanitise-uid r2uid)))
                        (syscat::get-dependent-resources
                          session
                          resourcetype-schema
                          (list (syscat::name r1type) r1uid))))
      ;; Search for it by relationship to parent and exact UID
      (syscat::log-message :info ";TEST Searching for the secondary resource")
      (let ((result (car (syscat::get-resources
                           session
                           resourcetype-schema
                           (list (syscat::name r1type)
                                 r1uid
                                 (syscat::name rel)
                                 (syscat::name r2type))
                           :filters (syscat::process-filters
                                      `(("ScUID" . ,(syscat::sanitise-uid r2uid)))
                                      resourcetype-schema
                                      (syscat::name r2type))))))
        (fiveam:is (= 5 (hash-table-count result)))
        (fiveam:is (not (null (gethash "ScCreateddate" result))))
        (fiveam:is (not (null (gethash "ScUID" result))))
        (fiveam:is (equal (syscat::sanitise-uid r2uid)
                          (gethash "ScUID" result))))
      ;; Search for it by type and partial UID
      (let ((result (car (syscat::get-resources
                           session
                           resourcetype-schema
                           (list (syscat::name r1type)
                                 r1uid
                                 (syscat::name rel)
                                 (syscat::name r2type))
                           :filters (syscat::process-filters
                                      `(("ScUID" . ,r2partial))
                                      resourcetype-schema
                                      (syscat::name r2type))))))
        (fiveam:is (= 5 (hash-table-count result)))
        (fiveam:is (not (null (gethash "ScCreateddate" result))))
        (fiveam:is (not (null (gethash "ScUID" result))))
        (fiveam:is (equal (syscat::sanitise-uid r2uid)
                          (gethash "ScUID" result))))
      ;; Clean up: delete the primary and dependent resources.
      (syscat::log-message :info ";TEST Cleanup: removing the resources")
      (syscat::delete-resource-by-path
        session
        (list (syscat::name r1type) r1uid)
        resourcetype-schema
        :recursive t)
      ;; Delete the temporary schema-version
      (syscat::delete-schema-version session schema-version))
    ;; Clean up the session
    (neo4cl:disconnect session)))

(fiveam:test
  versions-basic
  :depends-on 'resources-basic
  "Basic checks of resource versioning."
  (let* ((rtype "People")
         (uid1 "Travis")
         (attr1name "displayname")
         (attr1val1 "Travis")
         (attr2name "notes")
         (attr2val1 "Kind of a thug.")
         (versioncomment1 "This is a version comment.")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    (syscat::log-message :info ";TEST Set up the fixtures")
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Create a resource
      (syscat::store-resource session
                                  resourcetype-schema
                                  rtype
                                  `(("ScUID" . ,uid1))
                                  *admin-user*)
      ;; Check that it has 1 version now, and that the version is also the current version.
      (syscat::log-message :info ";TEST Initial version check")
      (let ((versions (syscat::list-resource-versions session
                                                          (list rtype uid1)
                                                          resourcetype-schema)))
        (syscat::log-message :debug (format nil "Retrieved version data ~A" versions))
        (fiveam:is (= 1 (length (cdr (assoc :versions versions)))))
        ;; Remember that each version is a dotted pair: version and comment
        (fiveam:is (= (caar (cdr (assoc :versions versions)))
                      (cdr (assoc :current-version versions)))))
      ;; Set the first attribute.
      (syscat::log-message :info ";TEST Add first new version")
      (fiveam:is (integerp (syscat::add-resource-version session
                                                             resourcetype-schema
                                                             (list rtype uid1)
                                                             `((,attr1name . ,attr1val1))
                                                             :comment versioncomment1)))
      ;; Confirm that the resource has 2 versions.
      (let ((versions (syscat::list-resource-versions
                        session
                        (list rtype uid1)
                        resourcetype-schema)))
        (fiveam:is (= 2 (length (cdr (assoc :versions versions)))))
        ;; Confirm that the newer version is now the current one.
        (let ((current-version (cdr (assoc :current-version versions))))
          (fiveam:is (= (apply #'max (mapcar #'car (cdr (assoc :versions versions))))
                        current-version))
          ;; Confirm that the version comment has been correctly recorded
          (fiveam:is (equal versioncomment1
                            (cdr (assoc current-version (cdr (assoc :versions versions))))))))
      ;; Set the current version back to the original
      (syscat::log-message :debug ";TEST Set the current version back to the original")
      (let* ((this-path (list rtype uid1)))
        (fiveam:is
          (syscat::set-resource-version
            session
            resourcetype-schema
            this-path
            (apply #'min
                   (mapcar #'car (cdr (assoc :versions
                                             (syscat::list-resource-versions
                                               session
                                               (list rtype uid1)
                                               resourcetype-schema))))))))
      ;; Set the second attribute
      (syscat::log-message :info ";TEST Add second new version")
      (fiveam:is (integerp
                   (syscat::add-resource-version session
                                                     resourcetype-schema
                                                     (list rtype uid1)
                                                     `((,attr2name . ,attr2val1)))))
      ;; Confirm that we now have 3 versions, and the newest is current
      (let ((versions (syscat::list-resource-versions session
                                                          (list rtype uid1)
                                                          resourcetype-schema)))
        (fiveam:is (= 3 (length (cdr (assoc :versions versions)))))
        ;; Confirm that the newer version is now the current one.
        (fiveam:is (= (apply #'max (mapcar #'car (cdr (assoc :versions versions))))
                      (cdr (assoc :current-version versions)))))
      ;; Delete the resource
      (syscat::log-message :info ";TEST Deleting fixtures")
      (syscat::delete-resource-by-path session
                                           (list rtype uid1)
                                           resourcetype-schema
                                           :recursive t)
      ;; Delete the temporary schema-version
      (syscat::delete-schema-version session schema-version))
    ;; Clean up the session
    (neo4cl:disconnect session)))

(fiveam:test
  character-encoding
  :depends-on 'resources-basic
  "Check handling of non-ASCII characters."
  (let* ((attr1name "fullname")
         (attr1val "Röver Edwárd Petrusky De'Ath the fourth")
         (restype (syscat::make-incoming-rtypes
                    :name "Dogs"
                    :attributes (list (syscat::make-incoming-rtype-attrs
                                        (list :name attr1name)))))
         (uid "Spot")
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Create a resource
    (syscat::log-message :info ";TEST Set up the fixtures")
    ;; Install the core schema in the new schema-version
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    (syscat::install-subschema-resourcetype session restype schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Create a resource with a non-contentious UID
      (syscat::store-resource session
                                  resourcetype-schema
                                  (syscat::name restype)
                                  `(("ScUID" . ,uid))
                                  *admin-user*)
      ;; Add an attribute whose value has non-ASCII characters
      (syscat::log-message :info ";TEST Try to set the attribute")
      (fiveam:is (integerp (syscat::add-resource-version
                             session
                             resourcetype-schema
                             (list (syscat::name restype) uid)
                             `((,attr1name . ,attr1val)))))
      ;; Verify that the same string is returned when we query it
      (syscat::log-message :info ";TEST Verify whether the attribute was set")
      (let ((result (syscat::get-resources
                      session
                      resourcetype-schema
                      (list (syscat::name restype) uid)
                      :attributes (list attr1name))))
        (fiveam:is (= 1 (length result)))
        (fiveam:is (not (null (gethash "ScCanonicalpath" (first result)))))
        (fiveam:is (equal (format nil "/~A/~A"
                                  (syscat::name restype)
                                  (syscat::sanitise-uid uid))
                          (gethash "ScCanonicalpath" (first result))))
        (fiveam:is (not (null (gethash attr1name (first result)))))
        (fiveam:is (equal attr1val (gethash attr1name (first result)))))
      ;; Delete the resource
      (syscat::log-message :info ";TEST Remove the fixtures")
      (syscat::delete-resource-by-path session
                                           (list (syscat::name restype) uid)
                                           resourcetype-schema)
      (syscat::delete-schema-version session schema-version))
    ;; Clean up the session
    (neo4cl:disconnect session)))
