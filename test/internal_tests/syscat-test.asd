;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(asdf:defsystem #:syscat-test
                :serial t
                :license "MIT license"
                :author "Kat Sebastian <kat@electronic-quill.net>"
                :description "Test suite for Syscat"
                :depends-on (#:syscat
                             #:fiveam)
                :components ((:file "package")
                             (:file "syscat-test")
                             (:file "utilities-pure")
                             (:file "utilities-side-effecting")
                             (:file "pure-tests")
                             (:file "bolt-schema")
                             (:file "bolt-side-effecting-resources")
                             (:file "bolt-side-effecting-resources-canonical-paths")
                             (:file "bolt-side-effecting-relationships")
                             (:file "ipam")
                             (:file "sessions")))
