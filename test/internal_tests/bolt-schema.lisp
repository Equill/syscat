;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Test suite for schema functions which interact with the database

(in-package #:syscat-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(fiveam:def-suite
  schema
  :description "Tests for schema functions which interact with the database."
  :in main)

(fiveam:in-suite schema)

(fiveam:test
  schema-parsing
  "Test the schema-parser itself."
  (let ((parsed-schema (syscat::parse-schema-from-alist
                         (json:decode-json-from-source #P"../test/internal_tests/parsing_test_schema.json"))))
    (syscat::log-message :debug ";TEST Reject attributes and relationships with reserved prefixes.")
    (fiveam:is (= 2 (length (syscat::resourcetypes parsed-schema))))
    (fiveam:is (equal "Buildings"
                      (syscat::name (car (syscat::resourcetypes parsed-schema)))))
    (fiveam:is (= 1
                  (length (syscat::attributes (car (syscat::resourcetypes parsed-schema))))))
    (fiveam:is (= 1 (length (syscat::relationships parsed-schema))))
    (fiveam:is (equal "LOCATED_IN"
                      (syscat::name (car (syscat::relationships parsed-schema)))))))

(fiveam:test
  schema-versions
  "Can we create and remove schema versions?"
  (let ((session (neo4cl:establish-bolt-session *bolt-server*)))
    ;; Install a new schema version, and get its ID
    (let ((schema-version (syscat::create-new-schema-version session)))
      (syscat::log-message :debug (format nil "New schema version: ~D" schema-version))
      ;; Is the current version an integer?
      (fiveam:is (integerp (syscat::current-schema-version session)))
      ;; Is it the same as the one we were told we just installed?
      (fiveam:is (equal schema-version (syscat::current-schema-version session)))
      ;; Check whether list-schema-versions works
      (fiveam:is (every
                   #'integerp
                   (cdr
                     (assoc :VERSIONS (syscat::list-schema-versions session)))))
      ;; Clean up the schema version we installed.
      ;; This action should return nil, so that's what we're testing for.
      (fiveam:is (null (syscat::delete-schema-version session schema-version))))
    (neo4cl:disconnect session)))

(fiveam:test
  schema-version-rollbacks
  "Basic operations on schema versions"
  (let* ((session (neo4cl:establish-bolt-session *bolt-server*))
         ;; version-zero is to ensure there _is_ a prior version,
         ;; because otherwise some of these tests break when run against a clean database.
         (version-zero (syscat::create-new-schema-version session))
         (original-version-list (syscat::list-schema-versions session))
         (original-version (syscat::current-schema-version session)))
    (syscat::log-message :debug
                             (format nil ";DEBUG version-list at start of test: ~A"
                                     original-version-list))
    ;; Create a new schema version
    (syscat::log-message :info ";TEST Add a schema version")
    (sleep 2) ;; Version timestamps have 1-second resolution, so prevent collisions
    (syscat::create-new-schema-version session)
    ;; Do we have one more version than before?
    (fiveam:is (= (+ (length (assoc :VERSIONS original-version-list)) 1)
                  (length (assoc :VERSIONS (syscat::list-schema-versions session)))))
    (let ((new-current-version (syscat::current-schema-version session)))
      ;; Is the new current version newer than the previous one?
      (fiveam:is (> new-current-version original-version))
      (syscat::log-message :info ";TEST Set schema version back and forward")
      ;; Set the version back to the previous one
      (fiveam:is (null (syscat::set-current-schema-version session original-version)))
      (fiveam:is (= original-version (syscat::current-schema-version session)))
      ;; Roll forward to the newer one
      (fiveam:is (null (syscat::set-current-schema-version session new-current-version)))
      (fiveam:is (= new-current-version (syscat::current-schema-version session)))
      ;; Delete the new version
      (syscat::log-message :info ";TEST Delete a schema version")
      (fiveam:is (null (syscat::delete-schema-version session new-current-version)))
      ;; Are we back to the original current-version?
      (syscat::log-message :info ";TEST Check whether we´re back to the original version")
      (fiveam:is (equal original-version (syscat::current-schema-version session)))
      (syscat::log-message
        :info
        ";TEST Check whether we´re back to the original number of versions")
      ;; Are we back to the original number of versions?
      (fiveam:is (= (length (assoc :VERSIONS original-version-list))
                    (length (assoc :VERSIONS (syscat::list-schema-versions session))))))
    ;; Tidy up
    (syscat::log-message :info ";INFO Post-test cleanup")
    (when (member version-zero (cdr (assoc :VERSIONS (syscat::list-schema-versions session))))
      (syscat::delete-schema-version session version-zero))
    (neo4cl:disconnect session)))

(fiveam:test
  install-core-schema
  "Annoyingly necessary to make the rest work."
  :depends-on 'schema-version-rollbacks
  (let ((session (neo4cl:establish-bolt-session *bolt-server*)))
    (let ((schema-version (syscat::create-new-schema-version session)))
      ;; Install the core schema in the new schema-version
      (fiveam:is (syscat::install-subschema
                   session
                   syscat::*core-schema* schema-version))
      ;; Remember to remove it
      (syscat::delete-schema-version session schema-version))
    (neo4cl:disconnect session)))

(fiveam:test
  process-filter
  "Check the filtering of GET query parameters."
  :depends-on 'install-core-schema
  (let* ((session (neo4cl:establish-bolt-session *bolt-server*))
         ;; Create a new schema-version
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Install a test-specific resourcetype
    (syscat::log-message :debug ";TEST: Install EnumTest resourcetype")
    (syscat::install-subschema-resourcetype
      session
      (syscat::make-incoming-rtypes
        :name "EnumTest"
        :description "For testing enum values"
        :attributes (list (syscat::make-incoming-rtype-attrs
                            (list :type "varchar"
                                  :name "carl"
                                  :maxlength 16
                                  :readonly nil
                                  :description "It needed a name, alright?"
                                  :values '("one" "two" "three")))))
      schema-version)
    ;; Fetch the schema into memory
    (syscat::log-message :debug ";TEST: Fetch the current schema")
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Now delete the newly-created schema version, because we don't need it any more
      (syscat::log-message :debug ";TEST: Delete the schema we created.")
      (syscat::delete-schema-version session schema-version)
      ;; Now disconnect from the server and release the session resources
      (neo4cl:disconnect session)
      ;; On with the tests
      (syscat::log-message :DEBUG ";TEST: null filter")
      (fiveam:is (null (syscat::process-filter '() resourcetype-schema "any")))
      (syscat::log-message :DEBUG ";TEST: regex filter")
      (fiveam:is (equal "v.displayname =~ '.*foo.*'"
                        (syscat::process-filter '("displayname" . ".*foo.*")
                                                    resourcetype-schema "People")))
      (syscat::log-message :DEBUG ";TEST: negated regex filter")
      (fiveam:is (equal "NOT v.displayname =~ '.*foo.*'"
                        (syscat::process-filter '("displayname" . "!.*foo.*")
                                                    resourcetype-schema "People")))
      (syscat::log-message :DEBUG ";TEST: existence filter")
      (fiveam:is (equal "exists(v.displayname)"
                        (syscat::process-filter '("displayname" . "exists")
                                                    resourcetype-schema "People")))
      (syscat::log-message :DEBUG ";TEST: negated existence filter")
      (fiveam:is (equal "NOT exists(v.displayname)"
                        (syscat::process-filter '("displayname" . "!exists")
                                                    resourcetype-schema "People")))
      (syscat::log-message :DEBUG ";TEST: enum filter")
      (fiveam:is (equal "v.carl IN ['one']"
                        (syscat::process-filter '("carl" . "one")
                                                    resourcetype-schema "EnumTest")))
      (fiveam:is (equal "v.carl IN ['one', 'three']"
                        (syscat::process-filter '("carl" . "one,three")
                                                    resourcetype-schema "EnumTest")))
      ;; This next one _should_ throw some kind of error, because it's not a valid value.
      ;; However, that will have to wait until I figure out how to usefull report on invalid filters.
      (fiveam:is (equal "v.carl IN ['four']"
                        (syscat::process-filter '("carl" . "four")
                                                    resourcetype-schema "EnumTest")))
      (syscat::log-message :DEBUG ";TEST: text filter")
      (fiveam:is (equal "v.displayname = 'blah'"
                        (syscat::process-filter '("displayname" . "blah")
                                                    resourcetype-schema "People")))
      (syscat::log-message :DEBUG ";TEST: negated text filter")
      (fiveam:is (equal "NOT v.displayname = 'blah'"
                        (syscat::process-filter '("displayname" . "!blah")
                                                    resourcetype-schema "People")))
      (syscat::log-message :DEBUG ";TEST: outbound filters")
      (fiveam:is (equal "(n)-[:HAS_CREATOR]->(:People { ScUID: 'ScAdmin' })"
                        (syscat::process-filter '("SCoutbound" . "/HAS_CREATOR/People/ScAdmin")
                                                    resourcetype-schema
                                                    "People")))
      (fiveam:is (equal "(n)-[:THINGS]->(:Things { ScUID: 'this' })-[:RELATES_TO]->(:Things { ScUID: 'that' })"
                        (syscat::process-filter
                          '("SCoutbound" . "/THINGS/Things/this/RELATES_TO/Things/that")
                          resourcetype-schema
                          "People")))
      (syscat::log-message :DEBUG ";TEST: negated outbound filters")
      (fiveam:is (equal "NOT (n)-[:HAS_CREATOR]->(:People { ScUID: 'ScAdmin' })"
                        (syscat::process-filter '("SCoutbound" . "!/HAS_CREATOR/People/ScAdmin")
                                                    resourcetype-schema
                                                    "People")))
      (fiveam:is (equal "NOT (n)-[:THINGS]->(:Things { ScUID: 'this' })-[:RELATES_TO]->(:Things { ScUID: 'that' })"
                        (syscat::process-filter
                          '("SCoutbound" . "!/THINGS/Things/this/RELATES_TO/Things/that")
                          resourcetype-schema
                          "People")))
      (syscat::log-message :DEBUG ";TEST: inbound filters")
      (fiveam:is (equal "(:Things { ScUID: 'this' })-[:RELATES_TO]->(n)"
                        (syscat::process-filter '("SCinbound" . "/Things/this/RELATES_TO")
                                                    resourcetype-schema
                                                    "People")))
      (syscat::log-message :DEBUG ";TEST: inbound filters with wildcards")
      (fiveam:is (equal "(:Things)-[:RELATES_TO]->(n)"
                        (syscat::process-filter '("SCinbound" . "/Things/*/RELATES_TO")
                                                    resourcetype-schema
                                                    "People")))
      (syscat::log-message :DEBUG ";TEST: inbound filters with misplaced wildcards")
      (fiveam:is (equal "(:Things)-[:]->(n)"
                        (syscat::process-filter '("SCinbound" . "/Things/*/*")
                                                    resourcetype-schema
                                                    "People")))
      (fiveam:is (equal "(:Things { ScUID: 'that' })-[:]->(n)"
                        (syscat::process-filter '("SCinbound" . "/Things/that/*")
                                                    resourcetype-schema
                                                    "People")))
      (fiveam:is (equal "(:Things { ScUID: 'this' })-[:SUBTHINGS]->(:Things { ScUID: 'that' })-[:RELATES_TO]->(n)"
                        (syscat::process-filter
                          '("SCinbound" . "/Things/this/SUBTHINGS/Things/that/RELATES_TO")
                          resourcetype-schema
                          "People")))
      (syscat::log-message :DEBUG ";TEST: negated inbound filters")
      (fiveam:is (equal "NOT (:Things { ScUID: 'this' })-[:RELATES_TO]->(n)"
                        (syscat::process-filter '("SCinbound" . "!/Things/this/RELATES_TO")
                                                    resourcetype-schema
                                                    "People")))
      (fiveam:is (equal "NOT (:Things { ScUID: 'this' })-[:SUBTHINGS]->(:Things { ScUID: 'that' })-[:RELATES_TO]->(n)"
                        (syscat::process-filter
                          '("SCinbound" . "!/Things/this/SUBTHINGS/Things/that/RELATES_TO")
                          resourcetype-schema
                          "People"))))))
