# Basic configuration for a development instance of Syscat

export COOKIE_DOMAIN='sctest.onfire.onice'
export FILES_LOCATION="/tmp/syscat-files/"
export FILES_TEMP_LOCATION="/tmp/syscat-files-tmp/"
export SCHEMAPATH="/home/kat/devel/syscat/syscat/schemas/"
export TEMPLATE_PATH="/home/kat/devel/syscat/syscat/src/templates/"
# Uncomment the following line to enable debug-logging
#export DEBUG="true"

# Neo4j configs
export NEO4J_HOSTNAME="127.0.0.1"
export NEO4J_PORT=7805
export NEO4J_USER='neo4j'
export NEO4J_PASSWORD='wombatboots'

# Auth policy
#
## Other values are 'open' and 'readonly'
export ACCESS_POLICY='open'
