# Start up Neo4j with a clean set of data directories
# and a config specific to running internal tests of Restagraph


# Avoid time wasted in hunting down typos
DATADIR=/tmp/sc_internal_test

# Ensure a clean environment by nuking it and starting again
sudo rm -rf $DATADIR
mkdir $DATADIR
#sudo chown neo4j $DATADIR

# Make sure Neo4j has the same idea about its base directory that I do
export NEO4J_HOME=/opt/neo4j

# Tell Neo4j to look in this directory for its configs
export NEO4J_CONF=/home/kat/devel/restagraph/restagraph/test/internal_tests/

# Set the initial password
/opt/neo4j/bin/neo4j-admin dbms set-initial-password wombatboots

# Start it up
/opt/neo4j/bin/neo4j start
