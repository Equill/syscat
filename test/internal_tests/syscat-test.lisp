;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Test suite for all of Syscat
;;;;
;;;; Beware: it currently only tests _expected_ cases,
;;;; and does not test edge-cases or wrong input.

(in-package #:syscat-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(fiveam:def-suite main)
(fiveam:in-suite main)


;; Main DB configs

(defparameter *bolt-auth-basic*
  (make-instance 'neo4cl:bolt-auth-basic
                 :username "neo4j"
                 :password "wombatboots"))

(defparameter *bolt-auth-none*
  (make-instance 'neo4cl:bolt-auth-none
                 :username "neo4j"))

(defparameter *bolt-server*
  (syscat::make-default-bolt-server))


;; Auth+session server configs

(defparameter *auth-server*
  (cond ((not (member (sb-ext:posix-getenv "ACCESS_POLICY")
                      '("write-authenticated"
                        "authenticated-only")
                      :test #'equal))
         nil)
        ((and (sb-ext:posix-getenv "AUTH_NEO4J_HOSTNAME")
              (or (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD_FILE")
                  (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD")))
         (make-instance
           'syscat::auth-bolt-server
           :hostname (sb-ext:posix-getenv "AUTH_NEO4J_HOSTNAME")
           :port (if (sb-ext:posix-getenv "AUTH_NEO4J_PORT")
                     (parse-integer (sb-ext:posix-getenv "AUTH_NEO4J_PORT"))
                     7687)
           :domain (sb-ext:posix-getenv "COOKIE_DOMAIN")
           ;; Adapt to the authentication scheme in effect
           :auth-token (make-instance
                         'neo4cl:bolt-auth-basic
                         :username (or (sb-ext:posix-getenv "AUTH_NEO4J_USER")
                                       "neo4j")
                         ;; Again, check for the file _first_.
                         ;; If that's specified, it takes precedence over one supplied via env var.
                         :password (if (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD_FILE")
                                       (with-open-file (infile (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD_FILE")
                                                               :direction :input)
                                         (read-line infile))
                                       (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD")))))
        ((and (sb-ext:posix-getenv "LDAP_HOST")
              (sb-ext:posix-getenv "LDAP_PORT")
              (or (sb-ext:posix-getenv "LDAP_BASE_DN")
                  (sb-ext:posix-getenv "COOKIE_DOMAIN")))
         (make-instance 'syscat::ldap-server
                        :host (sb-ext:posix-getenv "LDAP_HOST")
                        :port (parse-integer (sb-ext:posix-getenv "LDAP_PORT"))
                        :base-dn (or (sb-ext:posix-getenv "LDAP_BASE_DN")
                                     (format nil "~{dc=~A~^,~}"
                                             (remove-if (lambda (foo) (equal "" foo))
                                                        (cl-ppcre:split
                                                          "\\."
                                                          (sb-ext:posix-getenv "COOKIE_DOMAIN")))))))
        (t (error "Authentication profile specified, but no auth-server configs defined")))
  "An auth-server object, created according to the environment variables specified. Prioritises Neo4j over LDAP/Redis, if both sets of variables are defined.")

(defparameter *session-server*
  (when (member (sb-ext:posix-getenv "ACCESS_POLICY")
                '("write-authenticated"
                  "authenticated-only")
                :test #'equal)
    (if
       (and (sb-ext:posix-getenv "AUTH_NEO4J_HOSTNAME")
            (or (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD_FILE")
                (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD")))
       *auth-server*
       (let ((redis-server (make-instance 'redis-server)))
         ;; Now modify the host, if that was specified
         (when (sb-ext:posix-getenv "REDIS_HOST")
           (setf (syscat::host redis-server) (sb-ext:posix-getenv "REDIS_HOST")))
         (when (sb-ext:posix-getenv "REDIS_PORT")
           (setf (syscat::port redis-server) (sb-ext:posix-getenv "REDIS_PORT")))
         ;; Return the object
         redis-server))))

(defparameter *admin-user* "ScAdmin")
(defparameter *admin-password* "thisisabadpassword")
