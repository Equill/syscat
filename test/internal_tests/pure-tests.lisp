;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Test suite for pure code.
;;;; I.e, functions and methods without side-effects.

(in-package #:syscat-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(fiveam:def-suite
  pure
  :description "Tests for pure code, i.e. code with no side-effects."
  :in main)

(fiveam:in-suite pure)


(fiveam:test
  schema-hash-basic
  "Basic tests of functions and methods on a schema implemented as a hash-table."
  (let ((schema (syscat::make-schema-hash-table)))
    (setf (gethash "foo" schema)
          (syscat::make-instance 'syscat::schema-rtypes
                                     :name "foo"
                                     :dependent nil
                                     :description nil
                                     :attributes nil
                                     :relationships nil))
    (setf (gethash "bar" schema)
          (syscat::make-instance 'syscat::schema-rtypes
            :name "bar"
            :dependent nil
            :description "For testing attribute validation"
            :attributes (list (make-instance 'syscat::schema-rtype-attr-varchar
                                :name "baz"
                                :description "Valid attribute."))))
    ;; Confirm it isn't dependent
    (fiveam:is (null (syscat::dependent (gethash "foo" schema))))))

(fiveam:test
  validate-attributes
  "Check the validation of attribute values,
  as supplied by a client for updating a resource instance."
  (let ((defined (list (make-instance 'syscat::schema-rtype-attr-varchar
                                      :name "status"
                                      :description "Task status."
                                      :attrvalues '("idea"
                                                    "active"
                                                    "waiting"
                                                    "scheduled"
                                                    "done"
                                                    "cancelled"))
                       (make-instance 'syscat::schema-rtype-attr-text
                                      :name "description"
                                      :description "More details about the task.")
                       (make-instance 'syscat::schema-rtype-attr-integer
                                      :name "numeric"
                                      :description "Something something numbers"
                                      :minimum -5
                                      :maximum 20)
                       (make-instance 'syscat::schema-rtype-attr-boolean
                                      :name "truefalse"
                                      :description "Well, it's one of those."))))
    ;; Simple check for no attributes at all
    (fiveam:is (equal '(nil nil nil)
                      (syscat::validate-attributes '() defined)))
    ;; Varchar
    ;; Simple check for valid attribute
    (fiveam:is (equalp '((("status" . "active")) nil nil)
                       (syscat::validate-attributes '(("status" . "active")) defined)))
    ;; Simple check for invalid attribute
    (fiveam:is (equalp '(nil (("foo" . "active")) nil)
                       (syscat::validate-attributes '(("foo" . "active")) defined)))
    ;; Simple check for invalid value
    (fiveam:is (equalp '(nil nil (("status" . "inactive")))
                       (syscat::validate-attributes '(("status" . "inactive")) defined)))
    ;; Combo-check for enums
    (fiveam:is (equalp '((("status" . "active") ("description" . "The legends were true."))
                         (("foo" . "active"))
                         (("status" . "inactive")))
                       (syscat::validate-attributes '(("status" . "active")
                                                          ("foo" . "active")
                                                          ("description" . "The legends were true.")
                                                          ("status" . "inactive"))
                                                        defined)))
    ;; Text
    ;; Basic validation
    (fiveam:is (equalp '((("description" . "I love kung foooooo!")) nil nil)
                       (syscat::validate-attributes '(("description" . "I love kung foooooo!")) defined)))
    ;; Integer
    ;; Basic validation
    (fiveam:is (equalp '((("numeric" . 0)) nil nil)
                       (syscat::validate-attributes '(("numeric" . 0)) defined)))
    ;; Basic validation with string-parsing
    (fiveam:is (equalp '((("numeric" . 0)) nil nil)
                       (syscat::validate-attributes '(("numeric" . "0")) defined)))
    ;; Enforce the minimum
    (fiveam:is (equalp '(nil nil (("numeric" . -10)))
                       (syscat::validate-attributes '(("numeric" . -10)) defined)))
    (fiveam:is (equalp '(nil nil (("numeric" . "-10")))
                       (syscat::validate-attributes '(("numeric" . "-10")) defined)))
    ;; Enforce the maximum
    (fiveam:is (equalp '(nil nil (("numeric" . 30)))
                       (syscat::validate-attributes '(("numeric" . 30)) defined)))
    ;; Boolean
    ;; Basic validation
    (fiveam:is (equalp '((("truefalse" . :false)) nil nil)
                       (syscat::validate-attributes '(("truefalse" . nil)) defined)))
    (fiveam:is (equalp '((("truefalse" . :true)) nil nil)
                       (syscat::validate-attributes '(("truefalse" . t)) defined)))
    ;; Reject invalid values
    (fiveam:is (equalp '(nil nil (("truefalse" . "true")))
                       (syscat::validate-attributes '(("truefalse" . "true")) defined)))))


(fiveam:test
  list-to-plist
  "Make sure list-to-plist does what we expect."
  (syscat::log-message :debug ";TEST Null input")
  (fiveam:is (null (syscat::list-to-plist '()))))

(fiveam:test
  re-order-path
  "Ensure that re-order-path does what we expect."
  (fiveam:is (null (syscat::re-order-path nil)))
  (fiveam:is (equal '("type" "uid" "rel")
                    (syscat::re-order-path '("rel" "type" "uid"))))
  (fiveam:is (equal '("type1" "uid1" "rel1" "type2" "uid2" "rel2")
                    (syscat::re-order-path '("rel2" "type2" "uid2"
                                                 "rel1" "type1" "uid1")))))

(fiveam:test
  generate-subnet-path
  (fiveam:is (equal '("Organisations" "org1")
                    (syscat::generate-subnet-path :org "org1")))
  (fiveam:is (equal '("Organisations" "org1" "CONTAINS_VRF_GROUPS" "VrfGroups" "VRF1")
                    (syscat::generate-subnet-path :org "org1"
                                                      :vrf "VRF1")))
  (fiveam:is (equal '("Organisations" "org1"
                      "CONTAINS_IPV4_SUBNETS" "Ipv4Subnets" "172.16.0.0_12")
                    (syscat::generate-subnet-path
                      :org "org1"
                      :subnets (list (ipaddress:make-ipv4-subnet "172.16.0.0/12")))))
  (fiveam:is (equal '("Organisations" "org1"
                      "CONTAINS_IPV4_SUBNETS" "Ipv4Subnets" "172.16.0.0_12"
                      "CONTAINS_IPV4_SUBNETS" "Ipv4Subnets" "172.16.2.0_24")
                    (syscat::generate-subnet-path
                      :org "org1"
                      :subnets (list (ipaddress:make-ipv4-subnet "172.16.0.0/12")
                                     (ipaddress:make-ipv4-subnet "172.16.2.0/24")))))
  (fiveam:is (equal '("Organisations" "org1"
                      "CONTAINS_IPV4_SUBNETS" "Ipv4Subnets" "192.0.2.0_24"
                      "CONTAINS_IPV4_ADDRESSES" "Ipv4Addresses")
                    (syscat::generate-subnet-path
                      :org "org1"
                      :subnets (list (ipaddress:make-ipv4-subnet "192.0.2.0/24"))
                      :suffix '("CONTAINS_IPV4_ADDRESSES" "Ipv4Addresses"))))
  (fiveam:is (equal '("Organisations" "org1"
                      "CONTAINS_IPV6_SUBNETS" "Ipv6Subnets" "00fe0000000000000000000000000000_64")
                    (syscat::generate-subnet-path
                      :org "org1"
                      :subnets (list (ipaddress:make-ipv6-subnet "fe:00/64"))))))
