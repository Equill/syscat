;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Test suite for side-effecting code pertaining to relationships.

(in-package #:syscat-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(fiveam:def-suite
  ipam
  :description "IPAM functionality"
  :in main)

(fiveam:in-suite ipam)


;; Database interactions

(fiveam:test
  ipam-ipv4-subnets-no-vrf
  "Basic create/read/delete test on IPv4 subnets. Depth of 1, no VRF"
  (let* ((org "internet")
         (subnet1 (ipaddress:make-ipv4-subnet "172.16.0.0/12"))
         (policy :ALLOW-ALL)
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      (syscat::log-message :debug ";TEST Install the fixtures")
      ;; Install the default set of resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Confirm the fixtures aren't already present
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org))))
      ;; Add the fixtures
      (syscat::store-resource session
                                  resourcetype-schema
                                  "Organisations"
                                  `(("ScUID" . ,org))
                                  *admin-user*)
      (syscat::log-message :debug ";TEST Add a top-level subnet")
      ;; Add a top-level subnet; this should return NIL.
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet1 policy))
      ;; Confirm the subnet is there
      (syscat::log-message :debug ";TEST Confirm the top-level subnet")
      (fiveam:is (syscat::find-subnet session resourcetype-schema org subnet1))
      ;; Remove the subnet
      (syscat::log-message :debug ";TEST Remove the top-level subnet")
      (fiveam:is (not (syscat::delete-subnet session
                                                 org
                                                 subnet1
                                                 resourcetype-schema
                                                 :recursive t)))
      ;; Confirm the subnet is gone
      (syscat::log-message :debug ";TEST Confirm the top-level subnet is gone")
      (fiveam:is (not (syscat::find-subnet session resourcetype-schema org subnet1)))
      ;; Remove the fixtures
      (syscat::log-message :debug ";TEST Remove the fixtures")
      (syscat::delete-resource-by-path session
                                           (list "Organisations" org)
                                           resourcetype-schema
                                           :recursive t)
      (syscat::delete-schema-version session schema-version))
    (neo4cl:disconnect session)))

(fiveam:test
  ipam-ipv6-subnets-no-vrf
  "Basic create/read/delete test on IPv6 subnets. Depth of 1, no VRF"
  (let* ((org "internet")
         (subnet1 (ipaddress:make-ipv6-subnet "2001:db8::/32"))
         (policy :ALLOW-ALL)
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    (syscat::log-message :debug ";TEST Install the fixtures")
    ;; Install the core schema
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default set of resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Confirm the fixtures aren't already present
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org))))
      ;; Add the fixtures
      (syscat::store-resource session
                                  resourcetype-schema
                                  "Organisations"
                                  `(("ScUID" . ,org))
                                  *admin-user*)
      ;; Add a top-level subnet
      (syscat::log-message :debug ";TEST Add a top-level subnet")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet1 policy))
      ;; Confirm the subnet is there
      (syscat::log-message :debug ";TEST Confirm presence of the top-level subnet")
      (fiveam:is (syscat::find-subnet session resourcetype-schema org subnet1))
      ;; Remove the subnet
      (syscat::log-message :debug ";TEST Remove the top-level subnet")
      (fiveam:is (not (syscat::delete-subnet session org subnet1 resourcetype-schema)))
      ;; Confirm the subnet is gone
      (syscat::log-message :debug ";TEST Confirm absence of the top-level subnet")
      (fiveam:is (not (syscat::find-subnet session resourcetype-schema org subnet1)))
      ;; Remove the fixtures
      (syscat::log-message :debug ";TEST Remove the fixtures")
      (syscat::delete-resource-by-path session
                                           (list "Organisations" org)
                                           resourcetype-schema
                                           :recursive t)
      (syscat::delete-schema-version session schema-version))
    (neo4cl:disconnect session)))

(fiveam:test
  ipam-ipv4-subnets-one-vrf
  "Basic create/read/delete test on IPv4 subnets. Depth of 1, one VRF"
  (let*
    ((org "internet")
     (vrf "red")
     (subnet1 (ipaddress:make-ipv4-subnet "172.16.0.0/12"))
     (policy :ALLOW-ALL)
     (session (neo4cl:establish-bolt-session *bolt-server*))
     (schema-version (syscat::create-new-schema-version session)))
    (syscat::log-message :debug ";TEST Set up fixtures")
    ;; Install the core schema
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Now fetch the schema
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default set of resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Confirm the fixtures aren't already present
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org))))
      ;; Add the fixtures
      (syscat::log-message :debug ";TEST Set up initial resources")
      (syscat::store-resource session
                                  resourcetype-schema
                                  "Organisations"
                                  `(("ScUID" . ,org))
                                  *admin-user*)
      (syscat::store-dependent-resource
        session
        resourcetype-schema
        (list "Organisations" org "CONTAINS_VRF_GROUPS" "VrfGroups")
        `(("ScUID" . ,vrf))
        *admin-user*)
      ;; Add a top-level subnet; this should return NIL.
      (syscat::log-message :debug ";TEST Add top-level subnet")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet1 policy :vrf vrf))
      ;; Confirm the subnet is there
      (syscat::log-message :debug ";TEST Confirm top-level subnet")
      (fiveam:is (syscat::find-subnet session resourcetype-schema org subnet1 :vrf vrf))
      ;; Remove the subnet
      (syscat::log-message :debug ";TEST Remove top-level subnet")
      (fiveam:is (null (syscat::delete-subnet session org subnet1 resourcetype-schema :vrf vrf)))
      ;; Confirm the subnet is gone
      (syscat::log-message :debug ";TEST Confirm top-level subnet is gone")
      (fiveam:is (null (syscat::find-subnet session resourcetype-schema org subnet1 :vrf vrf)))
      (syscat::log-message :debug ";TEST Remove fixtures")
      ;; Remove the fixtures
      (syscat::delete-resource-by-path session
                                           (list "Organisations" org)
                                           resourcetype-schema
                                           :recursive t))
    ;; Clean up the mess
    (syscat::delete-schema-version session schema-version)
    (neo4cl:disconnect session)))

(fiveam:test
  ipam-ipv6-subnets-one-vrf
  "Basic create/read/delete test on IPv6 subnets. Depth of 1, one VRF"
  (let*
    ((org "internet")
     (vrf "red")
     (subnet1 (ipaddress:make-ipv6-subnet "2001:db8::/32"))
     (policy :ALLOW-ALL)
     (session (neo4cl:establish-bolt-session *bolt-server*))
     (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Now fetch the schema
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default set of resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Confirm the fixtures aren't already present
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org))))
      ;; Add the fixtures
      (syscat::store-resource session
                                  resourcetype-schema
                                  "Organisations"
                                  `(("ScUID" . ,org))
                                  *admin-user*)
      (syscat::store-dependent-resource
        session
        resourcetype-schema
        (list "Organisations" org "CONTAINS_VRF_GROUPS" "VrfGroups")
        `(("ScUID" . ,vrf))
        *admin-user*)
      ;; Add a top-level subnet; this should return NIL.
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet1 policy :vrf vrf))
      ;; Confirm the subnet is there
      (fiveam:is (syscat::find-subnet session resourcetype-schema org subnet1 :vrf vrf))
      ;; Remove the subnet
      (fiveam:is (null (syscat::delete-subnet session org subnet1 resourcetype-schema :vrf vrf)))
      ;; Confirm the subnet is gone
      (fiveam:is (null (syscat::find-subnet session resourcetype-schema org subnet1 :vrf vrf)))
      ;; Remove the fixtures
      (syscat::delete-resource-by-path session
                                           (list "Organisations" org)
                                           resourcetype-schema
                                           :recursive t))
    ;; Clean up the mess
    (syscat::delete-schema-version session schema-version)
    (neo4cl:disconnect session)))

(fiveam:test
  ipam-ipv4-subnets-2-levels-no-vrf
  "Create/read/delete tests on nested IPv4 subnets directly under an organisation."
  (let* ((org "testco42")
         (subnet1 (ipaddress:make-ipv4-subnet "172.16.0.0/12"))
         (subnet2 (ipaddress:make-ipv4-subnet "172.18.0.0/23"))
         (policy :ALLOW-ALL)
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Now fetch the schema
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default set of resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Confirm the fixtures aren't already present
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org))))
      ;; Add the fixtures
      (syscat::log-message :info ";TEST Creating the fixtures")
      (syscat::store-resource session
                                  resourcetype-schema
                                  "Organisations"
                                  `(("ScUID" . ,org))
                                  *admin-user*)
      ;; Add a top-level subnet; this should return NIL.
      (syscat::log-message :info ";TEST Add a top-level subnet")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet1 policy))
      ;; Confirm the subnet is there
      (syscat::log-message :info ";TEST Confirm the top-level subnet is present")
      (fiveam:is (syscat::find-subnet session resourcetype-schema org subnet1))
      ;; Add another subnet
      (syscat::log-message :info ";TEST Add a second-level subnet")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet2 policy))
      ;; Confirm that's also there
      (syscat::log-message :info ";TEST Confirm the second-level subnet is present")
      (fiveam:is (syscat::find-subnet session resourcetype-schema org subnet2))
      ;; Remove the second subnet
      (syscat::log-message :info ";TEST Delete the second-level subnet")
      (fiveam:is (null(syscat::delete-subnet session org subnet2 resourcetype-schema)))
      ;; Remove the top-level subnet
      (syscat::log-message :info ";TEST Delete the top-level subnet")
      (fiveam:is (null(syscat::delete-subnet session org subnet1 resourcetype-schema)))
      ;; Confirm the top-level subnet is gone
      (fiveam:is (null (syscat::find-subnet session resourcetype-schema org subnet1)))
      ;; Remove the fixtures
      (syscat::log-message :info ";TEST Deleting the fixtures")
      (syscat::delete-resource-by-path session
                                           (list "Organisations" org)
                                           resourcetype-schema
                                           :recursive t))
    ;; Clean up the mess
    (syscat::delete-schema-version session schema-version)
    (neo4cl:disconnect session)))

(fiveam:test
  ipam-ipv6-subnets-2-levels-no-vrf
  "Create/read/delete tests on nested IPv6 subnets directly under an organisation."
  (let* ((org "testco62")
         (subnet1 (ipaddress:make-ipv6-subnet "2001:db8::/32"))
         (subnet2 (ipaddress:make-ipv6-subnet "2001:db8:3456::/48"))
         (policy :ALLOW-ALL)
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Now fetch the schema
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default set of resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Confirm the fixtures aren't already present
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org))))
      ;; Add the fixtures
      (syscat::log-message :info ";TEST Creating the fixtures.")
      (syscat::store-resource session
                                  resourcetype-schema
                                  "Organisations"
                                  `(("ScUID" . ,org))
                                  *admin-user*)
      ;; Add a top-level subnet; this should return NIL.
      (syscat::log-message :info ";TEST Add a top-level subnet.")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet1 policy))
      ;; Confirm the subnet is there
      (syscat::log-message :info ";TEST Confirm the top-level subnet is present.")
      (fiveam:is (syscat::find-subnet session resourcetype-schema org subnet1))
      ;; Add another subnet
      (syscat::log-message :info ";TEST Add a second-level subnet.")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet2 policy))
      ;; Confirm that's also there
      (syscat::log-message :info ";TEST Confirm the second-level subnet is present.")
      (fiveam:is (syscat::find-subnet session resourcetype-schema org subnet2))
      ;; Remove the second subnet
      (syscat::log-message :info ";TEST Delete the second-level subnet.")
      (fiveam:is (null(syscat::delete-subnet session org subnet2 resourcetype-schema)))
      ;; Remove the top-level subnet
      (syscat::log-message :info ";TEST Delete the top-level subnet.")
      (fiveam:is (null(syscat::delete-subnet session org subnet1 resourcetype-schema)))
      ;; Confirm the top-level subnet is gone
      (fiveam:is (null (syscat::find-subnet session resourcetype-schema org subnet1)))
      ;; Remove the fixtures
      (syscat::log-message :info ";TEST Deleting the fixtures.")
      (syscat::delete-resource-by-path session
                                           (list "Organisations" org)
                                           resourcetype-schema
                                           :recursive t))
    ;; Clean up the mess
    (syscat::delete-schema-version session schema-version)
    (neo4cl:disconnect session)))

(fiveam:test
  ipam-ipv4-subnets-3-levels-no-vrf
  "Create/read/delete tests on nested IPv4 subnets directly under an organisation."
  (let* ((org "testco43")
         (subnet1 (ipaddress:make-ipv4-subnet "172.16.0.0/12"))
         (subnet2 (ipaddress:make-ipv4-subnet "172.16.19.0/24"))
         (subnet3 (ipaddress:make-ipv4-subnet "172.16.18.0/23"))
         (policy :ALLOW-ALL)
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Now fetch the schema
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default set of resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Confirm the fixtures aren't already present
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org))))
      ;; Add the fixtures
      (syscat::log-message :info ";TEST Creating the fixtures.")
      (syscat::store-resource session
                                  resourcetype-schema
                                  "Organisations"
                                  `(("ScUID" . ,org))
                                  *admin-user*)
      ;; Add a top-level subnet; this should return NIL.
      (syscat::log-message :info ";TEST Add a top-level subnet.")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet1 policy))
      ;; Confirm the subnet is there
      (syscat::log-message :info ";TEST Confirm the top-level subnet is present.")
      (fiveam:is (equal (list (syscat::make-subnet-uid subnet1))
                        (mapcar #'syscat::make-subnet-uid
                                (syscat::find-subnet session resourcetype-schema org subnet1))))
      ;; Add a second subnet
      (syscat::log-message :info ";TEST Add a second-level subnet.")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet2 policy))
      ;; Confirm that's also there
      (syscat::log-message :info ";TEST Confirm the second-level subnet is present.")
      (fiveam:is (equal
                   (mapcar #'syscat::make-subnet-uid
                           (list subnet1 subnet2))
                   (mapcar #'syscat::make-subnet-uid
                           (syscat::find-subnet session resourcetype-schema org subnet2))))
      ;; Add a third subnet
      (syscat::log-message :info ";TEST Add a third subnet between the first two.")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet3 policy))
      ;; Confirm that's also there
      (syscat::log-message :info ";TEST Confirm the new second-level subnet is present.")
      (fiveam:is (syscat::find-subnet session resourcetype-schema org subnet3))
      ;; Confirm it's correctly moved the second subnet
      (syscat::log-message :info ";TEST Confirm the original second-level subnet is now third.")
      (fiveam:is (equal
                   (mapcar #'syscat::make-subnet-uid
                           (list subnet1 subnet3 subnet2))
                   (mapcar #'syscat::make-subnet-uid
                           (syscat::find-subnet session resourcetype-schema org subnet2))))
      ;; Remove the top-level subnet
      (syscat::log-message :info ";TEST Delete the top-level subnet.")
      (fiveam:is (null (syscat::delete-subnet session org subnet1 resourcetype-schema)))
      ;; Confirm the top-level subnet is gone
      (fiveam:is (null (syscat::find-subnet session resourcetype-schema org subnet1)))
      ;; Remove the second subnet
      (syscat::log-message :info ";TEST Delete the now top-level subnet.")
      (fiveam:is (null (syscat::delete-subnet session org subnet2 resourcetype-schema)))
      ;; Remove the fixtures
      (syscat::log-message :info ";TEST Deleting the fixtures.")
      (syscat::delete-resource-by-path
        session
        (list "Organisations" org)
        resourcetype-schema
        :recursive t))
    ;; Clean up the mess
    (syscat::delete-schema-version session schema-version)
    (neo4cl:disconnect session)))

(fiveam:test
  ipam-ipv6-subnets-3-levels-no-vrf
  "Create/read/delete tests on nested IPv6 subnets directly under an organisation."
  (let* ((org "testco63")
         (subnet1 (ipaddress:make-ipv6-subnet "2001:db8::/32"))
         (subnet2 (ipaddress:make-ipv6-subnet "2001:db8:dead:beef::/64"))
         (subnet3 (ipaddress:make-ipv6-subnet "2001:db8:dead::/48"))
         (policy :ALLOW-ALL)
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Now fetch the schema
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default set of resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Confirm the fixtures aren't already present
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org))))
      ;; Add the fixtures
      (syscat::log-message :info ";TEST Creating the fixtures.")
      (syscat::store-resource session
                                  resourcetype-schema
                                  "Organisations"
                                  `(("ScUID" . ,org))
                                  *admin-user*)
      ;; Add a top-level subnet; this should return NIL.
      (syscat::log-message :info ";TEST Add a top-level subnet.")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet1 policy))
      ;; Confirm the subnet is there
      (syscat::log-message :info ";TEST Confirm the top-level subnet is present.")
      (fiveam:is (equal (list (syscat::make-subnet-uid subnet1))
                        (mapcar #'syscat::make-subnet-uid
                                (syscat::find-subnet session resourcetype-schema org subnet1))))
      ;; Add a second subnet
      (syscat::log-message :info ";TEST Add a second-level subnet.")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet2 policy))
      ;; Confirm that's also there
      (syscat::log-message :info ";TEST Confirm the second-level subnet is present.")
      (fiveam:is (equal
                   (mapcar #'syscat::make-subnet-uid
                           (list subnet1 subnet2))
                   (mapcar #'syscat::make-subnet-uid
                           (syscat::find-subnet session resourcetype-schema org subnet2))))
      ;; Add a third subnet
      (syscat::log-message :info ";TEST Add a third subnet between the first two.")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet3 policy))
      ;; Confirm that's also there
      (syscat::log-message :info ";TEST Confirm the new second-level subnet is present.")
      (fiveam:is (syscat::find-subnet session resourcetype-schema org subnet3))
      ;; Confirm it's correctly moved the second subnet
      (syscat::log-message :info ";TEST Confirm the original second-level subnet is now third.")
      (fiveam:is (equal
                   (mapcar #'syscat::make-subnet-uid
                           (list subnet1 subnet3 subnet2))
                   (mapcar #'syscat::make-subnet-uid
                           (syscat::find-subnet session resourcetype-schema org subnet2))))
      ;; Remove the top-level subnet
      (syscat::log-message :info ";TEST Delete the top-level subnet.")
      (fiveam:is (null (syscat::delete-subnet session org subnet1 resourcetype-schema)))
      ;; Confirm the top-level subnet is gone
      (fiveam:is (null (syscat::find-subnet session resourcetype-schema org subnet1)))
      ;; Remove the second subnet
      (syscat::log-message :info ";TEST Delete the now third-level subnet.")
      (fiveam:is (null (syscat::delete-subnet session org subnet2 resourcetype-schema)))
      ;; Remove the fixtures
      (syscat::log-message :info ";TEST Deleting the fixtures.")
      (syscat::delete-resource-by-path
        session
        (list "Organisations" org)
        resourcetype-schema
        :recursive t))
    ;; Clean up the mess
    (syscat::delete-schema-version session schema-version)
    (neo4cl:disconnect session)))

(fiveam:test
  ipv4address-basic
  "Basic create/find/delete operations on an IPv4 address"
  (let* ((org "example")
         (address (ipaddress:make-ipv4-address "172.17.2.3"))
         (vrf "green")
         (subnet (ipaddress:make-ipv4-subnet "172.17.2.0/24"))
         (policy :ALLOW-ALL)
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Now fetch the schema
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default set of resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Ensure we're clear to start
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org))))
      ;; Create fixtures
      (syscat::log-message :info "Creating fixtures with one VRF")
      (syscat::store-resource session
                                  resourcetype-schema
                                  "Organisations"
                                  `(("ScUID" . ,org))
                                  *admin-user*)
      (syscat::store-dependent-resource session
        resourcetype-schema
        (list "Organisations" org "CONTAINS_VRF_GROUPS" "VrfGroups")
        `(("ScUID" . ,vrf))
        *admin-user*)
      (syscat::insert-subnet session resourcetype-schema org subnet policy :vrf vrf)
      ;; Tests
      (syscat::log-message :info ";TEST Address is absent")
      (fiveam:is (null (syscat::find-ipaddress session resourcetype-schema address org :vrf vrf)))
      (syscat::log-message :info ";TEST Insert address")
      (fiveam:is (syscat::insert-ipaddress session
                                               resourcetype-schema
                                               address
                                               org
                                               policy
                                               :vrf vrf))
      (fiveam:is (equal address
                        (car (last (syscat::find-ipaddress session
                                                               resourcetype-schema
                                                               address
                                                               org
                                                               :vrf vrf)))))
      (syscat::log-message :info ";TEST Delete address")
      (fiveam:is (null (syscat::delete-ipaddress session
                                                     resourcetype-schema
                                                     address
                                                     org
                                                     :vrf vrf)))
      (fiveam:is (null (syscat::find-ipaddress session
                                                   resourcetype-schema
                                                   address
                                                   org
                                                   :vrf vrf)))
      ;; Remove fixtures
      (syscat::log-message :info ";TEST Remove fixtures")
      (syscat::delete-resource-by-path session
                                           (list "Organisations" org)
                                           resourcetype-schema
                                           :recursive t)
      ;; Ensure the fixtures are gone
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org)))))
    ;; Clean up the mess
    (syscat::delete-schema-version session schema-version)
    (neo4cl:disconnect session)))

(fiveam:test
  ipv6address-basic
  "Basic create/find/delete operations on an IPv6 address"
  (let* ((org "example")
         (address (ipaddress:make-ipv6-address "2001:db8::6"))
         (vrf "green")
         (subnet (ipaddress:make-ipv6-subnet "2001:db8::/48"))
         (policy :ALLOW-ALL)
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Now fetch the schema
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default set of resources
      (syscat::install-default-resources session resourcetype-schema)
      ;; Ensure we're clear to start
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org))))
      ;; Create fixtures
      (syscat::log-message :info ";TEST Creating fixtures with one VRF")
      (syscat::store-resource session
                                  resourcetype-schema
                                  "Organisations"
                                  `(("ScUID" . ,org))
                                  *admin-user*)
      (syscat::store-dependent-resource
        session
        resourcetype-schema
        (list "Organisations" org "CONTAINS_VRF_GROUPS" "VrfGroups")
        `(("ScUID" . ,vrf))
        *admin-user*)
      (syscat::insert-subnet session resourcetype-schema org subnet policy :vrf vrf)
      ;; Tests
      (syscat::log-message :info ";TEST Address is absent")
      (fiveam:is (null (syscat::find-ipaddress session resourcetype-schema address org :vrf vrf)))
      (syscat::log-message :info ";TEST Insert address")
      (fiveam:is (syscat::insert-ipaddress session
                                               resourcetype-schema
                                               address
                                               org
                                               policy
                                               :vrf vrf))
      (syscat::log-message :info ";TEST Verify that we can find the address we just inserted")
      (fiveam:is (equal address
                        (car (last (syscat::find-ipaddress session
                                                               resourcetype-schema
                                                               address
                                                               org
                                                               :vrf vrf)))))
      (syscat::log-message :info ";TEST Delete address")
      (fiveam:is (null (syscat::delete-ipaddress session
                                                     resourcetype-schema
                                                     address
                                                     org
                                                     :vrf vrf)))
      (fiveam:is (null (syscat::find-ipaddress session
                                                   resourcetype-schema
                                                   address
                                                   org
                                                   :vrf vrf)))
      ;; Remove fixtures
      (syscat::delete-resource-by-path session
                                           (list "Organisations" org)
                                           resourcetype-schema
                                           :recursive t)
      ;; Ensure the fixtures are gone
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org)))))
    ;; Clean up the mess
    (syscat::delete-schema-version session schema-version)
    (neo4cl:disconnect session)))

(fiveam:test
  ipv4-subnets-and-addresses-basic
  "Basic tests for adding and removing IPv4 subnets with addresses"
  (let* ((org "sample")
         (subnet1 (ipaddress:make-ipv4-subnet "192.168.0.0/16"))
         (subnet2 (ipaddress:make-ipv4-subnet "192.168.32.0/23"))
         (address (ipaddress:make-ipv4-address "192.168.32.3"))
         (policy :ALLOW-ALL)
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema
    (syscat::log-message :info ";TEST Set up the schema")
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Now fetch the schema
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default set of resources
      (syscat::log-message :info ";TEST Install default resources")
      (syscat::install-default-resources session resourcetype-schema)
      (syscat::log-message :info ";TEST Creating the fixtures")
      ;; Confirm the fixtures aren't already present
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org))))
      ;; Add the fixtures
      (syscat::store-resource session
                                  resourcetype-schema
                                  "Organisations"
                                  `(("ScUID" . ,org))
                                  *admin-user*)
      (syscat::insert-subnet session resourcetype-schema org subnet1 policy)
      ;; Add the IP address
      (syscat::log-message :info ";TEST Add the IP address")
      (fiveam:is (syscat::insert-ipaddress session resourcetype-schema address org policy))
      ;; Confirm the address is there
      (syscat::log-message :info ";TEST Confirm the address is present")
      (fiveam:is (syscat::find-ipaddress session resourcetype-schema address org))
      (fiveam:is (equal address
                        (car (last (syscat::find-ipaddress session
                                                               resourcetype-schema
                                                               address
                                                               org)))))
      ;; Add another subnet
      (syscat::log-message :info ";TEST Add a second-level subnet")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet2 policy))
      ;; Confirm that's also there
      (syscat::log-message :info ";TEST Confirm the second-level subnet is present")
      (fiveam:is (equal (mapcar #'ipaddress:as-cidr (list subnet1 subnet2))
                        (mapcar #'ipaddress:as-cidr (syscat::find-subnet
                                                      session
                                                      resourcetype-schema
                                                      org
                                                      subnet2))))
      ;; Confirm the address has the correct new path
      (syscat::log-message :info ";TEST Confirm the address has been correctly moved")
      (let ((newpath (syscat::find-ipaddress session resourcetype-schema address org)))
        (syscat::log-message :debug
                                 (format nil ";DIAGNOSTIC Search returned path ~{/~A~}" newpath))
        (fiveam:is (equal (mapcar #'ipaddress:as-cidr (list subnet1 subnet2))
                          (mapcar #'ipaddress:as-cidr (butlast newpath))))
        (fiveam:is (equal address
                          (car (last newpath)))))
      ;; Remove the second subnet
      (syscat::log-message :info ";TEST Delete the second-level subnet")
      (fiveam:is (null (syscat::delete-subnet session org subnet2 resourcetype-schema)))
      ;; Confirm the address has moved back again
      (syscat::log-message :info ";TEST Confirm the address is back under the top-level subnet")
      (fiveam:is (syscat::find-ipaddress session resourcetype-schema address org))
      (let ((newpath (syscat::find-ipaddress session resourcetype-schema address org)))
        (fiveam:is (equal (list (ipaddress:as-cidr subnet1))
                          (mapcar #'ipaddress:as-cidr (butlast newpath))))
        (fiveam:is (equal address
                          (car (last newpath)))))
      ;; Remove the fixtures
      (syscat::log-message :info ";TEST Deleting the fixtures")
      (syscat::delete-resource-by-path session
                                           (list "Organisations" org)
                                           resourcetype-schema
                                           :recursive t))
    ;; Clean up the mess
    (syscat::delete-schema-version session schema-version)
    (neo4cl:disconnect session)))

(fiveam:test
  ipv6-subnets-and-addresses-basic
  "Basic tests for adding and removing IPv6 subnets with addresses"
  (let* ((org "sample")
         (subnet1 (ipaddress:make-ipv6-subnet "2001:db8::/32"))
         (subnet2 (ipaddress:make-ipv6-subnet "2001:db8:37::/64"))
         (address (ipaddress:make-ipv6-address "2001:db8:37::45"))
         (policy :ALLOW-ALL)
         (session (neo4cl:establish-bolt-session *bolt-server*))
         (schema-version (syscat::create-new-schema-version session)))
    ;; Install the core schema
    (syscat::log-message :info ";TEST Set up the schema")
    (syscat::install-subschema session syscat::*core-schema* schema-version)
    ;; Now fetch the schema
    (let ((resourcetype-schema (car (syscat::fetch-current-schema session))))
      ;; Install the default set of resources
      (syscat::log-message :info ";TEST Install default resources")
      (syscat::install-default-resources session resourcetype-schema)
      (syscat::log-message :info ";TEST Creating the fixtures.")
      ;; Confirm the fixtures aren't already present
      (fiveam:is (null (syscat::resource-exists-p session (list "Organisations" org))))
      ;; Add the fixtures
      (syscat::store-resource session
                                  resourcetype-schema
                                  "Organisations"
                                  `(("ScUID" . ,org))
                                  *admin-user*)
      (syscat::insert-subnet session resourcetype-schema org subnet1 policy)
      ;; Add the IP address
      (syscat::log-message :info ";TEST Add the IP address")
      (fiveam:is (syscat::insert-ipaddress session resourcetype-schema address org policy))
      ;; Confirm the address is there
      (syscat::log-message :info ";TEST Confirm the address is present.")
      (fiveam:is (syscat::find-ipaddress session resourcetype-schema address org))
      (fiveam:is (equal address
                        (car (last (syscat::find-ipaddress session
                                                               resourcetype-schema
                                                               address org)))))
      ;; Add another subnet
      (syscat::log-message :info ";TEST Add a second-level subnet.")
      (fiveam:is (syscat::insert-subnet session resourcetype-schema org subnet2 policy))
      ;; Confirm that's also there
      (syscat::log-message :info ";TEST Confirm the second-level subnet is present.")
      (fiveam:is (equal (mapcar #'ipaddress:as-cidr (list subnet1 subnet2))
                        (mapcar #'ipaddress:as-cidr (syscat::find-subnet
                                                      session
                                                      resourcetype-schema
                                                      org
                                                      subnet2))))
      ;; Confirm the address has the correct new path
      (syscat::log-message :info ";TEST Confirm the address has been correctly moved.")
      (let ((newpath (syscat::find-ipaddress session resourcetype-schema address org)))
        (syscat::log-message :debug
                                 (format nil ";DIAGNOSTIC Search returned path ~{/~A~}." newpath))
        (fiveam:is (equal (mapcar #'ipaddress:as-cidr (list subnet1 subnet2))
                          (mapcar #'ipaddress:as-cidr (butlast newpath))))
        (fiveam:is (equal address
                          (car (last newpath)))))
      ;; Remove the second subnet
      (syscat::log-message :info ";TEST Delete the second-level subnet.")
      (fiveam:is (null (syscat::delete-subnet session org subnet2 resourcetype-schema)))
      ;; Confirm the address has moved back again
      (syscat::log-message :info ";TEST Confirm the address is back under the top-level subnet.")
      (fiveam:is (syscat::find-ipaddress session resourcetype-schema address org))
      (let ((newpath (syscat::find-ipaddress session resourcetype-schema address org)))
        (fiveam:is (equal (list (ipaddress:as-cidr subnet1))
                          (mapcar #'ipaddress:as-cidr (butlast newpath))))
        (fiveam:is (equal address
                          (car (last newpath)))))
      ;; Remove the fixtures
      (syscat::log-message :info ";TEST Deleting the fixtures.")
      (syscat::delete-resource-by-path session
                                           (list "Organisations" org)
                                           resourcetype-schema
                                           :recursive t))
    ;; Clean up the mess
    (syscat::delete-schema-version session schema-version)
    (neo4cl:disconnect session)))
