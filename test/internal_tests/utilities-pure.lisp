;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Test suite for pure code.
;;;; I.e, functions and methods without side-effects.

(in-package #:syscat-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(fiveam:def-suite
  utilities-pure
  :description "Tests for *non* side-effecting functions in utilities.lisp.
  Tests are named for the functions they're testing."
  :in main)

(fiveam:in-suite utilities-pure)

(fiveam:test
  syscat::sanitise-uid
  (fiveam:is (equal "foo_bar"
                    (syscat::sanitise-uid "foo bar"))))

(fiveam:test
  get-sub-uri
  (fiveam:is (equal "/People/TestPerson"
                    (syscat::get-sub-uri
                      "/raw/v1/People/TestPerson"
                      "/raw/v1"))))

(fiveam:test
  get-uri-parts
  (fiveam:is (equal '("People" "TestPerson")
                    (syscat::get-uri-parts "/People/TestPerson")))
  (fiveam:is (equal '("TestPerson")
                    (syscat::get-uri-parts "People/TestPerson"))))

(fiveam:test
  uri-node-helper
  ;; End-of-list
  (fiveam:is (equal "(n)" (syscat::uri-node-helper '() :path "" :marker "n")))
  (fiveam:is (equal "()" (syscat::uri-node-helper '() :path "" :marker "")))
  (fiveam:is (equal "path(n)" (syscat::uri-node-helper '() :path "path" :marker "n")))
  (fiveam:is (equal "path()" (syscat::uri-node-helper '() :path "path" :marker "")))
  ;; 1-element URI
  (fiveam:is (equal "(n:foo)" (syscat::uri-node-helper '("foo") :path "" :marker "n")))
  (fiveam:is (equal "(:foo)" (syscat::uri-node-helper '("foo") :path "" :marker "")))
  (fiveam:is (equal "path(n:foo)" (syscat::uri-node-helper '("foo") :path "path" :marker "n")))
  (fiveam:is (equal "path(:foo)" (syscat::uri-node-helper '("foo") :path "path" :marker "")))
  ;; 2-element URI
  (fiveam:is (equal "(n:Foo { ScUID: 'bar' })"
                    (syscat::uri-node-helper '("Foo" "bar") :path "" :marker "n")))
  (fiveam:is (equal "(:Foo { ScUID: 'bar' })"
                    (syscat::uri-node-helper '("Foo" "bar") :path "" :marker "")))
  (fiveam:is (equal "path(n:Foo { ScUID: 'bar' })"
                    (syscat::uri-node-helper '("Foo" "bar") :path "path" :marker "n")))
  (fiveam:is (equal "path(:Foo { ScUID: 'bar' })"
                    (syscat::uri-node-helper '("Foo" "bar") :path "path" :marker "")))
  ;; 3-element URI
  (fiveam:is (equal "(:Foo { ScUID: 'bar' })-[:BAZ]->(n)"
                    (syscat::uri-node-helper '("Foo" "bar" "BAZ")
                                                 :path ""
                                                 :marker "n")))
  (fiveam:is (equal "(:Foo { ScUID: 'bar' })-[:BAZ]->()"
                    (syscat::uri-node-helper '("Foo" "bar" "BAZ")
                                                 :path ""
                                                 :marker "")))
  (fiveam:is (equal "path(:Foo { ScUID: 'bar' })-[:BAZ]->(n)"
                    (syscat::uri-node-helper '("Foo" "bar" "BAZ")
                                                 :path "path"
                                                 :marker "n")))
  (fiveam:is (equal "path(:Foo { ScUID: 'bar' })-[:BAZ]->()"
                    (syscat::uri-node-helper '("Foo" "bar" "BAZ")
                                                 :path "path"
                                                 :marker "")))
  ;; 3-element URI with wildcard
  (fiveam:is (equal "(:Foo)-[:BAZ]->(n)"
                    (syscat::uri-node-helper '("Foo" "*" "BAZ")
                                                 :path ""
                                                 :marker "n")))
  (fiveam:is (equal "(:Foo)-[:BAZ]->()"
                    (syscat::uri-node-helper '("Foo" "*" "BAZ")
                                                 :path ""
                                                 :marker "")))
  (fiveam:is (equal "path(:Foo)-[:BAZ]->(n)"
                    (syscat::uri-node-helper '("Foo" "*" "BAZ")
                                                 :path "path"
                                                 :marker "n")))
  (fiveam:is (equal "path(:Foo)-[:BAZ]->()"
                    (syscat::uri-node-helper '("Foo" "*" "BAZ")
                                                 :path "path"
                                                 :marker "")))
  )


;; FIXME: write these tests
#+(or)
(fiveam:test
  build-cypher-path)

#+(or)
(fiveam:test
  digest-to-filepath)

#+(or)
(fiveam:test
  replace-all)
