# Basic configuration for a development instance of Restagraph

export COOKIE_DOMAIN='sctest.onfire.onice'
export FILES_LOCATION="/tmp/restagraph-files/"
export FILES_TEMP_LOCATION="/tmp/restagraph-files-tmp/"
#export SCHEMAPATH="/home/kat/devel/restagraph/restagraph/schemas/"
export TEMPLATE_PATH="/home/kat/devel/restagraph/restagraph/src/templates/"
# Uncomment the following line to enable debug-logging
#export DEBUG="true"

# Neo4j configs
export NEO4J_HOSTNAME="192.0.2.1"
export NEO4J_PORT=7802
export NEO4J_USER='neo4j'
export NEO4J_PASSWORD='wombat'

# Auth policy
#
## Other values are 'open' and 'readonly'
export ACCESS_POLICY='write-authenticated'
#
export AUTH_NEO4J_HOSTNAME="192.0.2.1"
export AUTH_NEO4J_PORT=7803
export AUTH_NEO4J_PASSWORD="bilby"
export AUTH_SCADMIN_PASSWORD="thisisabadpassword"
