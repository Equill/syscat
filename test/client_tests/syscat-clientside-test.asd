;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(asdf:defsystem #:syscat-clientside-test
                :serial nil
                :license "AGPL-3.0"
                :author "Kat Sebastian <kat@electronic-quill.net>"
                :description "Client-side test suite for Syscat"
                :depends-on (#:fiveam
                             #:ironclad
                             #:log4cl
                             #:osicat
                             #:syscat
                             #:syscat-client)
                :components ((:file "package")
                             (:file "main" :depends-on ("package"))
                             (:file "utilities" :depends-on ("main"))
                             ;; Functionality
                             (:file "functionality/fiveam-functionality" :depends-on ("utilities"))
                             (:file "functionality/schema"
                                    :depends-on ("functionality/fiveam-functionality"))
                             (:file "functionality/resources-primary"
                                    :depends-on ("functionality/fiveam-functionality"))
                             (:file "functionality/resources-dependent"
                                    :depends-on ("functionality/fiveam-functionality"))
                             (:file "functionality/relationships"
                                    :depends-on ("functionality/fiveam-functionality"))
                             (:file "functionality/files"
                                    :depends-on ("functionality/fiveam-functionality"))
                             (:file "functionality/ipam"
                                    :depends-on ("functionality/fiveam-functionality"))
                             ;; Authenticated-only
                             (:file "authenticated_only/fiveam-auth-only")))
