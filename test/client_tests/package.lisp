;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(defpackage :syscat-clientside-test
  (:use #:cl)
  (:export
    main
    functionality
    functionality-files
    functionality-ipam
    functionality-relationships
    functionality-resources-dependent
    functionality-resources-primary
    functionality-schema
    authenticated-only))
