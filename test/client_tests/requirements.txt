mypy==1.3.0
requests==2.31.0
pylint==2.17.4
pytest==7.3.1
types-requests==2.30.0
