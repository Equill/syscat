;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(in-package #:syscat-clientside-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(fiveam:def-suite functionality-ipam
                  :description "IPAM operations."
                  :in functionality)

(fiveam:in-suite functionality-ipam)

(fiveam:test
  ipv4-subnets-basic-no-vrf
  "Basic CRD functions for IPv4 subnets"
  :depends-on 'relationships-basic
  (let ((subnet1 "172.16.0.0/12")
        (organisation "testco"))
  ;; Create the organisation
  (fiveam:is (= 201 (syscat-client:post-request *server*
                                            "/Organisations"
                                            :payload `(("ScUID" . ,organisation)))))
  ;; Add the subnet
  (fiveam:is (= 201 (syscat-client:post-request *server*
                                            "/subnets"
                                            :api "ipam"
                                            :payload `(("org" . ,organisation)
                                                       ("subnet" . ,subnet1)))))
  ;; Confirm the subnet is present
  (multiple-value-bind (status body)
    (syscat-client:get-request *server*
                           (format nil "/subnets?org=~A&subnet=~A" organisation subnet1)
                           :api "ipam")
    (fiveam:is (= 200 status))
    (fiveam:is (equal '("/Organisations/testco/CONTAINS_IPV4_SUBNETS/Ipv4Subnets/172.16.0.0_12")
                       body)))
  ;; Delete the subnet
  (fiveam:is (= 204 (syscat-client:delete-request
                      *server*
                      "/subnets"
                      :api "ipam"
                      :payload `(("org" . ,organisation)
                                 ("subnet" . ,subnet1)))))
  ;; Confirm the subnet is gone
  (multiple-value-bind (status body)
    (syscat-client:get-request *server*
                           (format nil "/subnets?org=~A&subnet=~A" organisation subnet1)
                           :api "ipam")
    (fiveam:is (= 200 status))
    (fiveam:is (null body)))
  ;; Remove the organisation
  (fiveam:is (= 204 (syscat-client:delete-request *server* (format nil "/Organisations/~A" organisation))))))

(fiveam:test
  ipv6-subnets-basic-no-vrf
  "Basic CRD functions for IPv4 subnets"
  :depends-on 'relationships-basic
  (let ((subnet1 "2001:db8::/32")
        (organisation "testco"))
  ;; Create the organisation
  (fiveam:is (= 201 (syscat-client:post-request *server*
                                            "/Organisations"
                                            :payload `(("ScUID" . ,organisation)))))
  ;; Add the subnet
  (fiveam:is (= 201 (syscat-client:post-request *server*
                                            "/subnets"
                                            :api "ipam"
                                            :payload `(("org" . ,organisation)
                                                       ("subnet" . ,subnet1)))))
  ;; Confirm the subnet is present
  (multiple-value-bind (status body)
    (syscat-client:get-request *server*
                           (format nil "/subnets?org=~A&subnet=~A" organisation subnet1)
                           :api "ipam")
    (fiveam:is (= 200 status))
    (fiveam:is (equal '("/Organisations/testco/CONTAINS_IPV6_SUBNETS/Ipv6Subnets/20010db8000000000000000000000000_32")
                       body)))
  ;; Delete the subnet
  (fiveam:is (= 204 (syscat-client:delete-request
                      *server*
                      "/subnets"
                      :api "ipam"
                      :payload `(("org" . ,organisation)
                                 ("subnet" . ,subnet1)))))
  ;; Confirm the subnet is gone
  (multiple-value-bind (status body)
    (syscat-client:get-request *server*
                           (format nil "/subnets?org=~A&subnet=~A" organisation subnet1)
                           :api "ipam")
    (fiveam:is (= 200 status))
    (fiveam:is (null body)))
  ;; Remove the organisation
  (fiveam:is (= 204 (syscat-client:delete-request *server* (format nil "/Organisations/~A" organisation))))))

(fiveam:test
  ipv4-addresses-basic-no-vrf
  "Basic CRD functions for IPv4 addresses"
  :depends-on 'ipv4-subnets-basic-no-vrf
  (let ((subnet1 "172.16.0.0/12")
        (address1 "172.16.23.4")
        (organisation "testco"))
    ;; Create the organisation
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              "/Organisations"
                                              :payload `(("ScUID" . ,organisation)))))
    ;; Add the subnet
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              "/subnets"
                                              :api "ipam"
                                              :payload `(("org" . ,organisation)
                                                         ("subnet" . ,subnet1)))))
    ;; Add the address
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              "/addresses"
                                              :api "ipam"
                                              :payload `(("org" . ,organisation)
                                                         ("address" . ,address1)))))
    ;; Confirm the address is present
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "/addresses?org=~A&address=~A" organisation address1)
                             :api "ipam")
      (fiveam:is (= 200 status))
      (fiveam:is (equal '("/Organisations/testco/CONTAINS_IPV4_SUBNETS/Ipv4Subnets/172.16.0.0_12/CONTAINS_IPV4_ADDRESSES/Ipv4Addresses/172.16.23.4")
                        body)))
    ;; Delete the address
    (fiveam:is (= 204 (syscat-client:delete-request
                        *server*
                        "/addresses"
                        :api "ipam"
                        :payload `(("org" . ,organisation)
                                   ("address" . ,address1)))))
    ;; Confirm the address is gone
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "/addresses?org=~A&address=~A" organisation address1)
                             :api "ipam")
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    ;; Remove the organisation
    (fiveam:is (= 204 (syscat-client:delete-request *server*
                                                (format nil "/Organisations/~A?recursive=true"
                                                        organisation))))))

(fiveam:test
  ipv6-addresses-basic-no-vrf
  "Basic CRD functions for IPv6 addresses"
  :depends-on 'ipv6-subnets-basic-no-vrf
  (let ((subnet1 "2001:db8::/32")
        (address1 "2001:db8::4")
        (organisation "testco"))
    ;; Create the organisation
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              "/Organisations"
                                              :payload `(("ScUID" . ,organisation)))))
    ;; Add the subnet
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              "/subnets"
                                              :api "ipam"
                                              :payload `(("org" . ,organisation)
                                                         ("subnet" . ,subnet1)))))
    ;; Add the address
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              "/addresses"
                                              :api "ipam"
                                              :payload `(("org" . ,organisation)
                                                         ("address" . ,address1)))))
    ;; Confirm the address is present
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "/addresses?org=~A&address=~A" organisation address1)
                             :api "ipam")
      (fiveam:is (= 200 status))
      (fiveam:is (equal '("/Organisations/testco/CONTAINS_IPV6_SUBNETS/Ipv6Subnets/20010db8000000000000000000000000_32/CONTAINS_IPV6_ADDRESSES/Ipv6Addresses/20010db8000000000000000000000004")
                        body)))
    ;; Delete the address
    (fiveam:is (= 204 (syscat-client:delete-request
                        *server*
                        "/addresses"
                        :api "ipam"
                        :payload `(("org" . ,organisation)
                                   ("address" . ,address1)))))
    ;; Confirm the address is gone
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "/addresses?org=~A&address=~A" organisation address1)
                             :api "ipam")
      (fiveam:is (= 200 status))
      (fiveam:is (null body)))
    ;; Remove the organisation
    (fiveam:is (= 204 (syscat-client:delete-request *server*
                                                (format nil "/Organisations/~A?recursive=true"
                                                        organisation))))))
