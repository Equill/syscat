;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(in-package #:syscat-clientside-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(fiveam:def-suite functionality-resources-dependent
                  :description "Operations specifically on dependent resources."
                  :in functionality)

(fiveam:in-suite functionality-resources-dependent)

(fiveam:test
  dependent-resources-basic
  "Basic create/confirm/delete operations on dependent resources."
  :depends-on 'resources-basic
  (let ((schema-path
          (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                         :name "test_schema.json"))
        (res1type "Buildings")
        (res1uid "Xenon")
        (res1uid2 "XenonBase")
        (depres1rel "CONTAINS_FLOORS")
        (depres1type "Floors")
        (depres1uid "hangar"))
    ;; Add the test schema
    (install-schema *server* schema-path)
    ;; Create the primary resources
    (syscat-client:post-request *server* (format nil "/~A" res1type) :payload `(("ScUID" . ,res1uid)))
    ;(syscat-client:post-request *server* (format nil "/~A" res3type) :payload `(("ScUID" . ,res3uid)))
    ;; Fail to create the dependent resource as a primary one
    (fiveam:is (= 400 (syscat-client:post-request *server*
                                              (format nil "/~A" depres1type)
                                              :payload `(("ScUID" . ,depres1uid)))))
    ;; Create the dependent resource correctly
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              (format nil "~{/~A~}" (list res1type
                                                                          res1uid
                                                                          depres1rel
                                                                          depres1type))
                                              :payload `(("ScUID" . ,depres1uid)))))
    ;; Confirm the dependent resource is there
    (fiveam:is (hash-table-p (first (nth-value 1 (syscat-client:get-request
                                                   *server*
                                                   (format nil "~{/~A~}" (list res1type
                                                                               res1uid
                                                                               depres1rel
                                                                               depres1type
                                                                               depres1uid)))))))
    ;; Confirm the dependent resource can't be fetched directly
    (fiveam:is (= 400 (syscat-client:get-request *server*
                                             (format nil "/~A/~A" depres1type depres1uid)
                                             :expected-status-code 400)))
    ;; Confirm the dependent resource can't be deleted directly
    (fiveam:is (= 400 (syscat-client:delete-request *server* (format nil "/~A/~A" depres1type depres1uid))))
    ;; Rename the parent resource
    (fiveam:is (= 200 (syscat-client:post-request *server*
                                              (format nil "/~A/~A" res1type res1uid)
                                              :put-p t
                                              :payload `(("ScUID" . ,res1uid2)))))
    ;; Confirm that the dependent resource's ScCanonicalpath attribute has been updated
    (let ((path (format nil "~{/~A~}" (list res1type res1uid2 depres1rel depres1type depres1uid))))
      (multiple-value-bind (status body)
        (syscat-client:get-request *server* path)
        (declare (ignore status))
        (fiveam:is (string= path
                            (gethash "ScCanonicalpath" (first body))))))
    ;; Delete the dependent resource correctly
    (fiveam:is (= 204 (syscat-client:delete-request *server*
                                                (format nil "~{/~A~}" (list res1type
                                                                            res1uid2
                                                                            depres1rel
                                                                            depres1type
                                                                            depres1uid)))))
    ;; Confirm the dependent resource is gone
    (fiveam:is (null (nth-value 1 (syscat-client:get-request
                                    *server*
                                    (format nil "~{/~A~}" (list res1type
                                                                res1uid2
                                                                depres1rel
                                                                depres1type
                                                                depres1uid))))))
    ;; Confirm the parent is there
    (fiveam:is (hash-table-p (first (nth-value 1 (syscat-client:get-request
                                                   *server*
                                                   (format nil "~{/~A~}" (list res1type res1uid2)))))))
    ;; Clean up by deleting the parent resource
    (syscat-client:delete-request *server* (format nil "/~A/~A" res1type res1uid2))
    ;; Remove the test schema
    (remove-last-schema *server*)))

(fiveam:test
  dependent-resources-uid-sanitising
  "Check sanitising of dependent resource UIDs in case there's a failure mode specific to this path.
  Also check recursive deletion."
  :depends-on 'dependent-resources-basic
  (let ((schema-path
          (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                         :name "test_schema.json"))
        (res1type "Buildings")
        (res1uid "Xenon")
        (depres1rel "CONTAINS_FLOORS")
        (depres1type "Floors")
        (depres1uid "hangar"))
    ;; Add the test schema
    (install-schema *server* schema-path)
    ;; Create the parent resource
    (syscat-client:post-request *server*
                            (format nil "/~A" res1type)
                            :payload `(("ScUID" . ,res1uid)))
    ;; Create the dependent resource, but wrap its UID in whitespace
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              (format nil "~{/~A~}" (list res1type
                                                                          res1uid
                                                                          depres1rel
                                                                          depres1type))
                                              :payload `(("ScUID" . ,(format nil " ~A " depres1uid))))))
    ;; Confirm the dependent resource is there as expected
    (fiveam:is (hash-table-p (first (nth-value 1 (syscat-client:get-request
                                                   *server*
                                                   (format nil "~{/~A~}" (list res1type
                                                                               res1uid
                                                                               depres1rel
                                                                               depres1type
                                                                               depres1uid)))))))
    ;; Clean up by recursively deleting the parent resource
    (syscat-client:delete-request *server* (format nil "/~A/~A?recursive=true" res1type res1uid))
    ;; Confirm that the parent resource is gone
    (fiveam:is (null (nth-value 1 (syscat-client:get-request
                                    *server*
                                    (format nil "~{/~A~}" (list res1type res1uid))))))
    ;; Clean up the schema we added
    (remove-last-schema *server*)))

(fiveam:test
  dependent-resources-recursive-2
  "Create a grandchild resource, and check recursive deletion from the parent.
  - check for orphaned grandchildren.
  - check for recursive deletion, from the parent down 2 levels."
  :depends-on 'dependent-resources-uid-sanitising
  (let* ((schema-path
           (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                          :name "test_schema.json"))
         (res1type "Buildings")
         (res1uid "Xenon")
         (depres1rel "CONTAINS_FLOORS")
         (depres1type "Floors")
         (depres1uid "hangar")
         (depres1uid2 "Main Hangar")
         (sanitised-depres1uid2 (syscat::sanitise-uid "Main Hangar"))
         (depres2rel "CONTAINS_ROOMS")
         (depres2type "Rooms")
         (depres2uid "toolshed")
         (depres4uid "reception"))
    ;; Add the test schema
    (install-schema *server* schema-path)
    ;; Create the parent resource
    (syscat-client:post-request *server*
                            (format nil "/~A" res1type)
                            :payload `(("ScUID" . ,res1uid)))
    ;; Create the child resource
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              (format nil "~{/~A~}" (list res1type
                                                                          res1uid
                                                                          depres1rel
                                                                          depres1type))
                                              :payload `(("ScUID" . ,depres1uid)))))
    ;; Create the grandchild resource
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              (format nil "~{/~A~}" (list res1type
                                                                          res1uid
                                                                          depres1rel
                                                                          depres1type
                                                                          depres1uid
                                                                          depres2rel
                                                                          depres2type))
                                              :payload `(("ScUID" . ,depres2uid)))))
    ;; Confirm the grandchild resource is there
    (fiveam:is (hash-table-p (first (nth-value 1 (syscat-client:get-request
                                                   *server*
                                                   (format nil "~{/~A~}" (list res1type
                                                                               res1uid
                                                                               depres1rel
                                                                               depres1type
                                                                               depres1uid
                                                                               depres2rel
                                                                               depres2type
                                                                               depres2uid)))))))
    ;; Create a second grandchild resource, to confirm that 1:many relationships work
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              (format nil "~{/~A~}" (list res1type
                                                                          res1uid
                                                                          depres1rel
                                                                          depres1type
                                                                          depres1uid
                                                                          depres2rel
                                                                          depres2type))
                                              :payload `(("ScUID" . ,depres4uid)))))
    ;; Confirm the second grandchild resource is there
    (fiveam:is (hash-table-p (first (nth-value 1 (syscat-client:get-request
                                                   *server*
                                                   (format nil "~{/~A~}" (list res1type
                                                                               res1uid
                                                                               depres1rel
                                                                               depres1type
                                                                               depres1uid
                                                                               depres2rel
                                                                               depres2type
                                                                               depres4uid)))))))
    ;; Rename the child resource
    (fiveam:is (= 200
                  (syscat-client:post-request *server*
                                          (format nil "~{/~A~}" (list res1type
                                                                      res1uid
                                                                      depres1rel
                                                                      depres1type
                                                                      depres1uid))
                                          :put-p t
                                          :payload `(("ScUID" . ,depres1uid2)))))
    ;; Confirm that all the canonical paths are still correct
    (mapcar (lambda (path)
              (multiple-value-bind (status body)
                (syscat-client:get-request *server* (format nil "~{/~A~}" path))
                (fiveam:is (= 200 status))
                (fiveam:is (and (stringp (gethash "ScCanonicalpath" (first body)))
                                (string= (format nil "~{/~A~}" path)
                                         (gethash "ScCanonicalpath" (first body)))))))
            (list (list res1type res1uid)
                  (list res1type res1uid depres1rel depres1type sanitised-depres1uid2)
                  (list res1type
                        res1uid
                        depres1rel
                        depres1type
                        sanitised-depres1uid2
                        depres2rel
                        depres2type
                        depres2uid)
                  (list res1type
                        res1uid
                        depres1rel
                        depres1type
                        sanitised-depres1uid2
                        depres2rel
                        depres2type
                        depres4uid)))
    ;; Recursively delete the parent resource
    (syscat-client:delete-request *server* (format nil "/~A/~A?recursive=true" res1type res1uid))
    ;; Confirm the grandchild is gone
    (fiveam:is (null (nth-value 1 (syscat-client:get-request
                                    *server*
                                    (format nil "~{/~A~}" (list res1type
                                                                res1uid
                                                                depres1rel
                                                                depres1type
                                                                sanitised-depres1uid2
                                                                depres2rel
                                                                depres2type
                                                                depres2uid))))))
    ;; Confirm the child is gone
    (fiveam:is (null (nth-value 1 (syscat-client:get-request
                                    *server*
                                    (format nil "~{/~A~}" (list res1type
                                                                res1uid
                                                                depres1rel
                                                                depres1type
                                                                sanitised-depres1uid2))))))
    ;; Confirm the parent is gone
    (fiveam:is (null (nth-value 1 (syscat-client:get-request
                                    *server*
                                    (format nil "~{/~A~}" (list res1type res1uid))))))
    ;; Remove the schema we added
    (remove-last-schema *server*)))

(fiveam:test
  dependent-resources-parent-restrictions
  "Confirm that the server prevents a dependent resource having two parents via the same relationship."
  :depends-on 'dependent-resources-recursive-2
  (let ((schema-path
          (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                         :name "test_schema.json"))
        (res1type "Buildings")
        (res1uid "Xenon")
        (depres1rel "CONTAINS_FLOORS")
        (depres1type "Floors")
        (depres1uid "hangar")
        (depres3uid "cavern")
        (res2uid "ResidenceOne")
        )
    ;; Add the test schema
    (install-schema *server* schema-path)
    ;; Create the parent and dependent resources
    (syscat-client:post-request *server* (format nil "/~A" res1type) :payload `(("ScUID" . ,res1uid)))
    (syscat-client:post-request *server*
                            (format nil "~{/~A~}" (list res1type
                                                        res1uid
                                                        depres1rel
                                                        depres1type))
                            :payload `(("ScUID" . ,depres1uid)))
    ;; Fail to create a duplicate dependent resource
    (fiveam:is (= 200 (syscat-client:post-request *server*
                            (format nil "~{/~A~}" (list res1type
                                                        res1uid
                                                        depres1rel
                                                        depres1type))
                            :payload `(("ScUID" . ,depres1uid)))))
    ;; Fail to create a second 1:1 dependent resource
    (fiveam:is (= 409 (syscat-client:post-request *server*
                            (format nil "~{/~A~}" (list res1type
                                                        res1uid
                                                        depres1rel
                                                        depres1type))
                            :payload `(("ScUID" . ,depres3uid)))))
    ;; Add the new parent resource
    (syscat-client:post-request *server* (format nil "/~A" res1type) :payload `(("ScUID" . ,res2uid)))
    ;; Fail to add a duplicate dependent relationship from the new parent
    (fiveam:is (= 409 (syscat-client:post-request
                        *server*
                        (format nil "~{/~A~}" (list res1type
                                                    res2uid
                                                    depres1rel))
                        :payload `(("target" . ,(format nil "~{/~A~}"
                                                        (list res1type
                                                              res1uid
                                                              depres1rel
                                                              depres1type
                                                              depres1uid)))))))
    ;; Move the child to the new parent
    (fiveam:is (= 201 (syscat-client:post-request
                        *server*
                        (format nil "~{/~A~}"
                                (list res1type
                                      res1uid
                                      depres1rel
                                      depres1type
                                      depres1uid))
                        :payload `(("target" . ,(format nil "~{/~A~}" (list res1type
                                                                            res2uid
                                                                            depres1rel)))))))
    ;; Confirm that the child is now accessible via the new parent
    (fiveam:is (hash-table-p (first (nth-value 1 (syscat-client:get-request *server*
                                                                        (format nil "~{/~A~}"
                                                                                (list res1type
                                                                                      res2uid
                                                                                      depres1rel
                                                                                      depres1type
                                                                                      depres1uid)))))))
    ;; Confirm that the child is *not* accessible via the old parent
    (fiveam:is (null (nth-value 1 (syscat-client:get-request *server*
                                                         (format nil "~{/~A~}"
                                                                 (list res1type
                                                                       res1uid
                                                                       depres1rel
                                                                       depres1type
                                                                       depres1uid))))))
    ;; Remove both parents recursively
    (syscat-client:delete-request *server*
                              (format nil "/~A/~A" res1type res1uid)
                              :payload '(("recursive" . "True")))
    (syscat-client:delete-request *server*
                              (format nil "/~A/~A" res1type res2uid)
                              :payload '(("recursive" . "True")))
    ;; Remove the test-schema that we added
    (remove-last-schema *server*)))

(fiveam:test
  dependent-resources-errors
  "Confirm that the server returns errors when it should."
  :depends-on 'dependent-resources-parent-restrictions
  (let ((schema-path
          (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                         :name "test_schema.json"))
        (res1type "Buildings")
        (res1uid "Xenon")
        (depres1rel "CONTAINS_FLOORS")
        (depres1type "Floors")
        (depres1uid "hangar")
        (invalidparenttype1 "Building")
        (invalidreltype1 "FLOOR")
        (invaliddeptype1 "Floor")
        (backrel1 "LOCATED_IN"))
    ;; Add the test schema
    (install-schema *server* schema-path)
    ;; Try to link a parent resource of invalid type to a dependent resource, neither of which actually exist.
    ;; Yes, this is a real test. Yes, the server stacktraced if you did that, so it's a regression test.
    (fiveam:is (= 400 (syscat-client:post-request
                        *server*
                        (format nil "~{/~A~}" (list invalidparenttype1 res1uid depres1rel))
                        :payload `(("target" . ,(format nil "/~A/~A" depres1type depres1uid))))))
    ;; Try to link a parent resource to a dependent resource of invalid type, neither of which exist.
    (fiveam:is (= 400 (syscat-client:post-request
                        *server*
                        (format nil "~{/~A~}" (list res1type res1uid depres1rel))
                        :payload `(("target" . ,(format nil "/~A/~A" invaliddeptype1 depres1uid))))))
    ;; Try to link a parent resource to a dependent resource, neither of which exist, via an invalid relationship.
    (fiveam:is (= 400 (syscat-client:post-request
                        *server*
                        (format nil "~{/~A~}" (list res1type res1uid invalidreltype1))
                        :payload `(("target" . ,(format nil "/~A/~A" depres1type depres1uid))))))
    ;; Try to add a dependent resource from a nonexistent parent type
    (fiveam:is (= 400 (syscat-client:post-request *server*
                                              (format nil "~{/~A~}" (list invalidparenttype1
                                                                          res1uid
                                                                          depres1rel
                                                                          depres1type))
                                              :payload `(("ScUID" . ,depres1uid)))))
    ;; Try to add a dependent resource of a type that doesn't exist
    (fiveam:is (= 400 (syscat-client:post-request *server*
                                              (format nil "~{/~A~}" (list res1type
                                                                          res1uid
                                                                          depres1rel
                                                                          invaliddeptype1))
                                              :payload `(("ScUID" . ,depres1uid)))))
    ;; Try to create a dependent resource of a parent that doesn't exist, where both types are valid.
    (fiveam:is (= 400 (syscat-client:post-request *server*
                                              (format nil "~{/~A~}" (list res1type
                                                                          res1uid
                                                                          depres1rel
                                                                          depres1type))
                                              :payload `(("ScUID" . ,depres1uid)))))
    ;; Try to create a link from a dependent resource as though it's a top-level one.
    (fiveam:is (= 201
                  (syscat-client:post-request *server*
                                          (format nil "/~A" res1type)
                                          :payload `(("ScUID" . ,res1uid)))))
    (syscat-client:post-request *server*
                            (format nil "~{/~A~}" (list res1type res1uid depres1rel depres1type))
                            :payload `(("ScUID" . ,depres1uid)))
    (fiveam:is (= 400 (syscat-client:post-request
                        *server*
                        (format nil "~{/~A~}" (list depres1type
                                                    depres1uid
                                                    backrel1))
                        :payload `(("target" . ,(format nil "/~A/~A" res1type res1uid))))))
    (syscat-client:delete-request *server*
                              (format nil "/~A/~A" res1type res1uid)
                              :payload '(("recursive" . "true")))
    ;;; Tests to add
    ;; Check that `recursive=true` _only_ deletes dependent resources,
    ;; and _doesn't_ go on a rampage.
    ;; Check that _only_ dependent resources can be created with a dependent relationship.
    ;; Check that dependent relationships _cannot_ be created to existing resources, whether dependent or not.
    ;; Positively confirm both cases.
    ;; Check that the `recursive` parameter really does work in both GET- and POST-styles.
    ;; Clean up the test-schema that we added
    (remove-last-schema *server*)))

(fiveam:test
  dependent-resources-rehoming
  "Move dependent resources between parents."
  :depends-on 'dependent-resources-recursive-2
  (let ((schema-path
          (make-pathname :defaults (format nil "~A/functionality/" *test-directory*)
                         :name "test_schema.json"))
        (res1type "Buildings")
        (res1uid "HeadOffice")
        (res2type "Buildings")
        (res2uid "BranchOffice")
        (depres1rel1 "CONTAINS_FLOORS")
        (depres1type "Floors")
        (depres1uid "Second")
        (depres2rel1 "CONTAINS_ROOMS")
        (depres2type "Rooms")
        (depres2uid "CornerOffice"))
    ;; Install the test schema
    (install-schema *server* schema-path)
    ;; Create the resources
    (syscat-client:post-request *server* (format nil "/~A" res1type) :payload `(("ScUID" . ,res1uid)))
    (syscat-client:post-request *server*
                            (format nil "~{/~A~}" (list res1type res1uid depres1rel1 depres1type))
                            :payload `(("ScUID" . ,depres1uid)))
    (syscat-client:post-request *server*
                            (format nil "~{/~A~}"
                                    (list res1type
                                          res1uid
                                          depres1rel1
                                          depres1type
                                          depres1uid
                                          depres2rel1
                                          depres2type))
                            :payload `(("ScUID" . ,depres2uid)))
    (syscat-client:post-request *server* (format nil "/~A" res2type) :payload `(("ScUID" . ,res2uid)))
    ;; Move the room to a building of its own.
    (fiveam:is (= 201
                  (syscat-client:post-request *server*
                                          (format nil "~{/~A~}"
                                                  (list res1type
                                                        res1uid
                                                        depres1rel1
                                                        depres1type
                                                        depres1uid
                                                        depres2rel1
                                                        depres2type
                                                        depres2uid))
                                          :payload `(("target" . ,(format nil "~{/~A~}" (list res2type res2uid depres2rel1)))))))
    ;; Confirm that the room's ScCanonicalpath has been correctly updated.
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "~{/~A~}" (list res2type
                                                         res2uid
                                                         depres2rel1
                                                         depres2type
                                                         depres2uid)))
      (fiveam:is (= 200 status))
      (fiveam:is (string= (format nil "~{/~A~}" (list res2type res2uid depres2rel1 depres2type depres2uid))
                          (gethash "ScCanonicalpath" (first body)))))
    ;; Move the room back.
    (fiveam:is (= 201
                  (syscat-client:post-request
                    *server*
                    (format nil "~{/~A~}" (list res2type
                                                res2uid
                                                depres2rel1
                                                depres2type
                                                depres2uid))
                    :payload `(("target" . ,(format nil "~{/~A~}" (list res1type
                                                                        res1uid
                                                                        depres1rel1
                                                                        depres1type
                                                                        depres1uid
                                                                        depres2rel1)))))))
    ;; Confirm that the room's ScCanonicalpath has been correctly updated.
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "~{/~A~}" (list res1type
                                                         res1uid
                                                         depres1rel1
                                                         depres1type
                                                         depres1uid
                                                         depres2rel1
                                                         depres2type
                                                         depres2uid)))
      (fiveam:is (= 200 status))
      (fiveam:is (string= (format nil "~{/~A~}" (list res1type
                                                      res1uid
                                                      depres1rel1
                                                      depres1type
                                                      depres1uid
                                                      depres2rel1
                                                      depres2type
                                                      depres2uid))
                          (gethash "ScCanonicalpath" (first body)))))
    ;; Move the floor to the other building.
    (fiveam:is (= 201
                  (syscat-client:post-request
                    *server*
                    (format nil "~{/~A~}" (list res1type
                                                res1uid
                                                depres1rel1
                                                depres1type
                                                depres1uid))
                    :payload `(("target" . ,(format nil "~{/~A~}" (list res2type
                                                                        res2uid
                                                                        depres1rel1)))))))
    ;; Confirming that ScCanonicalpath has been updated for floor and room.
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "~{/~A~}" (list res2type
                                                         res2uid
                                                         depres1rel1
                                                         depres1type
                                                         depres1uid
                                                         depres2rel1
                                                         depres2type
                                                         depres2uid)))
      (fiveam:is (= 200 status))
      (fiveam:is (string= (format nil "~{/~A~}" (list res2type
                                                      res2uid
                                                      depres1rel1
                                                      depres1type
                                                      depres1uid
                                                      depres2rel1
                                                      depres2type
                                                      depres2uid))
                          (gethash "ScCanonicalpath" (first body)))))
    ;; Clean up the remaining test resources
    (syscat-client:delete-request *server* (format nil "/~A/~A?recursive=true" res1type res1uid))
    (syscat-client:delete-request *server* (format nil "/~A/~A?recursive=true" res2type res2uid))
    ;; Clean up the test schema
    (remove-last-schema *server*)))
