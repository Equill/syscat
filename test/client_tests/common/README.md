# Common resources

Contains resources that are common to 2 or more of the test suites: configs, utilities, content files, whatever.

Its sole purpose is to remove clutter from the parent directory for these tests, by moving it out of immediate view.
