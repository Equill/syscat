;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(in-package #:syscat-clientside-test)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(fiveam:def-suite authenticated-only
                  :description "When the site's auth policy is auth-only."
                  :in main)

(fiveam:in-suite authenticated-only)

(fiveam:test
  refusal-of-unauthenticated
  "Confirm that syscat refuses all actions requested by unauthenticated clients."
  (let ((filepath (make-pathname :defaults (format nil "~A/functionality/"
                                                   *test-directory*)
                                 :name "cats_cuddling.jpg"))
        (schemapath (make-pathname :defaults (format nil "~A/functionality/"
                                                     *test-directory*)
                                   :name "test_schema.json")))
    ;;
    ;; Raw API
    ;;
    ;; GET
    (fiveam:is (= 401 (syscat-client:get-request *server* "/People/ScAdmin" :expected-status-code 401)))
    (fiveam:is (= 401 (syscat-client:get-request *server* "/People" :expected-status-code 401)))
    ;; POST
    (fiveam:is (= 401 (syscat-client:post-request *server* "/People" :payload '(("ScUID" . "Frank")))))
    ;; PUT
    (fiveam:is (= 401 (syscat-client:post-request *server*
                                              "/People/Frank"
                                              :put-p t
                                              :payload '(("displayname" . "The administrator")))))
    ;; DELETE
    (fiveam:is (= 401 (syscat-client:delete-request *server* "/People/Frank")))
    ;;
    ;; Files API
    (fiveam:is (= 401 (syscat-client:upload-file
                        *server*
                        `(("name" . "testfile")
                          ("file" . ,filepath)))))
    ;;
    ;; Schema API
    ;;
    ;; GET
    (fiveam:is (= 401 (syscat-client:get-request *server* "/" :api "schema" :expected-status-code 401)))
    (fiveam:is (= 401 (syscat-client:get-request *server*
                                             "/version=list"
                                             :api "schema"
                                             :expected-status-code 401)))
    ;; POST
    ;; Add schema
    (fiveam:is (= 401 (syscat-client:post-request
                        *server*
                        "/"
                        :api "schema"
                        :payload `(("schema" . ,schemapath)))))
    ;; New version
    (fiveam:is (= 401 (syscat-client:post-request *server* "/" :api "schema" :payload '(("create" . "True")))))
    ;; New version *and* added schema
    (fiveam:is (= 401 (syscat-client:post-request
                        *server*
                        "/"
                        :api "schema"
                        :payload `(("schema" . ,(make-pathname :defaults (format nil "~A/functionality/"
                                                                                 *test-directory*)
                                                               :name "test_schema.json"))
                                   ("create" . "True")))))
    ;; DELETE
    (fiveam:is (= 401 (syscat-client:delete-request
                        *server*
                        "/"
                        :api "schema"
                        :payload `(("version" . ,(format nil "~D" (get-universal-time)))))))
    ;;
    ;; IPAM API
    ;; GET
    (fiveam:is (= 401 (syscat-client:get-request
                        *server*
                        "/subnets?org=testco&subnet=172.16.0.0/12"
                        :expected-status-code 401)))
    ;; POST
    (fiveam:is (= 401 (syscat-client:post-request *server*
                                              "/subnets"
                                              :api "ipam"
                                              :payload '(("org" . "testco")
                                                         ("subnet" . "172.16.0.0/12")))))))

(fiveam:test
  acceptance-of-authenticated
  "Confirm that Syscat responds correctly to authenticated clients."
  (let ((username "ScAdmin")
        (password "thisisabadpassword")
        (cookie-jar (make-instance 'drakma:cookie-jar))
        (user1uid "Frank")
        (attr1name "displayname")
        (attr1val "That one guy. Girl. Uh, whatever...")
        (filepath (make-pathname :defaults (format nil "~A/functionality/"
                                                   *test-directory*)
                                 :name "cats_cuddling.jpg"))
        (filename "Cuddling kitties")
        (schemapath (make-pathname :defaults (format nil "~A/functionality/"
                                                     *test-directory*)
                                   :name "test_schema.json")))
    ;; Authenticate
    (syscat-client:login *server*
                     :username username
                     :password password
                     :cookie-jar cookie-jar)
    ;; Raw API
    ;;
    ;; POST
    (fiveam:is (= 201 (syscat-client:post-request *server*
                                              "/People"
                                              :cookie-jar cookie-jar
                                              :payload `(("ScUID" . ,user1uid)))))
    ;;
    ;; GET
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/People/~A" user1uid) :cookie-jar cookie-jar)
      (fiveam:is (= 200 status))
      (fiveam:is (hash-table-p (first body))))
    ;;
    ;; PUT
    (fiveam:is (= 200 (syscat-client:post-request *server*
                                              (format nil "/People/~A" user1uid)
                                              :put-p t
                                              :cookie-jar cookie-jar
                                              :payload `((,attr1name . ,attr1val)))))
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/People/~A?SCattributes=~A" user1uid attr1name)
                             :cookie-jar cookie-jar)
      (fiveam:is (= 200 status))
      (fiveam:is (equal attr1val
                        (gethash attr1name (first body)))))
    ;;
    ;; DELETE
    (fiveam:is (= 204 (syscat-client:delete-request *server*
                                                (format nil "/People/~A" user1uid)
                                                :cookie-jar cookie-jar)))
    ;; Confirm the deletion
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* (format nil "/People/~A" user1uid) :cookie-jar cookie-jar)
      (fiveam:is (= 200 status))
      (fiveam:is (null (first body))))
    ;;
    ;; Files API
    ;; Upload the file
    (fiveam:is (= 201 (syscat-client:upload-file *server*
                                             `(("name" . ,filename)
                                               ("file" , filepath))
                                             :cookie-jar cookie-jar)))
    ;; Confirm the file's there
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "/Files/~A" (sanitise-uid filename))
                             :cookie-jar cookie-jar)
      (fiveam:is (= 200 status))
      (fiveam:is (hash-table-p (first body))))
    ;; Delete the file
    (fiveam:is (= 204 (syscat-client:delete-request *server*
                                                (format nil "/~A" (sanitise-uid filename))
                                                :api "files"
                                                :cookie-jar cookie-jar)))
    ;; Confirm the file's gone
    (multiple-value-bind (status body)
      (syscat-client:get-request *server*
                             (format nil "/Files/~A" (sanitise-uid filename))
                             :cookie-jar cookie-jar)
      (fiveam:is (= 200 status))
      (fiveam:is (null (first body))))
    ;;
    ;; Schema API
    ;;
    ;; GET
    ;; Single resource
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* "resourcetypes/People" :api "schema" :cookie-jar cookie-jar)
      (fiveam:is (= 200 status))
      (fiveam:is (equal "People" (gethash "name" body))))
    ;; Version-list
    (multiple-value-bind (status body)
      (syscat-client:get-request *server* "?version=list" :api "schema" :cookie-jar cookie-jar)
      (fiveam:is (= 200 status))
      (fiveam:is (hash-table-p body))
      (fiveam:is (listp (gethash "versions" body)))
      (fiveam:is (integerp (gethash "current-version" body))))
      ;;
      ;; POST
      ;; Add a version
      (fiveam:is (= 201 (syscat-client:post-request *server*
                                                "/"
                                                :api "schema"
                                                :cookie-jar cookie-jar
                                                :payload '(("create" . "true")))))
      ;; Upload a subschema
      (fiveam:is (= 201 (syscat-client:post-request *server*
                                                "/"
                                                :api "schema"
                                                :cookie-jar cookie-jar
                                                :payload `(("schema" . ,schemapath)))))
      ;; Remove that version
      (fiveam:is (= 200 (remove-last-schema *server* :cookie-jar cookie-jar)))
    ;;
    ;; Log out
    (syscat-client:logout *server* cookie-jar)))
