# Create a release suitable for deployment

# Establish the version suffix
if [[ -z $1 ]]
then
  SUFFIX="snapshot"
else
  SUFFIX=${1}
fi

# Define the name of the directory into which we'll copy the executable
# and its supporting files.
DIRNAME="syscat_$SUFFIX"

# Create the executable
./create_executable.sh

# Ensure the release directory is present
RELEASES_DIR="../../releases"
# It's not there there
if [[ ! -d $RELEASES_DIR ]]
then
  # There's a non-directory file at that path
  if [[ -e $RELEASES_DIR ]]
  then
    echo "Please remove or rename the file at $RELEASES_DIR"
    exit 1
  # Nothing there
  else
    echo "Creating builds directory $RELEASES_DIR - please stand by."
    mkdir $RELEASES_DIR
  fi
fi


# Create the directory for this version if it's not already present.
# If it already exists, assume we're refreshing whatever's there.
if [[ ! -d "$RELEASES_DIR/$DIRNAME" ]]
then
  mkdir "$RELEASES_DIR/$DIRNAME"
fi

# Copy the executable in there
cp syscat "$RELEASES_DIR/$DIRNAME/"

# Copy the supporting files in there
cp -r ../../src/templates "$RELEASES_DIR/$DIRNAME/"
cp syscat.service "$RELEASES_DIR/$DIRNAME/"

# Turn it into a tarball for uploading and deploying.
cd $RELEASES_DIR
tar czf $DIRNAME.tar.gz $DIRNAME && rm -r $DIRNAME
