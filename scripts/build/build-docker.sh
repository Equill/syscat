# Establish the version suffix
if [[ -z $1 ]]
then
  SUFFIX="snapshot"
else
  SUFFIX=${1}
fi

# Copy local dependencies into place
mkdir -p resources/src
#rsync -a --delete-after ~/quicklisp/ resources/quicklisp/
rsync -a --delete-after ~/devel/lisp/bordeaux-threads/ resources/src/bordeaux-threads/
rsync -a --delete-after ~/devel/lisp/cl-cidr-notation/ resources/src/cl-cidr-notation/
rsync -a --delete-after ~/devel/lisp/ironclad/ resources/src/ironclad/
rsync -a --delete-after ~/devel/lisp/ipaddress/ resources/src/ipaddress/
rsync -a --delete-after ~/devel/lisp/neo4cl/ resources/src/neo4cl/
rsync -a --delete-after ../../src/ resources/src/syscat/
rsync -a --delete-after ../../schemas/ resources/src/syscat/schemas/

NOCACHE=true
docker build --no-cache=$NOCACHE --progress=plain -t equill/syscat:$SUFFIX ./
