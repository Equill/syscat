;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(defun subnet-dispatcher-v1 ()
  "Hunchentoot dispatch function for the IPAM-specific REST API, version 1. Subnet subset."
  (let ((access-policy (raw-policy (access-policy tbnl:*acceptor*)))
        ;; Oh, so you have a sessionid?
        ;; Let's just check whether that's connected to a valid session.
        (auth-session (when (tbnl:cookie-in "sessionid")
                        (fetch-session (session-server tbnl:*acceptor*)
                                       (tbnl:cookie-in "sessionid")))))
    (handler-case
      (cond
        ;; Access policies: dispatch "deny" as early as possible
        ((or (and (equal :GET (tbnl:request-method*))
                  (equal :DENY (get-policy access-policy)))
             (and (equal :POST (tbnl:request-method*))
                  (equal :DENY (post-policy access-policy)))
             (and (equal :DELETE (tbnl:request-method*))
                  (equal :DENY (delete-policy access-policy))))
         (forbidden))
        ;;
        ;; Access policies: if the policy is other than :ALLOW-ALL for this method
        ;; and there's no valid session, deny now.
        ((and (not auth-session)
              (or (and (equal :GET (tbnl:request-method*))
                       (not (equal :ALLOW-ALL (get-policy access-policy))))
                  (and (equal :POST (tbnl:request-method*))
                       (not (equal :ALLOW-ALL (post-policy access-policy))))
                  (and (equal :DELETE (tbnl:request-method*))
                       (not (equal :ALLOW-ALL (delete-policy access-policy))))))
         (unauthorised))
        ;;
        ;; Access policies: is this an admin-only request?
        ((and auth-session
              (not (equal "ScAdmin" (username auth-session)))
              (or
                (and (equal :GET (tbnl:request-method*))
                     (equal :ALLOW-ADMIN (get-policy access-policy)))
                (and (equal :POST (tbnl:request-method*))
                     (equal :ALLOW-ADMIN (post-policy access-policy)))
                (and (equal :PUT (tbnl:request-method*))
                     (equal :ALLOW-ADMIN (put-policy access-policy)))
                (and (equal :DELETE (tbnl:request-method*))
                     (equal :ALLOW-ADMIN (delete-policy access-policy)))))
         (unauthorised))
        ;;
        ;; Create a subnet
        ((and (equal (tbnl:request-method*) :POST)
              (tbnl:post-parameter "subnet")
              (tbnl:post-parameter "org"))
         (log-message :debug (format nil "Dispatching POST request for URI ~A" (tbnl:request-uri*)))
         (let ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*))))
           (cond
             ;; Sanity-check: does the organisation exist?
             ((not (resource-exists-p session (list "Organisations" (tbnl:post-parameter "org"))))
              (neo4cl:disconnect session)
              (return-client-error
                (format nil "Organisation '~A' does not exist" (tbnl:post-parameter "org"))))
             ;; It's passed all the sanity checks so far; insert it
             (t
               (let ((result
                       (insert-subnet session
                                      (resourcetype-schema tbnl:*acceptor*)
                                      (tbnl:post-parameter "org")
                                      (if (ipaddress:ipv4-subnet-p (tbnl:post-parameter "subnet"))
                                        (ipaddress:make-ipv4-subnet (tbnl:post-parameter "subnet"))
                                        (ipaddress:make-ipv6-subnet (tbnl:post-parameter "subnet")))
                                      (post-policy access-policy)
                                      :vrf (when (tbnl:post-parameter "vrf")
                                             (tbnl:post-parameter "vrf"))
                                      :creator-id (if auth-session
                                                    (username auth-session)
                                                    "ScAdmin")))
                     (returnval
                       (format-subnet-path
                         (tbnl:post-parameter "org")
                         (tbnl:post-parameter "vrf")
                         (find-subnet session
                                      (resourcetype-schema tbnl:*acceptor*)
                                      (tbnl:post-parameter "org")
                                      (if (ipaddress:ipv4-subnet-p (tbnl:post-parameter "subnet"))
                                        (ipaddress:make-ipv4-subnet (tbnl:post-parameter "subnet"))
                                        (ipaddress:make-ipv6-subnet (tbnl:post-parameter "subnet")))
                                      :vrf (tbnl:post-parameter "vrf")))))
                 ;; Clean up the Bolt connection that's served its purpose
                 (neo4cl:disconnect session)
                 ;; Return it to the client for confirmation
                 (log-message
                   :debug
                   (format nil
                           (if result
                             "Stored subnet ~A. Now retrieving it for positive confirmation."
                             "Subnet ~A was already present. Retrieving it for positive confirmation.")
                           (tbnl:post-parameter "subnet")))
                 (setf (tbnl:content-type*) "application/json")
                 (setf (tbnl:return-code*) (if result
                                             tbnl:+http-created+
                                             tbnl:+http-ok+))
                 returnval)))))
        ;;
        ;; Search for a subnet
        ((and (equal (tbnl:request-method*) :GET)
              (tbnl:get-parameter "subnet")
              (tbnl:get-parameter "org"))
         (log-message :debug (format nil "Dispatching GET request for URI ~A"
                                     (tbnl:request-uri*)))
         ;; Go look for it
         (handler-case
           (let* ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
                  (result (find-subnet session
                                       (resourcetype-schema tbnl:*acceptor*)
                                       (tbnl:get-parameter "org")
                                       (if (ipaddress:ipv4-subnet-p (tbnl:get-parameter "subnet"))
                                         (ipaddress:make-ipv4-subnet (tbnl:get-parameter "subnet"))
                                         (ipaddress:make-ipv6-subnet (tbnl:get-parameter "subnet")))
                                       :vrf (tbnl:get-parameter "vrf"))))
             ;; Clean up the Bolt session
             (neo4cl:disconnect session)
             (log-message :debug (format nil "Result obtained: ~A~%" result))
             ;; Did we find one?
             (if (or (null result)
                     (equal result ""))
               ;; Not found
               (progn
                 (setf (tbnl:content-type*) "application/json")
                 (setf (tbnl:return-code*) tbnl:+http-ok+)
                 "[]")
               ;; Found it!
               (progn
                 (setf (tbnl:content-type*) "application/json")
                 (setf (tbnl:return-code*) tbnl:+http-ok+)
                 (let ((output (list (format-subnet-path
                                       (tbnl:get-parameter "org")
                                       (tbnl:get-parameter "vrf")
                                       result))))
                   (log-message :debug (format nil "Retrieved subnet path ~A" output))
                   ;; Actually return it to the appserver
                   (cl-json:encode-json-to-string output)))))
           ;; Attempted violation of db integrity
           (integrity-error (e) (return-integrity-error (message e)))
           ;; Generic client errors
           (client-error (e) (return-client-error (message e)))))
        ;;
        ;; Delete a subnet
        ((and (equal (tbnl:request-method*) :DELETE)
              (tbnl:parameter "subnet")
              (tbnl:parameter "org"))
         (log-message :debug (format nil "Dispatching DELETE request for URI ~A"
                                     (tbnl:request-uri*)))
         (let ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*))))
           (delete-subnet session
                          (tbnl:parameter "org")
                          (if (ipaddress:ipv4-subnet-p (tbnl:parameter "subnet"))
                            (ipaddress:make-ipv4-subnet (tbnl:parameter "subnet"))
                            (ipaddress:make-ipv6-subnet (tbnl:parameter "subnet")))
                          (resourcetype-schema tbnl:*acceptor*)
                          :vrf (when (tbnl:parameter "vrf") (tbnl:parameter "vrf")))
           (neo4cl:disconnect session))
         (setf (tbnl:content-type*) "text/plain")
         (setf (tbnl:return-code*) tbnl:+http-no-content+)
         "")
        ;; Methods we don't support.
        ;; Take the whitelist approach
        ((not (member (tbnl:request-method*) '(:POST :GET :DELETE)))
         (method-not-allowed))
        ;;
        ;; Handle all other cases
        (t
          (return-client-error "This wasn't a valid request")))
      ;; Handle general errors
      ;;
      ;; Generic client errors
      (client-error (e) (return-client-error (message e)))
      (neo4cl:client-error (e) (return-client-error (neo4cl:message e)))
      ;; Transient error
      (neo4cl:transient-error (e) (return-transient-error e))
      ;; Database error
      (neo4cl:database-error (e) (return-database-error e)))))


(defun address-dispatcher-v1 ()
  "Hunchentoot dispatch function for the IPAM-specific REST API, version 1. Address subset."
  (let ((access-policy (raw-policy (access-policy tbnl:*acceptor*)))
        ;; Oh, so you have a sessionid?
        ;; Let's just check whether that's connected to a valid session.
        (auth-session (when (tbnl:cookie-in "sessionid")
                        (fetch-session (session-server tbnl:*acceptor*)
                                       (tbnl:cookie-in "sessionid")))))
    (handler-case
      (cond
        ;; Access policies: dispatch "deny" as early as possible
        ((or (and (equal :GET (tbnl:request-method*))
                  (equal :DENY (get-policy access-policy)))
             (and (equal :POST (tbnl:request-method*))
                  (equal :DENY (post-policy access-policy)))
             (and (equal :DELETE (tbnl:request-method*))
                  (equal :DENY (delete-policy access-policy))))
         (forbidden))
        ;;
        ;; Access policies: if the policy is other than :ALLOW-ALL for this method
        ;; and there's no valid session, deny now.
        ((and (not auth-session)
              (or (and (equal :GET (tbnl:request-method*))
                       (not (equal :ALLOW-ALL (get-policy access-policy))))
                  (and (equal :POST (tbnl:request-method*))
                       (not (equal :ALLOW-ALL (post-policy access-policy))))
                  (and (equal :DELETE (tbnl:request-method*))
                       (not (equal :ALLOW-ALL (delete-policy access-policy))))))
         (unauthorised))
        ((and auth-session
              (not (equal "ScAdmin" (username auth-session)))
              (or
                (and (equal :GET (tbnl:request-method*))
                     (equal :ALLOW-ADMIN (get-policy access-policy)))
                (and (equal :POST (tbnl:request-method*))
                     (equal :ALLOW-ADMIN (post-policy access-policy)))
                (and (equal :PUT (tbnl:request-method*))
                     (equal :ALLOW-ADMIN (put-policy access-policy)))
                (and (equal :DELETE (tbnl:request-method*))
                     (equal :ALLOW-ADMIN (delete-policy access-policy)))))
         (unauthorised))
        ;;
        ;;
        ;; Store an address
        ((and (equal (tbnl:request-method*) :POST)
              (tbnl:post-parameter "address")
              (tbnl:post-parameter "org"))
         (log-message :debug (format nil "Dispatching POST request for URI ~A" (tbnl:request-uri*)))
         (handler-case
           (let ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
                 (address (make-instance (if (ipaddress:ipv4-address-p (tbnl:post-parameter "address"))
                                           'ipaddress:ipv4-address
                                           'ipaddress:ipv6-address)
                                         :address (tbnl:post-parameter "address"))))
             (insert-ipaddress session
                               (resourcetype-schema tbnl:*acceptor*)
                               address
                               (tbnl:post-parameter "org")
                               (post-policy access-policy)
                               :vrf (tbnl:post-parameter "vrf")
                               :creator-id (if auth-session
                                             (username auth-session)
                                             "ScAdmin"))
             (log-message :debug
                          (format nil "Stored address ~A. Now retrieving it for positive confirmation."
                                  (ipaddress:as-string address)))
             (let ((returnval
                     (format-address-path
                       (tbnl:post-parameter "org")
                       (tbnl:post-parameter "vrf")
                       (find-ipaddress session
                                       (resourcetype-schema tbnl:*acceptor*)
                                       address
                                       (tbnl:post-parameter "org")
                                       :vrf (tbnl:post-parameter "vrf")))))
               ;; Clean up the Bolt session we no longer need here
               (neo4cl:disconnect session)
               ;; Return it to the client for confirmation
               (setf (tbnl:content-type*) "application/json")
               (setf (tbnl:return-code*) tbnl:+http-created+)
               returnval))
           (client-error (e) (return-client-error (message e)))))
        ;;
        ;; Search for an address
        ((and (equal (tbnl:request-method*) :GET)
              (tbnl:get-parameter "address")
              (tbnl:get-parameter "org"))
         (log-message :debug (format nil "Dispatching GET request for URI ~A" (tbnl:request-uri*)))
         ;; Go look for it
         (handler-case
           (let* ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
                  (result (find-ipaddress
                            session
                            (resourcetype-schema tbnl:*acceptor*)
                            (make-instance (if (ipaddress:ipv4-address-p (tbnl:get-parameter "address"))
                                             'ipaddress:ipv4-address
                                             'ipaddress:ipv6-address)
                                           :address (tbnl:get-parameter "address"))
                            (tbnl:get-parameter "org")
                            :vrf (when (tbnl:get-parameter "vrf") (tbnl:get-parameter "vrf")))))
             ;; Clean up the Bolt session
             (neo4cl:disconnect session)
             (log-message :debug (format nil "Result obtained: ~A~%" result))
             ;; Did we find one?
             (if (or (null result)
                     (equal result ""))
               ;; Not found
               (progn
                 (setf (tbnl:content-type*) "application/json")
                 (setf (tbnl:return-code*) tbnl:+http-ok+)
                 "[]")
               ;; Found it!
               (progn
                 (setf (tbnl:content-type*) "application/json")
                 (setf (tbnl:return-code*) tbnl:+http-ok+)
                 (cl-json:encode-json-to-string
                   (list (format-address-path (tbnl:get-parameter "org")
                                            (tbnl:get-parameter "vrf")
                                            result))))))
           ;; Failure to parse: client supplied invalid input for the address
           (sb-int:simple-parse-error
             (e)
             (declare (ignore e))
             (return-client-error "Invalid value received. Are you sure that was an IP address?"))
           ;; No parent subnet for this address
           (undefined-relationship
             (e)
             (declare (ignore e))
             (return-client-error "There is no valid parent subnet for this address."))
           ;; Attempted violation of db integrity
           (integrity-error (e) (return-integrity-error (message e)))
           ;; Generic client errors
           (client-error (e) (return-client-error (message e)))))
        ;;
        ;; Delete an address
        ((and (equal (tbnl:request-method*) :DELETE)
              (tbnl:parameter "address")
              (tbnl:parameter "org"))
         (log-message :debug (format nil "Dispatching DELETE request for URI ~A" (tbnl:request-uri*)))
         (let ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*))))
           (delete-ipaddress session
                             (resourcetype-schema tbnl:*acceptor*)
                             (make-instance (if (ipaddress:ipv4-address-p (tbnl:parameter "address"))
                                              'ipaddress:ipv4-address
                                              'ipaddress:ipv6-address)
                                            :address (tbnl:parameter "address"))
                             (tbnl:parameter "org")
                             :vrf (tbnl:parameter "vrf"))
           (neo4cl:disconnect session))
         (setf (tbnl:content-type*) "text/plain")
         (setf (tbnl:return-code*) tbnl:+http-no-content+)
         "")
        ;; Reject any methods we don't support.
        ;; Take the whitelist approach
        ((not (member (tbnl:request-method*) '(:POST :GET :DELETE)))
         (method-not-allowed))
        ;;
        ;; Handle all other cases
        (t
          (return-client-error "This wasn't a valid request")))
      ;; Handle general errors
      ;;
      ;; Generic client errors
      (neo4cl:client-error (e) (return-client-error (neo4cl:message e)))
      (client-error (e) (return-client-error (neo4cl:message e)))
      ;; Transient error
      (neo4cl:transient-error (e) (return-transient-error e))
      ;; Database error
      (neo4cl:database-error (e) (return-database-error e))
      ;; Service errors, e.g. connection refused
      (neo4cl:service-error (e) (return-service-error (neo4cl:message e))))))
