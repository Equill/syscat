;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Resource-related methods

(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(defclass policy-set ()
  ((raw :initarg :raw-policy
        :reader raw-policy
        :type access-policy
        :documentation "Access policy for the RAW and IPAM APIs.")
   (schema :initarg :schema-policy
           :reader schema-policy
           :type access-policy
           :documentation "Access policy for the Schema API.")
   (files :initarg :files-policy
          :reader files-policy
          :type access-policy
          :documentation "Access policy for the Files API. The PUT policy is irrelevant here."))
  (:documentation "Set of policies, one for each of the API endpoints."))


(defclass access-policy ()
  ((get-policy :initarg :get-policy
               :reader get-policy)
   (post-policy :initarg :post-policy
                :reader post-policy)
   (put-policy :initarg :put-policy
               :reader put-policy)
   (delete-policy :initarg :delete-policy
                  :reader delete-policy))
  (:documentation "Selector object, determining which access policies are applied"))

(defun make-access-policy (&key (get-policy :ALLOW-ALL)
                                (post-policy :ALLOW-ALL)
                                (put-policy :ALLOW-ALL)
                                (delete-policy :ALLOW-ALL))
  "Constructor function for access-policy instances."
  (declare (type keyword get-policy post-policy put-policy delete-policy))
  (let ((vals '(:DENY
                 :ALLOW-ALL
                 :ALLOW-AUTHENTICATED
                 :ALLOW-ADMIN)))
    (mapcar (lambda (arg)
              (unless (member arg vals)
                (error (format nil "All args must be one of ~{~A~^, ~}" vals))))
            (list get-policy post-policy put-policy delete-policy))
    (make-instance 'access-policy
                   :get-policy get-policy
                   :post-policy post-policy
                   :put-policy put-policy
                   :delete-policy delete-policy)))

(defun define-policy-set (policyname)
  "Create and return a policy-set object, based on a set of predefined policies.
   Acceptable values are 'open', 'readonly' and 'write-authenticated'."
  (declare (type string policyname))
  (cond
    ;; Everything fully open.
    ;; Max convenience, but only do this on your personal computer.
    ((equal "open" policyname)
     (make-instance 'policy-set
                    :raw-policy (make-access-policy)
                    :schema-policy (make-access-policy)
                    :files-policy (make-access-policy)))
    ;; readonly - good for allowing access while you investigate who broke something.
    ((equal "read-only" policyname)
     (make-instance 'policy-set
                    :raw-policy (make-access-policy :get-policy :ALLOW-ALL
                                                    :post-policy :DENY
                                                    :put-policy :DENY
                                                    :delete-policy :DENY)
                    :schema-policy (make-access-policy :get-policy :ALLOW-ALL
                                                       :post-policy :DENY
                                                       :put-policy :DENY
                                                       :delete-policy :DENY)
                    :files-policy (make-access-policy :get-policy :ALLOW-ALL
                                                      :post-policy :DENY
                                                      :put-policy :DENY
                                                      :delete-policy :DENY)))
    ;; write-authenticated - anybody can make GET requests, but only authenticated users
    ;; can make POST, PUT and DELETE requests.
    ((equal "write-authenticated" policyname)
     (make-instance 'policy-set
                    :raw-policy (make-access-policy :get-policy :ALLOW-ALL
                                                    :post-policy :ALLOW-AUTHENTICATED
                                                    :put-policy :ALLOW-AUTHENTICATED
                                                    :delete-policy :ALLOW-AUTHENTICATED)
                    :schema-policy (make-access-policy :get-policy :ALLOW-ALL
                                                       :post-policy :ALLOW-AUTHENTICATED
                                                       :put-policy :ALLOW-AUTHENTICATED
                                                       :delete-policy :ALLOW-AUTHENTICATED)
                    :files-policy (make-access-policy :get-policy :ALLOW-ALL
                                                      :post-policy :ALLOW-AUTHENTICATED
                                                      :put-policy :ALLOW-AUTHENTICATED
                                                      :delete-policy :ALLOW-AUTHENTICATED)))
    ;; authenticated-only - all unauthenticated requests are denied
    ((equal "authenticated-only" policyname)
     (make-instance 'policy-set
                    :raw-policy (make-access-policy :get-policy :ALLOW-AUTHENTICATED
                                                    :post-policy :ALLOW-AUTHENTICATED
                                                    :put-policy :ALLOW-AUTHENTICATED
                                                    :delete-policy :ALLOW-AUTHENTICATED)
                    :schema-policy (make-access-policy :get-policy :ALLOW-AUTHENTICATED
                                                       :post-policy :ALLOW-AUTHENTICATED
                                                       :put-policy :ALLOW-AUTHENTICATED
                                                       :delete-policy :ALLOW-AUTHENTICATED)
                    :files-policy (make-access-policy :get-policy :ALLOW-AUTHENTICATED
                                                      :post-policy :ALLOW-AUTHENTICATED
                                                      :put-policy :ALLOW-AUTHENTICATED
                                                      :delete-policy :ALLOW-AUTHENTICATED)))
    ;; Fallback: error out.
    (t (error 'misconfiguration :message "No such policy."))))
