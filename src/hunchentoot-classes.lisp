;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;; Classes that need to be predefined because of the way SBCL's compiler works

(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(defclass syscat-acceptor (tbnl:easy-acceptor)
  ;; Class attributes
  ((datastore :initarg :datastore
              :reader datastore
              :initform (error "Datastore object must be supplied.")
              :documentation "An object representing the datastore, on which the generic functions will be dispatched.")
   (cookie-domain :initarg :cookie-domain
                  :reader cookie-domain
                  :initform "subdomain.example.com")
   (uri-base-api :initarg :uri-base-api
                 :reader uri-base-api
                 :initform "/raw/v1"
                 :documentation "Base URI on which the raw API is to be presented.")
   (uri-base-schema :initarg :uri-base-schema
                    :reader uri-base-schema
                    :initform "/schema/v1"
                    :documentation "Base URI on which the schema API is to be presented.")
   (uri-base-files :initarg :uri-base-files
                   :reader uri-base-files
                   :initform "/files/v1"
                   :documentation "Base URI on which the files API is to be presented.")
   (uri-base-subnets :initarg :uri-base-subnets
                     :reader uri-base-subnets
                     :initform "/ipam/v1/subnets")
   (uri-base-addresses :initarg :uri-base-addresses
                       :reader uri-base-addresses
                       :initform "/ipam/v1/addresses")
   (files-location :initarg :files-location
                   :reader files-location
                   :initform (error "files-location is required")
                   :documentation "Parent directory under which file objects are to be stored.")
   (template-path :initarg :template-path
                  :reader template-path
                  :initform "/tmp")
   (resourcetype-schema :initarg :resourcetype-schema
                        :accessor resourcetype-schema
                        :initform (make-schema-hash-table)
                        :documentation "What to use for resourcetype-schema actions. Valid options are a hash-table or the datastore object, as methods are specialised on both.")
   (relationship-schema :initarg :relationship-schema
                        :accessor relationship-schema
                        :initform (make-schema-hash-table)
                        :documentation "What to use for relationship-schema actions. Valid options are a hash-table or the datastore object, as methods are specialised on both.")
   (access-policy :initarg :access-policy
                  :reader access-policy
                  :type policy-set
                  :initform (error "Required argument")
                  :documentation "The set of access policies to be applied to each incoming request. Must be in the form of a `policy-set` object.")
   (auth-server :initarg :auth-server
                :reader auth-server
                :initform (error "auth-server must be specified explicitly")
                :type (or null ldap-server auth-bolt-server)
                :documentation "Object on which the authentication requests are dispatched.")
   (session-server :initarg :session-server
                   :reader session-server
                   :initform (error "session-server must be specified explicitly")
                   :type (or null redis-server auth-bolt-server)
                   :documentation "Object on which the session requests are dispatched."))
  ;; Class defaults for initalising the superclass
  (:default-initargs :address "127.0.0.1")
  ;; Class documentation
  (:documentation "Customised Hunchentoot acceptor, subclassed from tbnl:easy-acceptor. Carries additional configuration data for the site."))


;; Helper functions

(defun make-default-bolt-server ()
  "Return an instance of `neo4cl:bolt-server`.
   Use environment variables where they're set, and reasonable-looking defaults otherwise.
   Refactored as a separate function for re-use in the test suite,
   with the bonus side-effect of simplifying make-default-acceptor."
  (make-instance
    'neo4cl:bolt-server
    :hostname (or (sb-ext:posix-getenv "NEO4J_HOSTNAME")
                  "192.0.2.1")
    :port (if (sb-ext:posix-getenv "NEO4J_PORT")
            (parse-integer (sb-ext:posix-getenv "NEO4J_PORT"))
            7687)
    ;; Adapt to the authentication scheme in effect
    :auth-token (cond
                  ;; Null authentication
                  ;; Put this first, because it'd be overriding the default
                  ((equal "none" (sb-ext:posix-getenv "BOLT_AUTH_SCHEME"))
                   (make-instance
                     'neo4cl:bolt-auth-none
                     :username (or (sb-ext:posix-getenv "NEO4J_USER")
                                   "neo4j")))
                  ;; Basic authentication
                  ;; If we're using `docker secrets` with Docker Swarm,
                  ;; we need to pull the password from a file.
                  ((or (sb-ext:posix-getenv "NEO4J_PASSWORD_FILE")
                       (sb-ext:posix-getenv "NEO4J_PASSWORD"))
                   (make-instance
                     'neo4cl:bolt-auth-basic
                     :username (or (sb-ext:posix-getenv "NEO4J_USER")
                                   "neo4j")
                     ;; Again, check for the file _first_.
                     ;; If that's specified, it takes precedence over one supplied via env var.
                     :password (if (sb-ext:posix-getenv "NEO4J_PASSWORD_FILE")
                                 (with-open-file (infile (sb-ext:posix-getenv "NEO4J_PASSWORD_FILE")
                                                         :direction :input)
                                   (read-line infile))
                                 (sb-ext:posix-getenv "NEO4J_PASSWORD"))))
                  (t
                   (error "Need both NEO4J_USER and either NEO4J_PASSWORD or NEO4J_PASSWORD_FILE")))))

(defun make-default-acceptor ()
  "Generate a syscat-acceptor instance, automagically using environment variables
  where they're set, and falling back to sensible-looking defaults where they're not.
  Returns an instance of `syscat-acceptor`."
  (let* ((cookie-domain (or (sb-ext:posix-getenv "COOKIE_DOMAIN")
                            "subdomain.example.com"))
         ;; Derive the LDAP Base DN from the cookie domain
         ;; if it wasn't explicitly specified.
         (ldap-base-dn (or (sb-ext:posix-getenv "LDAP_BASE_DN")
                           (format nil "~{dc=~A~^,~}"
                                   (remove-if (lambda (foo) (equal "" foo))
                                              (cl-ppcre:split "\\." cookie-domain))))))
    (make-instance 'syscat-acceptor
                   :address (or (sb-ext:posix-getenv "LISTEN_ADDR")
                                "0.0.0.0")
                   :port (or (when (sb-ext:posix-getenv "LISTEN_PORT")
                               (parse-integer (sb-ext:posix-getenv "LISTEN_PORT")))
                             4950)
                   :cookie-domain cookie-domain
                   :uri-base-api "/raw/v1"
                   :uri-base-schema "/schema/v1"
                   :uri-base-files "/files/v1"
                   :files-location (or (sb-ext:posix-getenv "FILES_LOCATION")
                                       "/tmp/syscat-files/")
                   :template-path (make-pathname :defaults
                                                 (or (sb-ext:posix-getenv "TEMPLATE_PATH")
                                                     "/templates/"))
                   ;; Send all logs to STDOUT, and let Docker sort 'em out
                   :access-log-destination (make-synonym-stream 'cl:*standard-output*)
                   :message-log-destination (make-synonym-stream 'cl:*standard-output*)
                   ;; Datastore object - for specialising all the db methods on
                   :datastore (make-default-bolt-server)
                   :access-policy (define-policy-set (or (sb-ext:posix-getenv "ACCESS_POLICY")
                                                         "open"))
                   ;; For the auth-server and session-server
                   ;; we're overriding the defaults iff an override was supplied.
                   :auth-server (when (member (sb-ext:posix-getenv "ACCESS_POLICY")
                                              '("write-authenticated"
                                                "authenticated-only")
                                              :test #'equal)
                                  (if
                                    (and (sb-ext:posix-getenv "AUTH_NEO4J_HOSTNAME")
                                         (or (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD_FILE")
                                             (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD")))
                                    (make-auth-bolt-server)
                                    (make-instance
                                      'ldap-server
                                      :base-dn ldap-base-dn
                                      :host (or (sb-ext:posix-getenv "LDAP_HOST")
                                                "192.0.2.1")
                                      :port (if (sb-ext:posix-getenv "LDAP_PORT")
                                              (parse-integer (sb-ext:posix-getenv "LDAP_PORT"))
                                              389))))
                   :session-server (when (member (sb-ext:posix-getenv "ACCESS_POLICY")
                                                 '("write-authenticated"
                                                   "authenticated-only")
                                                 :test #'equal)
                                     (if
                                       (and (sb-ext:posix-getenv "AUTH_NEO4J_HOSTNAME")
                                            (or (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD_FILE")
                                                (sb-ext:posix-getenv "AUTH_NEO4J_PASSWORD")))
                                       (make-auth-bolt-server)
                                       (let ((redis-server (make-instance 'redis-server)))
                                         ;; Now modify the host, if that was specified
                                         (when (sb-ext:posix-getenv "REDIS_HOST")
                                           (setf (host redis-server) (sb-ext:posix-getenv "REDIS_HOST")))
                                         (when (sb-ext:posix-getenv "REDIS_PORT")
                                           (setf (port redis-server) (sb-ext:posix-getenv "REDIS_PORT")))
                                         ;; Return the object
                                         redis-server))))))
