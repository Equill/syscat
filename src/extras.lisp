;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Extra utilities that may or may not become part of the main system at some point

(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(defclass rel-to-rename ()
  ((source-type :initarg :source-type
                :reader source-type
                :type string
                :initform (error "source-type must be specified")
                :documentation "The name of the source resourcetype in this relationship")
   (target-type :initarg :target-type
                :reader target-type
                :type string
                :initform (error "target-type must be specified")
                :documentation "The name of the target resourcetype in this relationship")
   (old-relname :initarg :old-relname
                :reader old-relname
                :type string
                :initform (error "old-relname must be specified")
                :documentation "The name of the existing relationship that needs to be renamed.")
   (new-relname :initarg :new-relname
                :reader new-relname
                :type string
                :initform (error "new-relname must be specified")
                :documentation "The new name for the relationship"))
  (:documentation "Represents a relationship to be renamed"))

(defun rename-relationship (session reldef)
  "Renames existing relationships between resources, to match changes in the schema."
  (declare
    (type neo4cl:bolt-session session)
    ;(ignore session)
    (type rel-to-rename reldef))
  (log-message :debug (format nil "Renaming relationship '~A' from '~A' to '~A, to new name '~A'"
                              (old-relname reldef)
                              (source-type reldef)
                              (target-type reldef)
                              (new-relname reldef)))
  (let ((add-query (format nil "MATCH (s:~A)-[r:~A]-(t:~A) CREATE (s)-[:~A]->(t)"
                           (source-type reldef)
                           (old-relname reldef)
                           (target-type reldef)
                           (new-relname reldef)))
        (delete-query (format nil "MATCH (:~A)-[r:~A]-(:~A) DELETE r"
                              (source-type reldef)
                              (old-relname reldef)
                              (target-type reldef))))
    (log-message :debug (format nil "Adding new rel with query string: ~A" add-query))
    (neo4cl:bolt-transaction-autocommit session add-query)
    (log-message :debug (format nil "Deleting old rel with query string: ~A" delete-query))
    (neo4cl:bolt-transaction-autocommit session delete-query)))

(defun read-renames (filepath)
  "Digest a pipe-delimited file, and return a list of rel-to-rename instances.
   Expected format of each line of the file is source-type|oldrel|target-type|newrel"
  (flet ((makeobj (lst)
           (make-instance 'rel-to-rename
                          :source-type (first lst)
                          :old-relname (second lst)
                          :target-type (third lst)
                          :new-relname (fourth lst))))
    (with-open-file (infile filepath)
      (loop for (line no-nl-p) = (multiple-value-list (read-line infile nil nil))
        while line
        collect (makeobj (cl-ppcre:split "\\|" line))))))
