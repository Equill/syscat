;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Schema-related functions, specific to extracting a schema *from* the database.

(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


;;; Extract a schema from the database

(defun make-schema-hash-table ()
  "Convenience function for repeatably creating the kind of hash-table we expect."
  (make-hash-table :test #'equal))

(defun fetch-current-schema (session)
  "Return a hash-table representing the current schema version,
  populated by definitions retrieved from the database.
  Keys = resourcetype names.
  Values = schema-rtypes instances."
  (declare (type neo4cl:bolt-session session))
  (log-message :info "Fetching the current schema from the database.")
  ;; Create a schema structure
  (let ((resourcetype-schema (make-schema-hash-table))
        (relationship-schema (make-schema-hash-table))
        (current-version (current-schema-version session)))
    ;; Populate the resourcetype-schema with resourcetypes
    (mapcar (lambda (rtype)
              (log-message
                :debug
                (format nil "Fetching initial details for resourcetype '~A'" rtype))
              ;; Add it to the main resourcetype-schema hash-table
              (setf (gethash rtype resourcetype-schema)
                    (describe-resource-type session rtype current-version))
              ;; Confirm what's now in the resourcetype-schema hash-table
              (log-message :debug (format nil "Added resourcetype-schema entry ~A"
                                          (a-listify (gethash rtype resourcetype-schema))))
              ;; This logging is disabled by default
              ;; because it drowns out other useful information with noise
              ;(log-message :debug (format nil "Current state of resourcetype-schema:~%"))
              #+(or)
              (maphash (lambda (name rtype-obj)
                         (log-message :debug
                                      (format nil "Resourcetype '~A': ~A"
                                              name (a-listify rtype-obj))))
                       resourcetype-schema))
            (get-resourcetype-names session current-version))
    ;; Dump the entire schema for a point-in-time reference
    ;; This logging is disabled by default
    ;; because it drowns out other useful information with noise
    ;(log-message :debug (format nil "Full state of schema after loading resourcetypes:~%"))
    #+(or)
    (maphash (lambda (name rtype-obj)
               (log-message :debug
                            (format nil "Resourcetype '~A': ~A" name (a-listify rtype-obj))))
             resourcetype-schema)
    ;; Inject the relationships
    (log-message :debug "Associating relationships with source resourcetypes")
    (mapcar (lambda (relname)
              ;; Generate a schema-rels instance corresponding to this relationship's name
              (let ((rel (get-relationship-from-db session
                                                   current-version
                                                   relname
                                                   resourcetype-schema)))
                ;; Add this instance to the relationship-schema table
                (setf (gethash (name rel) relationship-schema) rel)
                ;; For each source resourcetype, add this relationship to its list of relationships
                (mapcar (lambda (sourcetype)
                          (log-message :debug (format nil "Associating relationship '~A' with source-type '~A'"
                                                      (name rel) (name sourcetype)))
                          (setf (relationships sourcetype)
                                (pushnew rel (relationships sourcetype)
                                         :test #'equal
                                         :key #'name)))
                        (source-types rel))))
            (list-relationships session current-version))
    ;; Now put the relationships in alphabetical order, so we only have to sort them once
    (log-message :debug "Putting relationships in alphabetical order.")
    (maphash (lambda (typename rtype)
               (declare (ignore typename))
               (setf (relationships rtype) (sort (relationships rtype) #'string< :key #'name))
               ;(log-message :debug (format nil "Type of '~A' is '~A'" typename (type-of rtype)))
               )
             resourcetype-schema)
    (log-message :debug "Sorted.")
    ;; Return the schemas we created
    (cons resourcetype-schema relationship-schema)))


(defgeneric get-relationship (db source-type relationship target-type)
  (:documentation "Extract the attributes of interest for a given relationship.
                  Return a 'schema-rels instance if querying a hash-table.
                  Cardinality defaults to many:many."))

(defmethod get-relationship ((db hash-table)
                             (source-type string)
                             (relationship string)
                             (target-type string))
  (log-message
    :debug
    (format nil "Retrieving the relationship ~A from ~A to ~A."
            relationship source-type target-type))
  ;; First, try to fetch the definition of the source-type
  (let ((sourcetype-def (gethash source-type db)))
    ;; *If* we have the source-type, search through its relationships for this one.
    (when sourcetype-def
      (find-if (lambda (rel)
                 (and (equal relationship (name rel))
                      (member target-type (target-types rel)
                              :test #'equal
                              :key #'name)))
               (relationships sourcetype-def)))))


(defgeneric list-relationships (session schema-version)
  (:documentation "Return the names of all relationships in the specified schema-version, as a list of strings."))

(defmethod list-relationships ((session neo4cl:bolt-session)
                               (schema-version integer))
  (log-message :debug (format nil "Listing all relationships in schema version ~D" schema-version))
  (mapcar (lambda (element) (cdr (assoc "name" element :test #'equal)))
          (neo4cl:bolt-transaction-autocommit
            session
            "MATCH (:ScSchema {name: 'root'})-[:VERSION]->(:ScSchemaVersion {ScCreateddate: $version})-[:HAS]->(r:ScRelationship) RETURN r.name AS name"
            :parameters `(("version" . ,schema-version)))))


(defgeneric get-relationship-from-db (session schema-version relationship &optional schema)
  (:documentation "Extract the attributes of interest for a given relationship.
                  Return a `schema-rels type if the relationship is defined, and nil if it isn't.
                  Cardinality defaults to many:many.
                  Living dangerously here, this returns different results according to whether
                  `schema` was supplied:
                  - if it was, the source- and target-types are populated as lists of schema-rtypes
                  - otherwise, they're lists of strings, being the names of the resourcetypes"))

(defmethod get-relationship-from-db ((session neo4cl:bolt-session)
                                     (schema-version integer)
                                     (relationship string)
                                     &optional schema)
  (log-message :debug (format nil "Retrieving the relationship '~A' from schema version ~D."
                              relationship schema-version))
  (let* ((query-base (format nil "MATCH (:ScSchema {name: 'root'})-[:VERSION]->(:ScSchemaVersion { ScCreateddate: $version })-[:HAS]->(r:ScRelationship {name: $name})"))
         (rel-query (format nil "~A RETURN r.cardinality AS cardinality, r.reltype AS reltype, r.description AS description" query-base))
         (sources-query (format nil "~A-[:SOURCE]->(t:ScResourceType) RETURN t.name AS name" query-base))
         (targets-query (format nil "~A-[:TARGET]->(t:ScResourceType) RETURN t.name AS name" query-base)))
    (handler-case
      ;; First, try to fetch details of the relationship itself
      (let ((rel (car (neo4cl:bolt-transaction-autocommit
                        session
                        rel-query
                        :parameters `(("version" . ,schema-version)
                                      ("name" . ,relationship))))))
        (when rel
          ;; If that succeeded, get the lists of its source and target resourcetypes,
          ;; as lists of strings.
          (let ((source-types
                  (mapcar (lambda (element)
                            (cdr (assoc "name" element :test #'equal)))
                          (neo4cl:bolt-transaction-autocommit
                            session
                            sources-query
                            :parameters `(("version" . ,schema-version)
                                          ("name" . ,relationship)))))
                (target-types
                  (mapcar (lambda (element)
                            (cdr (assoc "name" element :test #'equal)))
                          (neo4cl:bolt-transaction-autocommit
                            session
                            targets-query
                            :parameters `(("version" . ,schema-version)
                                          ("name" . ,relationship))))))
            (log-message :debug "Fetched the source and target lists")
            (log-message :debug (format nil "Source types: ~{'~A'~^, ~}" source-types))
            (log-message :debug (format nil "Target types: ~{'~A'~^, ~}" target-types))
            ;; Convert to an object
            (make-instance 'schema-rels
                           :name relationship
                           ;; The source and target types are lists of schema-rtype instances,
                           ;; pulled from the existing schema.
                           :source-types
                           (if (hash-table-p schema)
                             (mapcar (lambda (rtype)
                                       (log-message :debug (format nil "Processing source-type ~A" rtype))
                                       (gethash rtype schema))
                                     source-types)
                             source-types)
                           :target-types
                           (if (hash-table-p schema)
                             (mapcar (lambda (rtype)
                                       (log-message :debug (format nil "Processing target-type ~A" rtype))
                                       (gethash rtype schema))
                                     target-types)
                             target-types)
                           :cardinality (cdr (assoc "cardinality" rel :test #'equal))
                           :reltype (cdr (assoc "reltype" rel :test #'equal))
                           :description (cdr (assoc "description" rel :test #'equal))))))
      (error (e)
             (if (typep e 'neo4cl:client-error)
               (log-message :fatal (format nil "Neo4j error ~A ~A - ~A"
                                           (neo4cl:category e)
                                           (neo4cl:title e)
                                           (neo4cl:message e)))
               (progn
                 (log-message :fatal (format nil "Unhandled error '~A'" e))))))))


(defgeneric describe-resource-type (schema-db resourcetype schema-version)
  (:documentation "Return the in-database description of a resource-type, as a schema-rtypes instance."))

(defmethod describe-resource-type ((schema-db neo4cl:bolt-session)
                                   (resourcetype string)
                                   (schema-version integer))
  (log-message :debug (format nil "Describing resource-type '~A'" resourcetype))
  ;; It's simpler to stuff this function in here
  (flet ((get-resourcetype-attributes ()
           (log-message :debug (format nil "Getting attributes for resourcetype '~A'" resourcetype))
           ;; Fetch the name and type for each attribute
           (mapcar (lambda (attr)
                     (make-schema-rtype-attrs
                       (cdr (assoc "a" attr :test #'equal))))
                   (neo4cl:bolt-transaction-autocommit
                     schema-db
                     (format
                       nil
                       "MATCH (:ScSchema {name: 'root'})-[:VERSION]->(v:ScSchemaVersion {ScCreateddate: $version})-[:HAS]->(:ScResourceType {name: $rtypename})-[:HAS]->(a:ScResourceTypeAttribute) RETURN a")
                     :parameters `(("version" . ,schema-version)
                                   ("rtypename" . ,resourcetype))))))
    (handler-case
      (let ((rtype
              (car
                (neo4cl:bolt-transaction-autocommit
                  schema-db
                  "MATCH (:ScSchema {name: 'root'})-[:VERSION]->(v:ScSchemaVersion { ScCreateddate: $version })-[:HAS]->(s:ScResourceType {name: $rtypename}) RETURN s.dependent AS dependent, s.description AS description"
                  :parameters `(("version" . ,schema-version)
                                ("rtypename" . ,resourcetype))))))
        (when rtype
          ;; If we got one, instantiate a schema-rtypes object for it, then fetch its attributes
          (let ((obj (make-instance 'schema-rtypes :name resourcetype
                                    :dependent (cdr (assoc "dependent" rtype :test #'equal))
                                    :description (cdr (assoc "description" rtype :test #'equal))
                                    :relationships ())))
            (set-attributes obj (get-resourcetype-attributes))
            ;; Explicitly return the object, not the return value for `set-attributes`
            obj)))
      (error (e)
        (cond
          ((typep e 'neo4cl:client-error)
           (log-message :fatal (format nil "Neo4j client error ~A ~A - ~A"
                                       (neo4cl:category e)
                                       (neo4cl:title e)
                                       (neo4cl:message e))))
          ((typep e 'neo4cl:bolt-error)
           (log-message :fatal (format nil "Neo4j Bolt error ~A - ~A"
                                       (neo4cl:category e)
                                       (neo4cl:message e))))
          (t
           (log-message :fatal (format nil "Unhandled error '~A'" e))))))))


(defgeneric get-resourcetype-names (db schema-version)
  (:documentation "Return the names of resourcetypes, as a list of strings."))

(defmethod get-resourcetype-names ((db neo4cl:bolt-session)
                                   (schema-version integer))
  (log-message :debug "Fetching resourcetype names.")
  (mapcar (lambda (rtype)
            (cdr (assoc "name" rtype :test #'equal)))
          (neo4cl:bolt-transaction-autocommit
            db
            "MATCH (:ScSchema {name: 'root'})-[:VERSION]->(:ScSchemaVersion {ScCreateddate: $version})-[:HAS]->(r:ScResourceType) RETURN r.name AS name"
            :parameters `(("version" . ,schema-version)))))
