;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(defun fetch-file-checksum (session filename)
  "Check for the presence of a file, and return its SHA3-256 checksum.
   Helper function, defined as a separate function to stop indentation getting out of hand.
   We can't use get-resources for this, because ScSHA3256sum is a system property,
   and thus not returned by that function."
  (declare (type neo4cl:bolt-session session)
           (type string filename))
  (let ((result
          (neo4cl:bolt-transaction-autocommit
            session
            "MATCH (f:Files { ScUID: $uid }) RETURN f.ScSHA3256sum AS ScSHA3256sum"
            :parameters `(("uid" . ,filename)))))
    (cdr (assoc "ScSHA3256sum" (car result) :test #'equal))))


(defun files-dispatcher-v1 ()
  "Files API handler, API version 1."
  (log-message :debug "Handling files request")
  (log-message :debug (format nil "Handling files '~A' request with content-type '~A'"
                              (tbnl:request-method*) (tbnl:content-type*)))
  ;; Simplify references to the relevant policy
  (let* ((access-policy (files-policy (access-policy tbnl:*acceptor*)))
         ;; Oh, so you have a sessionid?
         ;; Let's just check whether that's connected to a valid session.
         (auth-session (when (tbnl:cookie-in "sessionid")
                         (fetch-session (session-server tbnl:*acceptor*)
                                        (tbnl:cookie-in "sessionid")))))
    (cond
      ;; Access policies: dispatch "deny" as early as possible
      ((or (and (equal :GET (tbnl:request-method*))
                (equal :DENY (get-policy access-policy)))
           (and (equal :POST (tbnl:request-method*))
                (equal :DENY (post-policy access-policy)))
           (and (equal :DELETE (tbnl:request-method*))
                (equal :DENY (delete-policy access-policy))))
       (forbidden))
      ;; Access policies: deny unauthenticated requests where authentication is required
      ((and (not auth-session)
            (or (and (equal :GET (tbnl:request-method*))
                     (not (equal :ALLOW-ALL (get-policy access-policy))))
                (and (equal :POST (tbnl:request-method*))
                     (not (equal :ALLOW-ALL (post-policy access-policy))))
                (and (equal :DELETE (tbnl:request-method*))
                     (not (equal :ALLOW-ALL (delete-policy access-policy))))))
       (unauthorised))
      ;;
      ;; Access policies: is this an admin-only request?
      ((and auth-session
            (not (equal "ScAdmin" (username auth-session)))
            (or
              (and (equal :GET (tbnl:request-method*))
                   (equal :ALLOW-ADMIN (get-policy access-policy)))
              (and (equal :POST (tbnl:request-method*))
                   (equal :ALLOW-ADMIN (post-policy access-policy)))
              (and (equal :PUT (tbnl:request-method*))
                   (equal :ALLOW-ADMIN (put-policy access-policy)))
              (and (equal :DELETE (tbnl:request-method*))
                   (equal :ALLOW-ADMIN (delete-policy access-policy)))))
       (unauthorised))
      ;; Client fails to provide the mandatory parameters
      ((and (equal (tbnl:request-method*) :POST)
            (or (null (tbnl:post-parameter "name"))
                (equal "" (tbnl:post-parameter "name")))
            (or (null (tbnl:post-parameter "file"))
                (equal "" (tbnl:post-parameter "file"))))
       (return-client-error "The 'name' and 'file' parameters must be supplied"))
      ((and (equal (tbnl:request-method*) :POST)
            (or (null (tbnl:post-parameter "name"))
                (equal "" (tbnl:post-parameter "name"))))
       (return-client-error "The 'name' parameter must be supplied"))
      ((and (equal (tbnl:request-method*) :POST)
            (or (null (tbnl:post-parameter "file"))
                (equal "" (tbnl:post-parameter "file"))))
       (return-client-error "The 'file' parameter must be supplied"))
      ;; Client uploads a file
      ((equal (tbnl:request-method*) :POST)
       (log-message :debug "Passed sanity-tests. Processing uploaded file.")
       (log-message :debug (format nil "Requested filename: ~A"
                                   (or (tbnl:post-parameter "name")
                                       "<not specified>")))
       (log-message :debug (format nil "Temporary filepath: ~A"
                                   (first (tbnl:post-parameter "file"))))
       (log-message :debug (format nil "Reported content-type: ~A"
                                   (third (tbnl:post-parameter "file"))))
       (let* ((requested-filename (tbnl:post-parameter "name"))
              ;; File-details should be a three-element list: (path file-name content-type).
              ;; "[P]ath is a pathname denoting the place where the uploaded file was stored,
              ;; file-name ;; (a string) is the file name sent by the browser, and content-type
              ;; (also a string) is ;; the content type sent by the browser.
              ;; The file denoted by path will be deleted after the request has been handled
              ;; - you have to move or copy it somewhere else if you want to keep it."
              ;;
              ;; Get the filepath of the uploaded file
              (filepath-temp (first (tbnl:post-parameter "file")))
              ;; Derive the file's metadata
              (checksum (hash-file filepath-temp))
              (mimetype (get-file-mime-type (namestring filepath-temp)))
              (mimetype-type (first (cl-ppcre:split "/" mimetype)))
              (filesize (with-open-file (infile filepath-temp
                                                :element-type '(unsigned-byte 8))
                          (file-length infile)))
              (filepath-relative (digest-to-filepath checksum))
              (filepath-absolute (make-pathname :defaults
                                                (format nil "~A/~A"
                                                        (files-location tbnl:*acceptor*)
                                                        filepath-relative)))
              (session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
              (sanitised-uid (sanitise-uid requested-filename))
              (creator-uid (if auth-session (username auth-session) "ScAdmin"))
              (existing-checksum-probe (neo4cl:bolt-transaction-autocommit
                                    session
                                    "MATCH (f:Files { ScSHA3256sum: $checksum }) RETURN f.ScUID AS uid"
                                    :parameters `(("checksum" . ,checksum)))))
         ;; Perform some sanity checks before proceeding
         (cond
           ;; Do we already have a file with this checksum?
           (existing-checksum-probe
             (progn 
               (log-message :debug "File is already present")
               ;; Return client-error message indicating name collision
               (setf (tbnl:content-type*) "text/plain")
               (setf (tbnl:return-code*) tbnl:+http-ok+)
               (format nil "/Files/~A" (cdr (assoc "uid"
                                                   (car existing-checksum-probe)
                                                   :test #'equal)))))
           ;; We're good to go; rock on.
           (t
             (log-message :debug
                          (format nil "File ~A does not already exist; proceeding." sanitised-uid))
             ;; Now we need to store the file's metadata,
             (log-message
               :debug
               (format nil "Storing file metadata: name = '~A', checksum = '~A', mimetype '~A'"
                       requested-filename checksum mimetype))
             (handler-case
               (let ((uri (concatenate
                            'string
                            "/Files/"
                            (store-resource session
                                            (resourcetype-schema tbnl:*acceptor*)
                                            "Files"
                                            `(("ScUID" . ,sanitised-uid)
                                              ("title" . ,requested-filename))
                                            creator-uid
                                            :system-attributes `(("ScSHA3256sum" . ,checksum)
                                                                 ("ScFilepath" . ,filepath-relative)
                                                                 ("ScFilesize" . ,filesize))))))
                 ;; then if that succeeds move it to its new location.
                 ;; Check whether this file already exists by another name
                 (log-message :debug "Moving the file to its new home.")
                 (log-message :debug (format nil "New home: '~A'" filepath-absolute))
                 (if (probe-file filepath-absolute)
                   ;; If it does, don't bother overwriting it.
                   (log-message :warn
                                (format nil "File ~A already exists. Not overwriting it with a duplicate."
                                        filepath-absolute))
                   ;; If it doesn't, move it now.
                   (progn
                     (log-message :debug
                                  (format nil "Moving received file '~A' to storage location '~A'"
                                          filepath-temp filepath-absolute))
                     ;; Ensure the destination path actually exists
                     (log-message :debug (format nil "Ensuring existence of target directory '~A'"
                                                 (directory-namestring filepath-absolute)))
                     (ensure-directories-exist (directory-namestring filepath-absolute))
                     ;; Now move the file.
                     (log-message :debug
                                  (format nil "Actually moving received file '~A' to storage location '~A'"
                                          filepath-temp filepath-absolute))
                     (move-file filepath-temp (namestring filepath-absolute))))
                 ;; FIXME: If the location-move fails, we should probably remove the metadata
                 ;; and tell the client.
                 ;;
                 ;; Link the person to the file they uploaded
                 (create-relationship-by-path session
                                              (format nil "/People/~A/SC_UPLOADED" creator-uid)
                                              (format nil "/Files/~A" sanitised-uid)
                                              (resourcetype-schema tbnl:*acceptor*))
                 ;; Ensure the relevant MediaType exists, and link it to this file
                 (let ((mediatype-uid (sanitise-uid (cl-ppcre:regex-replace "/" mimetype "_"))))
                   (unless (resource-exists-p session (list "MediaTypes" mediatype-uid))
                     (progn
                       (store-resource session
                                       (resourcetype-schema tbnl:*acceptor*)
                                       "MediaTypes"
                                       `(("ScUID" . ,mediatype-uid)
                                         ("name" . ,mimetype))
                                       "ScSystem")
                       ;; Ensure the mimetype-type also exists
                       (unless (resource-exists-p session (list "MediatypeTypes" mimetype-type))
                         (store-resource session
                                         (resourcetype-schema tbnl:*acceptor*)
                                         "MediatypeTypes"
                                         `(("ScUID" . ,mimetype-type))
                                         "ScSystem"))
                       ;; Link the Mediatype to its type
                       (create-relationship-by-path session
                                                    (format nil "/MediaTypes/~A/HAS_MEDIATYPE_TYPE"
                                                            mediatype-uid)
                                                    (format nil "/MediatypeTypes/~A" mimetype-type)
                                                    (resourcetype-schema tbnl:*acceptor*))
                       (create-relationship-by-path session
                                                    (format nil "/MediatypeTypes/~A/MEDIATYPE_TYPE_OF"
                                                            mimetype-type)
                                                    (format nil "/MediaTypes/~A" mediatype-uid)
                                                    (resourcetype-schema tbnl:*acceptor*))))
                   (create-relationship-by-path session
                                                (format nil "/Files/~A/HAS_MEDIA_TYPE" sanitised-uid)
                                                (format nil "/MediaTypes/~A" mediatype-uid)
                                                (resourcetype-schema tbnl:*acceptor*))
                   (create-relationship-by-path session
                                                (format nil "/MediaTypes/~A/MEDIA_TYPE_OF" mediatype-uid)
                                                (format nil "/Files/~A" sanitised-uid)
                                                (resourcetype-schema tbnl:*acceptor*)))
                 ;; Now return success to the client
                 (setf (tbnl:content-type*) "text/plain")
                 (setf (tbnl:return-code*) tbnl:+http-created+)
                 (setf (tbnl:header-out "Location") uri)
                 uri)
               ;; Catch errors
               (integrity-error
                 (e)
                 (let ((error-message (format nil "Integrity error: ~A"
                                              (message e))))
                   (log-message :error error-message)
                   (setf (tbnl:content-type*) "text/plain")
                   (setf (tbnl:return-code*) tbnl:+http-internal-server-error+)
                   error-message))
               (neo4cl:client-error
                 (e)
                 (let ((error-message (format nil "~A.~A: ~A"
                                              (neo4cl:category e)
                                              (neo4cl:title e)
                                              (neo4cl:message e))))
                   (log-message :error error-message)
                   (setf (tbnl:content-type*) "text/plain")
                   (setf (tbnl:return-code*) tbnl:+http-internal-server-error+)
                   error-message))
               (file-error
                 (e)
                 (let ((error-message (format nil "File error: ~A" (file-error-pathname e))))
                   (log-message :error error-message)
                   (setf (tbnl:content-type*) "text/plain")
                   (setf (tbnl:return-code*) tbnl:+http-internal-server-error+)
                   error-message))
               (error
                 (e)
                 (let ((error-message (format nil "Error: ~A" e)))
                   (log-message :error error-message)
                   (setf (tbnl:content-type*) "text/plain")
                   (setf (tbnl:return-code*) tbnl:+http-internal-server-error+)
                   error-message)))))))
      ;; Client requests a file
      ((equal (tbnl:request-method*) :GET)
       (log-message :debug
                    (format nil "Dispatching GET request for URI ~A"
                            (tbnl:request-uri*)))
       (let* ((filename (first (get-uri-parts
                                 (get-sub-uri (tbnl:request-uri*)
                                              (uri-base-files tbnl:*acceptor*)))))
              (session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
              ;; Do it separately because we use it again later in this function.
              ;; Get the search result
              (result (get-resources session
                                     (resourcetype-schema tbnl:*acceptor*)
                                     (list "Files" filename)))
              (internal-details
                (when result
                  (car (neo4cl:bolt-transaction-autocommit
                         session
                         "MATCH (f:Files { ScUID: $uid }) RETURN f.ScSHA3256sum AS checksum, f.ScFilesize AS filesize, f.ScFilepath as filepath"
                         :parameters `(("uid" . ,filename))))))
              (mimetype
                (when result (gethash "name"
                                      (car (get-resources
                                             session
                                             (resourcetype-schema tbnl:*acceptor*)
                                             `("Files" ,filename "HAS_MEDIA_TYPE" "MediaTypes")
                                             :attributes '("name")))))))
         (neo4cl:disconnect session)
         (log-message :debug (format nil "Retrieved resource details ~A" result))
         ;; Return the file to the client if it's present
         (cond
           ((not (null result))
            ;; Be explicit that we're generating the absolute filepath here
            (let ((filepath-absolute (format nil "~A/~A"
                                             ;; Lead with the parent directory
                                              (files-location tbnl:*acceptor*)
                                              ;; Pull the relative filepath from the query results
                                              (cdr (assoc "filepath" internal-details :test #'equal)))))
              (log-message :debug (format nil "Returning the file from path '~A' with MediaType '~A'"
                                          filepath-absolute mimetype))
              (tbnl:handle-static-file
                filepath-absolute
                ;; Specify the mime-type
                mimetype)))
           ;; Fallthrough for not finding the file
           (t
             (log-message :debug "Null result returned")
             (setf (tbnl:content-type*) "text/plain")
             (setf (tbnl:return-code*) tbnl:+http-not-found+)
             "File not found"))))
      ;; Client requests deletion of a file
      ((equal (tbnl:request-method*) :DELETE)
       (log-message :debug "File deletion requested")
       (let ((filename (first (get-uri-parts
                                (get-sub-uri (tbnl:request-uri*)
                                             (uri-base-files tbnl:*acceptor*))))))
         (log-message :debug (format nil "Client requested deletion of file '~A'" filename))
         ;; Check whether the file is present.
         (let* ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
         ;; This needs a custom query, because ScSHA3256sum is a system property,
         ;; and thus not in the schema.
         (existing-checksum (fetch-file-checksum session filename)))
       (when existing-checksum
         ;; If it is, delete its metadata and then (conditionally) the file itself.
         (progn
           ;; Delete the metadata
           (delete-resource-by-path
             session
             (list "Files" filename)
             (resourcetype-schema tbnl:*acceptor*)
             :recursive t)
           (let ((filepath-absolute (make-pathname :defaults
                                                   (format nil "~A/~A"
                                                           (files-location tbnl:*acceptor*)
                                                           (digest-to-filepath existing-checksum)))))
             ;; Check whether there's a file to delete.
             (if (probe-file filepath-absolute)
               (progn
                 (log-message :debug
                              (format nil "Deleting the file at path '~A'" filepath-absolute))
                 (delete-file filepath-absolute))
               ;; No file; nothing to do.
               (log-message :warn (format nil "No file found at path '~A'" filepath-absolute))))))
       ;; Clean up the session
       (neo4cl:disconnect session))
      ;; Now return a suitable message to the client
      (setf (tbnl:content-type*) "text/plain")
      (setf (tbnl:return-code*) tbnl:+http-no-content+)
      ""))
  ;;
  ;; Methods we don't support.
  ;; Take the allow-list approach
  ((not (member (tbnl:request-method*) '(:POST :GET :DELETE)))
   (method-not-allowed))
  ;; Handle all other cases
  (t
    (log-message :warn (format nil "Bad request received with URI: ~A" (tbnl:request-uri*)))
    (return-client-error "This wasn't a valid request")))))
