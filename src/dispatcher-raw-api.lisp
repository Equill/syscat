;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(defun api-dispatcher-v1 ()
  "Hunchentoot dispatch function for the Syscat API, version 1."
  (handler-case
    (let* ((sub-uri (get-sub-uri (tbnl:request-uri*) (uri-base-api tbnl:*acceptor*)))
           (uri-parts (get-uri-parts sub-uri))
           (access-policy (raw-policy (access-policy tbnl:*acceptor*)))
           ;; Oh, so you have a sessionid?
           ;; Let's just check whether that's connected to a valid session.
           (auth-session (when (tbnl:cookie-in "sessionid")
                           (fetch-session (session-server tbnl:*acceptor*)
                                          (tbnl:cookie-in "sessionid")))))
      (log-message :debug (format nil "Handling API ~A request ~{/~A~} "
                                  (tbnl:request-method*) uri-parts))
      (cond
        ;; Access policies: dispatch "deny" as early as possible
        ((or (and (equal :GET (tbnl:request-method*))
                  (equal :DENY (get-policy access-policy)))
             (and (equal :POST (tbnl:request-method*))
                  (equal :DENY (post-policy access-policy)))
             (and (equal :PUT (tbnl:request-method*))
                  (equal :DENY (put-policy access-policy)))
             (and (equal :DELETE (tbnl:request-method*))
                  (equal :DENY (delete-policy access-policy))))
         (forbidden))
        ;;
        ;; Access policies: if the policy is other than :ALLOW-ALL for this method
        ;; and there's no valid session, deny now.
        ((and (not auth-session)
              (or (and (equal :GET (tbnl:request-method*))
                       (not (equal :ALLOW-ALL (get-policy access-policy))))
                  (and (equal :POST (tbnl:request-method*))
                       (not (equal :ALLOW-ALL (post-policy access-policy))))
                  (and (equal :PUT (tbnl:request-method*))
                       (not (equal :ALLOW-ALL (put-policy access-policy))))
                  (and (equal :DELETE (tbnl:request-method*))
                       (not (equal :ALLOW-ALL (delete-policy access-policy))))))
         (unauthorised))
        ;;
        ;; Access policies: is this an admin-only request?
        ((and auth-session
              (not (equal "ScAdmin" (username auth-session)))
              (or
                (and (equal :GET (tbnl:request-method*))
                     (equal :ALLOW-ADMIN (get-policy access-policy)))
                (and (equal :POST (tbnl:request-method*))
                     (equal :ALLOW-ADMIN (post-policy access-policy)))
                (and (equal :PUT (tbnl:request-method*))
                     (equal :ALLOW-ADMIN (put-policy access-policy)))
                (and (equal :DELETE (tbnl:request-method*))
                     (equal :ALLOW-ADMIN (delete-policy access-policy)))))
         (unauthorised))
        ;;
        ;; Intercept and reject attempts to interact with the "any" resource-type
        ((equal (third uri-parts) "any")
         (progn
           (setf (tbnl:content-type*) "text/plain")
           (setf (tbnl:return-code*) tbnl:+http-not-found+)
           (format nil "No resources found for ~A" uri-parts)))
        ;;
        ;; Mistaken attempt to create a relationship from a primary resource,
        ;; forgetting to include the relationship in the URL
        ((and (equal :POST (tbnl:request-method*))
              (equal 2 (length uri-parts)))
         (log-message :debug "Client attempted to create a relationship from a primary resource, without specifying the relationship.")
         (setf (tbnl:content-type*) "text/plain")
         (setf (tbnl:return-code*) tbnl:+http-bad-request+)
         "Request not valid. Did you forget to specify the relationship?")
        ;;
        ;; GET -> Retrieve something
        ;;
        ;; Version-list request
        ((and (equal (tbnl:request-method*) :GET)
              (= 2 (mod (length uri-parts) 3))
              (equal (tbnl:get-parameter "ScVersion") "list"))
         (log-message :debug (format nil "Dispatching version-list GET request for URI ~A"
                                     (tbnl:request-uri*)))
         ;; Sanity check: this request *must* be made with a canonical path in order to make sense
         (if (canonical-path-p uri-parts (resourcetype-schema tbnl:*acceptor*))
           ;; We're good; carry on.
           (let* ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
                  (result (list-resource-versions session
                                                  uri-parts
                                                  (resourcetype-schema tbnl:*acceptor*))))
             (neo4cl:disconnect session)
             (log-message :debug (format nil "Fetched content ~A" result))
             (setf (tbnl:content-type*) "application/json")
             (setf (tbnl:return-code*) tbnl:+http-ok+)
             (if result
               (cl-json:encode-json-to-string result)
               "[]"))
           ;; Failed the sanity check
           (return-client-error "This was not a canonical path")))
        ;; Client's trying to directly request a dependent resource
        ((and (equal (tbnl:request-method*) :GET)
              (= 2 (length uri-parts))
              (gethash (first uri-parts) (resourcetype-schema tbnl:*acceptor*)) ; ?nonexistent resourcetype
              (dependent (gethash (first uri-parts) (resourcetype-schema tbnl:*acceptor*))))
         (progn
           (log-message :debug "Refusing to fetch a dependent resource as though it were a primary one.")
           (setf (tbnl:content-type*) "text/plain")
           (setf (tbnl:return-code*) tbnl:+http-bad-request+)
           "Error: dependent resources must be fetched in context."))
        ;; Client requested either a path with a UID, or whatever's on the far end
        ;; of some relationship
        ;; ...or a dangling relationship, which may or may not go the way we expect
        ((and (equal (tbnl:request-method*) :GET)
              (member (mod (length uri-parts) 3)
                      '(2 0)))
         (log-message :debug (format nil "Dispatching GET request for URI ~A" (tbnl:request-uri*)))
         (let* ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
                (result (get-resources session
                                       (resourcetype-schema tbnl:*acceptor*)
                                       uri-parts
                                       :attributes (cl-ppcre:split
                                                     ","
                                                     (tbnl:get-parameter "SCattributes")))))
           (neo4cl:disconnect session)
           (log-message :debug (format nil "Fetched content ~A" result))
           (setf (tbnl:content-type*) "application/json")
           (setf (tbnl:return-code*) tbnl:+http-ok+)
           (if result
             (cl-json:encode-json-to-string result)
             "[]")))
        ;;
        ;; Class of resources was requested
        ((and (equal (tbnl:request-method*) :GET)
              (> (length uri-parts) 0))
         (log-message :debug (format nil "Dispatching GET request for URI ~A" (tbnl:request-uri*)))
         (let ((top-rtype-def (gethash (first uri-parts) (resourcetype-schema tbnl:*acceptor*))))
           ;; Prevent clients directly requesting dependent resources
           (if (and top-rtype-def
                    (dependent top-rtype-def)
                    (= (length uri-parts) 1))
             ;; They're trying to directly fetch resources of a dependent type,
             ;; as though it were a primary one.
             (progn
               (log-message :debug "Refusing to fetch a dependent resource as though it were a primary one.")
               (setf (tbnl:content-type*) "text/plain")
               (setf (tbnl:return-code*) tbnl:+http-bad-request+)
               "Error: dependent resources must be fetched in context.")
             ;; It passed that test; try to fetch the requested resources.
             (let* ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
                    (result (get-resources session
                                           (resourcetype-schema tbnl:*acceptor*)
                                           uri-parts
                                           :filters (process-filters
                                                      (remove-if
                                                        (lambda (param)
                                                          (equal (car param) "SCattributes"))
                                                        (tbnl:get-parameters*))
                                                      (resourcetype-schema tbnl:*acceptor*)
                                                      (car (last uri-parts)))
                                           :attributes (cl-ppcre:split
                                                         ","
                                                         (tbnl:get-parameter "SCattributes")))))
               (neo4cl:disconnect session)
               (log-message :debug (format nil "Fetched content ~A" result))
               (setf (tbnl:content-type*) "application/json")
               (setf (tbnl:return-code*) tbnl:+http-ok+)
               (if result
                 (cl-json:encode-json-to-string result)
                 "[]")))))
        ;; POST -> Store something
        ;;
        ;; Primary resource
        ((and
           (equal (tbnl:request-method*) :POST)
           (equal (length uri-parts) 1)
           (tbnl:post-parameter "ScUID"))
         (let ((resourcetype (first uri-parts))
               (uid (sanitise-uid (or (tbnl:post-parameter "ScUID") "")))
               ;; It's just simpler to establish this now, so we can use it in cond test-clauses.
               (session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*))))
           (log-message
             :debug
             (format nil "Attempting to dispatch a POST request for resource type ~A" resourcetype))
           ;; Perform a series of sanity-tests.
           ;; If all of them pass, try to store it.
           (cond
             ;; Did the client supply a usable UID?
             ((equal "" uid)
              (return-client-error
                "Error: 'uid' parameter must be specified, and must not be whitespace-only."
                "Client did not supply a usable UID"))
             ;; Is the client trying to store a dependent resource at the top level?
             ;; NB: this looks a little odd, but it's (somewhat) simpler this way.
             ((let ((top-rtype-def (gethash (first uri-parts) (resourcetype-schema tbnl:*acceptor*))))
                (and top-rtype-def
                     (dependent top-rtype-def)))
              (progn
                (log-message :debug "Refusing to store a dependent resource as though it were a primary one.")
                (setf (tbnl:content-type*) "text/plain")
                (setf (tbnl:return-code*) tbnl:+http-bad-request+)
                "Error: dependent resources must be stored in context."))
             ;; Do we already have one of these?
             ((resource-exists-p session (list resourcetype uid))
              ;; It's already there; return 304 "Not modified"
              (progn
                (neo4cl:disconnect session)
                (log-message :debug (format nil "Doomed attempt to re-create resource /~A/~A."
                                            resourcetype uid))
                (setf (tbnl:content-type*) "text/plain")
                (setf (tbnl:return-code*) tbnl:+http-ok+)
                (format nil "/~A/~A" resourcetype uid)))
             ;;
             ;; We don't already have one of these; store it
             (t
               (handler-case
                 (let ((uri (format
                              nil
                              "/~A/~A"
                              resourcetype
                              (store-resource session
                                              (resourcetype-schema tbnl:*acceptor*)
                                              resourcetype
                                              (tbnl:post-parameters*)
                                              (if auth-session
                                                (username auth-session)
                                                "ScAdmin")))))
                   ;; For the Audit part of AAA, record who stored it,
                   ;; separately from the database.
                   (log-message :info (format nil "User '~A' stored resource '~A'"
                                              (if auth-session
                                                (username auth-session)
                                                "ScAdmin")
                                              uri))
                   ;; Close the Bolt session
                   (neo4cl:disconnect session)
                   ;; Return 201 and the URI
                   (log-message :debug (format nil "Stored the new resource with URI '~A'" uri))
                   (setf (tbnl:content-type*) "text/plain")
                   (setf (tbnl:return-code*) tbnl:+http-created+)
                   (setf (tbnl:header-out "Location") uri)
                   ;; Return the URI to the newly-created resource
                   uri)
                 ;; Handle integrity errors
                 (integrity-error (e) (return-integrity-error (message e)))
                 ;; Handle general client errors
                 (client-error (e) (return-client-error (message e))))))))
        ;;
        ;; Store a relationship
        ((and (equal (tbnl:request-method*) :POST)
              (equal (mod (length uri-parts) 3) 0)
              (tbnl:post-parameter "target"))
         ;; Break down the destination URI into a list of strings.
         ;; Gracefully handle malformed input, where a user might have copy-pasted the wrong subset
         ;; of the target path, or some tooling is buggy.
         ;; Thus, we extract the largest subset of the tail of the path whose length is valid.
         (let* ((supplied-dest-uri-parts (get-uri-parts (tbnl:post-parameter "target")))
                (dest-uri-parts
                  (case (mod (length supplied-dest-uri-parts) 3)
                    (2 supplied-dest-uri-parts)
                    (1 (cddr supplied-dest-uri-parts))
                    (0 (cdr supplied-dest-uri-parts)))))
           (log-message :debug (format nil "Attempting to create a relationship from ~{/~A~} to ~{/~A~}"
                                       uri-parts dest-uri-parts))
           ;; Basic sanity checks
           (handler-case
             (cond
               ;; Reject any attempt to create a relationship whose name begins with `SC_`
               ((cl-ppcre:scan "^SC_" (car (last uri-parts)))
                (forbidden))
               ;; Ensure the source path is canonical
               ((not (canonical-path-p (butlast uri-parts) (resourcetype-schema tbnl:*acceptor*)))
                (return-client-error "Source path (minus the relationship) must be canonical"))
               ;; Ensure the target path is canonical
               ((not (canonical-path-p dest-uri-parts (resourcetype-schema tbnl:*acceptor*)))
                (return-client-error "Target path must be canonical"))
               ;; Duplicate check: does this relationship already exist?
               ;; Make a get-resource request for the resulting path.
               ((let* ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
                       (result (check-relationship-by-path session
                                                           ;; The relationship is at the end of the URI
                                                           ;; in these requests
                                                           (butlast uri-parts)
                                                           (car (last uri-parts))
                                                           dest-uri-parts)))
                  (neo4cl:disconnect session)
                  ;; Return whatever we got
                  result)
                (progn
                  (setf (tbnl:content-type*) "text/plain")
                  (setf (tbnl:return-code*) tbnl:+http-ok+)
                  ;; Return the path in question, consistent with what we do when a client tries
                  ;; to create a duplicate resource.
                  (format nil "~{/~A~}/~A/~A"
                          uri-parts    ; Path down to the relationship
                          (car (last dest-uri-parts 2))  ; Target resourcetype
                          (car (last dest-uri-parts)))))
               ;; Passed the sanity-checks; proceed.
               (t
                 (handler-case
                   (let ((new-uri (format nil "~{/~A~}/~A/~A"
                                          uri-parts    ; Path down to the relationship
                                          (car (last dest-uri-parts 2))  ; Target resourcetype
                                          (car (last dest-uri-parts))))  ; Target UID
                         (session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*))))
                     (create-relationship-by-path session
                                                  (format nil "~{/~A~}" uri-parts)
                                                  (format nil "~{/~A~}" dest-uri-parts)
                                                  (resourcetype-schema tbnl:*acceptor*))
                     (neo4cl:disconnect session)
                     ;; Report success to the client, plus the URI
                     (setf (tbnl:content-type*) "text/plain")
                     (setf (tbnl:return-code*) tbnl:+http-created+)
                     (setf (tbnl:header-out "Location") new-uri)
                     ;; Return the URI to the resource at the end of the newly-created relationship
                     new-uri) ; Target UID
                   ;; Attempted violation of db integrity
                   (integrity-error (e) (return-integrity-error (message e)))
                   ;; Generic client errors
                   (client-error (e) (return-client-error (message e)))
                   (neo4cl:client-error (e) (return-client-error (neo4cl:message e))))))
             (undefined-resourcetype (e) (return-client-error (message e)))
             (bad-argument (e) (return-client-error (message e))))))
        ;;
        ;; Create a dependent resource
        ((and
           (equal (tbnl:request-method*) :POST)
           (equal (mod (length uri-parts) 3) 1)
           (tbnl:post-parameter "ScUID"))
         (let ((uid (sanitise-uid (tbnl:post-parameter "ScUID"))))
           ;; Is the requested parent path a canonical one?
           (if
             (not
               (handler-case
                 (canonical-path-p (butlast uri-parts 2) (resourcetype-schema tbnl:*acceptor*))
                 (undefined-resourcetype (e) (return-client-error (message e)))))
             (return-client-error "The specified parent path is not valid."))
           ;; Do we already have one of these?
           (let ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*))))
             (if (resource-exists-p session (append uri-parts (list uid)))
               ;; It's already there; return 200/OK
               (progn
                 (neo4cl:disconnect session)
                 (log-message :debug (format nil "Doomed attempt to re-create resource /~A/~A."
                                             (car uri-parts) uid))
                 (setf (tbnl:content-type*) "text/plain")
                 (setf (tbnl:return-code*) tbnl:+http-ok+)
                 (format nil "~{/~A~}/~A" uri-parts uid))
               ;; We don't already have one of these; store it
               (handler-case
                 ;; Pre-generate this because we return it twice in the server response:
                 ;; once in the Location: header, and again in the body, just to make sure.
                 (let ((new-uri (format nil "~{/~A~}" (append uri-parts (list uid)))))
                   (store-dependent-resource
                     session
                     (resourcetype-schema tbnl:*acceptor*)
                     uri-parts
                     (tbnl:post-parameters*)
                     (if auth-session
                       (username auth-session)
                       "ScAdmin"))
                   (neo4cl:disconnect session)
                   (setf (tbnl:content-type*) "text/plain")
                   (setf (tbnl:return-code*) tbnl:+http-created+)
                   (setf (tbnl:header-out "Location") new-uri)
                   ;; Return the URI to the resource at the end of the newly-created resource
                   new-uri)
                 ;; Attempted violation of db integrity
                 (integrity-error (e) (return-integrity-error (message e)))
                 ;; Generic client errors
                 (client-error (e) (return-client-error (message e)))
                 (neo4cl:client-error (e) (return-client-error (neo4cl:message e)))
                 (undefined-resourcetype (e) (return-client-error (message e))))))))
        ;;
        ;; Re-home a dependent resource
        ((and
           (equal (tbnl:request-method*) :POST)
           (> (length uri-parts) 0)
           (equal (mod (length uri-parts) 3) 2)
           (tbnl:post-parameter "target"))
         (log-message :debug (format nil "Attempting to move dependent resource ~A to ~A"
                                     sub-uri (tbnl:post-parameter "target")))
         ;; Normally we'd stick a bunch of sanity-checks here,
         ;; but they're already in move-dependent-resource.
         (handler-case
           (let ((new-uri (format nil "~{/~A~}/~A/~A"
                                  (get-uri-parts (tbnl:post-parameter "target"))
                                  (car (last uri-parts 2))
                                  (car (last uri-parts))))
                 (session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*))))
             (move-dependent-resource
               session
               (resourcetype-schema tbnl:*acceptor*)
               sub-uri
               (tbnl:post-parameter "target"))
             (neo4cl:disconnect session)
             (setf (tbnl:content-type*) "text/plain")
             (setf (tbnl:return-code*) tbnl:+http-created+)
             (setf (tbnl:header-out "Location") new-uri)
             ;; Return the URI to the new path for this resource
             new-uri)
           ;; Attempted violation of db integrity
           (integrity-error (e) (return-integrity-error (message e)))
           ;; Generic client errors
           (client-error (e) (return-client-error (message e)))
           (neo4cl:client-error (e) (return-client-error (neo4cl:message e)))))
        ;; PUT -> update something
        ;;
        ;; Resource attributes
        ((and
           (equal (tbnl:request-method*) :PUT)
           (equal (mod (length uri-parts) 3) 2))
         (handler-case
           (let ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
                  ;; Extracting the requested-version takes a little work.
                  ;; Do it ahead of time, so we don't have to do it twice.
                  (requested-version (when (tbnl:parameter "ScVersion")
                                       (cond
                                         ((integerp (tbnl:parameter "ScVersion"))
                                          (tbnl:parameter "ScVersion"))
                                         ((stringp (tbnl:parameter "ScVersion"))
                                          (parse-integer (tbnl:parameter "ScVersion")
                                                         :junk-allowed t))
                                         (t nil)))))
             (cond
               ;; Attempt to update a dependent resource from the top level.
               ((not (canonical-path-p uri-parts (resourcetype-schema tbnl:*acceptor*)))
                (return-client-error "This path is not canonical."))
               ;; Set the current version
               (requested-version
                 ;; Does that version exist?
                 (if (resource-version-exists-p session
                                                uri-parts
                                                requested-version
                                                (resourcetype-schema tbnl:*acceptor*))
                   (progn
                     (set-resource-version session
                                           (resourcetype-schema tbnl:*acceptor*)
                                           uri-parts
                                           requested-version)
                     ;; Return 200/OK because we're trying to be idempotent here
                     (setf (tbnl:return-code*) tbnl:+http-ok+)
                     (setf (tbnl:content-type*) "text/plain")
                     "OK")
                   (return-client-error "Requested version does not exist")))
               ;; Client wants to  change the UID
               ((tbnl:parameter "ScUID")
                (let* ((new-uid (sanitise-uid (tbnl:parameter "ScUID")))
                       (new-path (append (butlast uri-parts) (list new-uid))))
                  (log-message
                    :debug
                    (format nil "User '~A' attempting to change ScUID of resource ~{/~A~} to '~A'"
                            (if auth-session
                              (username auth-session)
                              "ScAdmin")
                            uri-parts
                            (tbnl:parameter "ScUID")))
                  ;; Check the possible cases
                  (cond
                    ;; This can't be a valid path to a resource
                    ((not (= 2 (mod (length uri-parts) 3)))
                     (neo4cl:disconnect session)
                     (return-client-error "This is not a valid path to a resource."))
                    ;; Requested resource doesn't actually exist
                    ((not (resource-exists-p session uri-parts))
                     (neo4cl:disconnect session)
                     (return-client-error "Requested resource does not exist."))
                    ;; Requested new-uid is the same as the existing one
                    ((equal new-uid (car (last uri-parts)))
                     (neo4cl:disconnect session)
                     (setf (tbnl:content-type*) "text/plain")
                     (setf (tbnl:return-code*) tbnl:+http-ok+)
                     "OK")
                    ;; Requested new-uid is already present
                    ((resource-exists-p session new-path)
                     (neo4cl:disconnect session)
                     (return-integrity-error "Requested UID is already in use."))
                    ;; Happy path
                    (t
                      ;; Define the query to change the UID on this resource
                      (let ((cypher-path (format nil "MATCH ~A SET p.ScUID = $newuid"
                                                 (uri-node-helper uri-parts :marker "p"))))
                        (log-message :debug (format nil "Attempting to set ScUID with query '~A'" cypher-path))
                        ;; Execute that query
                        (neo4cl:bolt-transaction-autocommit
                          session
                          cypher-path
                          :parameters `(("newuid" . ,new-uid)))
                        ;; Now update  `ScCanonicalpath` for this resource and all its dependents
                        (recursively-canonicalise-resource
                          session
                          (append (butlast uri-parts) (list new-uid))
                          (resourcetype-schema tbnl:*acceptor*))
                        ;; Clean up and report back to the client
                        (neo4cl:disconnect session)
                        (setf (tbnl:content-type*) "text/plain")
                        (setf (tbnl:return-code*) tbnl:+http-ok+)
                        (format nil "~{/~A~}" new-path))))))
               ;; Default case: we're good, carry on.
               (t
                 ;; For the Audit part of AAA, record who updated it
                 (log-message
                   :info
                   (format nil "User '~A' attempting to create a new version of resource ~{/~A~}"
                           (if auth-session
                             (username auth-session)
                             "ScAdmin")
                           uri-parts))
                 (let ((new-version (add-resource-version
                                      session
                                      (resourcetype-schema tbnl:*acceptor*)
                                      uri-parts
                                      (remove-if (lambda (param)
                                                   (or (null (cdr param))
                                                       (equal (cdr param) "")
                                                       (equal "ScVersioncomment" (car param))))
                                                 (append (tbnl:post-parameters*)
                                                         (tbnl:get-parameters*)))
                                      :comment (tbnl:parameter "ScVersioncomment"))))
                   (neo4cl:disconnect session)
                   ;; Return 200/Updated in accordance with the Working Group spec:
                   ;; https://httpwg.org/specs/rfc7231.html#PUT
                   ;; Technically, it'd be more correct to return 201/Created if a new attribute is set,
                   ;; but there are two issues with this:
                   ;; - all attributes are null by default in this system
                   ;; - multiple attributes can be updated in a single PUT request, leading to a conflict
                   ;;   where one or more is updated, and one or more is not.
                   ;;   The simplest solution to this conflict is to use the one return-code shared by
                   ;;   these cases, and interpret the spec as "ensure that these attributes have these
                   ;;   values" rather than "conditionally update whichever of these attributes doesn't
                   ;;   already have the value specified in this request."
                   (setf (tbnl:content-type*) "text/plain")
                   (setf (tbnl:return-code*) tbnl:+http-ok+)
                   ;; Write to string, because Hunchentoot chokes on integers
                   (write-to-string new-version)))))
           ;; Attempted violation of db integrity
           (integrity-error (e) (return-integrity-error (message e)))
           ;; Generic client errors
           (client-error (e) (return-client-error (message e)))
           (neo4cl:client-error (e) (return-client-error (neo4cl:message e)))))
        ;;
        ;; DELETE -> Delete something
        ;;
        ;; Attempting to delete a file
        ((and (equal (tbnl:request-method*) :DELETE)
              (equal (mod (length uri-parts) 3) 2)
              (equal "Files" (first uri-parts)))
         (log-message :debug "Client is requesting deletion of a file")
         (setf (tbnl:content-type*) "text/plain")
         (setf (tbnl:return-code*) tbnl:+http-bad-request+)
         (format nil "Use the files API to delete files: /files/v1/~A" (second uri-parts)))
        ;; Attempting to delete a resource
        ((and (equal (tbnl:request-method*) :DELETE)
              (equal (mod (length uri-parts) 3) 2))
         (log-message :debug "Attempting to delete a resource on an arbitrary path")
         ;; Try to delete the resource.
         (handler-case
           (let* ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
                  ;; Normally this would be replaced with resource-exists-p,
                  ;; but we need the data for the "yoink" option.
                  (resource (get-resources session (resourcetype-schema tbnl:*acceptor*) uri-parts))
                  (canonicalised-path (canonical-path-p uri-parts (resourcetype-schema tbnl:*acceptor*))))
             (cond
               ;; Path is not valid
               ((null canonicalised-path)
                (return-client-error "The requested path was not canonical"))
               ;; Resource is not there
               ((null resource)
                (neo4cl:disconnect session)
                (setf (tbnl:content-type*) "text/plain")
                (setf (tbnl:return-code*) tbnl:+http-no-content+)
                "")
               ;; If the resource is there, delete it
               (t
                 (delete-resource-by-path
                   session
                   uri-parts
                   (resourcetype-schema tbnl:*acceptor*)
                   :recursive (and (tbnl:parameter "recursive")
                                   (not (equal "" (tbnl:parameter "recursive")))))
                 (neo4cl:disconnect session)
                 ;; For the Audit part of AAA, record who deleted it,
                 (log-message :info (format nil "User '~A' deleted resource '~A'"
                                            (if auth-session
                                              (username auth-session)
                                              "ScAdmin")
                                            sub-uri))
                 (setf (tbnl:content-type*) "text/plain")
                 ;; If the client requested "yoink" then return a representation of what we just deleted.
                 (if (tbnl:get-parameter "yoink")
                   (progn
                     (setf (tbnl:return-code*) tbnl:+http-ok+)
                     (cl-json:encode-json-to-string resource))
                   ;; If they didn't, just acknowledge
                   (progn
                     (setf (tbnl:return-code*) tbnl:+http-no-content+)
                     "")))))
           ;; Attempted violation of db integrity
           (integrity-error (e) (return-integrity-error (message e)))
           ;; Generic client errors
           (client-error (e) (return-client-error (message e)))
           ;; One of the resourcetypes in the requested path is not defined in this schema
           (undefined-resourcetype (e) (return-client-error (message e)))))
        ;;
        ;; Delete a relationship on an arbitrary path
        ((and (equal (tbnl:request-method*) :DELETE)
              (tbnl:parameter "target")
              (equal (mod (length uri-parts) 3) 0))
         ;; Reject any attempt to delete a relationship beginning with `SC_`
         (if (cl-ppcre:scan "^SC_" (car (last uri-parts)))
           (forbidden)
           (handler-case
             (let ((session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*))))
               (log-message :debug (format nil "Attempting to delete a relationship on an arbitrary path: ~A" sub-uri))
               (delete-relationship-by-path session
                                            (resourcetype-schema tbnl:*acceptor*)
                                            sub-uri
                                            (tbnl:parameter "target"))
               (neo4cl:disconnect session)
               (setf (tbnl:content-type*) "text/plain")
               (setf (tbnl:return-code*) tbnl:+http-no-content+)
               "")
             ;; Attempted violation of db integrity
             (integrity-error (e) (return-integrity-error (message e)))
             ;; Generic client errors
             (client-error (e) (return-client-error (message e))))))
        ;;
        ;; Methods we don't support.
        ;; Take the whitelist approach
        ((not (member (tbnl:request-method*) '(:POST :GET :PUT :DELETE)))
         (method-not-allowed))
        ;; Handle all other cases
        (t
          (log-message
            :warn
            (format nil "Bad request received with URI: ~A, reassembled as ~{/~A~}" (tbnl:request-uri*) uri-parts))
          (return-client-error "This wasn't a valid request"))))
    ;; Handle general errors
    ;;
    ;; Generic client errors
    (neo4cl:client-error (e) (return-client-error (neo4cl:message e)))
    (client-error (e) (return-client-error (message e)))
    ;; Transient error
    (neo4cl:transient-error (e) (return-transient-error e))
    ;; Database error
    (neo4cl:database-error (e) (return-database-error e))
    ;; Service errors, e.g. connection refused
    (neo4cl:service-error (e) (return-service-error (neo4cl:message e)))))
