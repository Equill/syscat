;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(asdf:defsystem #:syscat
                :license "AGPL-3.0"
                :author "Kat Sebastian <kat@electronic-quill.net>"
                :description "Generates a REST-ish API from a schema defined in Neo4j"
                :depends-on (#:cl-json
                             #:drakma
                             #:hunchentoot
                             #:html-template
                             #:ipaddress
                             #:ironclad
                             #:uuid
                             #:neo4cl
                             #:trivial-ldap
                             #:cl-redis)
                :components ((:file "package")
                             (:file "conditions"
                                    :depends-on ("package"))
                             (:file "logging"
                                    :depends-on ("package"))
                             (:file "config"
                                    :depends-on ("logging"))
                             ;; Authentication/authorisation
                             (:file "access-control"
                                    :depends-on ("package"))
                             (:file "auth-generics"
                                    :depends-on ("package"))
                             (:file "auth-neo4j"
                                    :depends-on ("auth-generics"))
                             (:file "auth-ldap"
                                    :depends-on ("auth-generics"))
                             (:file "ldap-admin"
                                    :depends-on ("auth-ldap"))
                             (:file "sessions-generics"
                                    :depends-on ("logging"))
                             (:file "sessions-neo4j"
                                    :depends-on ("sessions-generics"))
                             (:file "sessions-redis"
                                    :depends-on ("sessions-generics"))
                             ;; Schema management
                             (:file "schema-structures"
                                    :depends-on ("logging"))
                             (:file "core-schema"
                                    :depends-on ("schema-structures"))
                             (:file "schema-installation"
                                    :depends-on ("schema-structures"))
                             (:file "schema-retrieval"
                                    :depends-on ("schema-installation"))
                             (:file "utilities"
                                    :depends-on ("logging"))
                             (:file "schema-queries"
                                    :depends-on ("schema-structures" "utilities"))
                             ;; Resources and relationships
                             (:file "resource-canonicalisation"
                                    :depends-on ("schema-queries" "conditions"))
                             (:file "resources"
                                    :depends-on ("schema-queries"
                                                 "conditions"
                                                 "resource-canonicalisation"))
                             (:file "relationships"
                                    :depends-on ("schema-retrieval" "resources"))
                             (:file "ipam"
                                    :depends-on ("resources"))
                             ;; API
                             (:file "dispatcher-healthcheck"
                                    :depends-on ("package"))
                             (:file "dispatcher-responses"
                                    :depends-on ("logging"))
                             (:file "hunchentoot-classes"
                                    :depends-on ("package"
                                                 "auth-ldap"
                                                 "auth-neo4j"
                                                 "sessions-neo4j"
                                                 "sessions-redis"))
                             (:file "dispatcher-auth"
                                    :depends-on ("auth-generics"
                                                 "dispatcher-responses"
                                                 "hunchentoot-classes"
                                                 "resources"
                                                 "sessions-generics"))
                             (:file "dispatcher-files"
                                    :depends-on ("access-control"
                                                 "dispatcher-responses"
                                                 "hunchentoot-classes"
                                                 "resources"
                                                 "sessions-generics"))
                             (:file "dispatcher-ipam"
                                    :depends-on ("access-control"
                                                 "dispatcher-responses"
                                                 "hunchentoot-classes"
                                                 "ipam"))
                             (:file "dispatcher-raw-api"
                                    :depends-on ("access-control"
                                                 "dispatcher-responses"
                                                 "hunchentoot-classes"
                                                 "relationships"
                                                 "sessions-generics"))
                             (:file "dispatcher-schema"
                                    :depends-on ("access-control"
                                                 "dispatcher-responses"
                                                 "hunchentoot-classes"
                                                 "sessions-generics"
                                                 "utilities"))
                             (:file "hunchentoot"
                                    :depends-on ("hunchentoot-classes"
                                                 "dispatcher-auth"
                                                 "dispatcher-files"
                                                 "dispatcher-ipam"
                                                 "dispatcher-raw-api"
                                                 "dispatcher-schema"))))
