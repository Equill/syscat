;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Resource-related methods

(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(defclass resource-metadata ()
  ((resourcetype :initarg :resourcetype
                 :reader resourcetype
                 :type string
                 :initform (error 'bad-data-in-database "No resourcetype identified for this resource.")
                 :documentation "The type of this resource.")
   (uid :initarg :uid
        :reader uid
        :type string
        :initform (error 'bad-data-in-database "No UID retrieved for this resource.")
        :documentation "The Unique IDentifier for this resource.")
   (createddate :initarg :createddate
                :reader createddate
                :type integer
                :initform (error 'bad-data-in-database :message "No createddate retreved for this resource.")
                :documentation "The datestamp in Unix epoch time at which this resource was created.")
   (canonical-path :initarg :canonical-path
                   :reader canonical-path
                   :type (or string null)
                   :initform nil
                   :documentation "The path that uniquely, unambiguously and definitively identifies this resource."))
  (:documentation "The data retrieved from the node representing a resource in the database."))


(defgeneric store-resource (db
                             resourcetype-schema
                             resourcetype
                             attributes
                             creator-uid
                             &key system-attributes)
            (:documentation "Store a resource in the database.
                            Attributes argument is expected in the form of an alist.
                            Return the UID on success.
                            Return an error if
                            - the resource type is not present in the resourcetype-schema
                            - the client attempts to set attributes that aren't defined for this resourcetype."))

(defmethod store-resource ((db neo4cl:bolt-session)
                           (resourcetype-schema hash-table)
                           (resourcetype string)
                           ;; `attributes` is an alist, where the car is the name
                           ;; and the cdr is the value
                           (attributes list)
                           (creator-uid string)
                           &key system-attributes)
  (log-message :debug (format nil "Attempting to store a resource of type '~A' with attributes ~{~A~^, ~} and system attributes ~{/~A~}"
                              resourcetype attributes system-attributes))
  (flet ((generate-parent-params (params)
                                 (format nil "~{~A: ~:*$~A~^, ~}"
                                         (mapcar #'car params)))
         (generate-version-params (params)
                                  (format nil "~{~A: ~:*$~A~^, ~}"
                                          (mapcar #'car
                                                  (remove-if (lambda (el) (equal "ScUID" (car el)))
                                                             params)))))
    ;; Initial sanity-check
    (if (null (assoc "ScUID" attributes :test #'equal))
      (error 'client-error :message "ScUID must be specified")
      ;; More sanity-checks.
      ;; Also derive the requested UID once, to save lots of repeated code.
      (let ((sanitised-uid (sanitise-uid (cdr (assoc "ScUID" attributes :test #'equal)))))
        (cond
          ;; Do we even have this resourcetype?
          ((null (gethash resourcetype resourcetype-schema))
           (error 'client-error :message "No such resourcetype."))
          ;; Did the client provide a non-empty UID?
          ((equal "" sanitised-uid)
           (error 'client-error :message "The UID must be a non-empty string"))
          ;; If this is a dependent resource, bail out now
          ((dependent (gethash resourcetype resourcetype-schema))
           (let ((message "This is a dependent resource; it must be created as a sub-resource of an existing resource."))
             (log-message :warn message)
             (error 'integrity-error :message message)))
          ;; Do we already have one of these?
          ((resource-exists-p db (list resourcetype sanitised-uid))
           (let ((message (format nil "/~A/~A already exists; refusing to create a duplicate."
                                  resourcetype sanitised-uid)))
             (log-message :warn message)
             (error 'integrity-error :message message)))
          ;; OK so far: carry on
          (t
            (let* ((created-date (get-universal-time)) ; Guarantee they're both the same
                   (canonical-path (format nil "/~A/~A" resourcetype sanitised-uid))
                   (validated-attributes (append (validate-resource-before-creating
                                                   resourcetype-schema
                                                   resourcetype
                                                   attributes)
                                                 `(("ScVersion" . ,created-date)
                                                   ("ScVersioncomment" . "Initial version"))))
                   (collected-attributes (append system-attributes
                                                 validated-attributes
                                                 `(("creator_uid" . ,creator-uid)
                                                   ("ScCreateddate" . ,created-date)
                                                   ("ScCanonicalpath" . ,canonical-path))))
                   ;; This is quite a query, so let's break it down:
                   ;; - Get a reference to the user-account creating this thing (c).
                   ;; - Create the parent-node (n), containing UID and ScCreateddate, and link it to (c).
                   ;; - Create the version-node containing all the resourcetype-specific attributes (v),
                   ;;   link to it from (n) as a version, and link (c) as the creator of this version.
                   ;; - Create a second link from (n) to (v), denoting (v) as the _current_ version.
                   ;; - Create a link back from (v) to (n), so you can find what this is a version of.
                   (query (format nil "MATCH (c:People { ScUID: $creator_uid }) CREATE (n:~A { ~A })-[:SC_CREATOR]->(c), (n)-[:SC_HAS_VERSION]->(v:ScVersion { ~A })-[:SC_CREATOR]->(c), (n)-[:SC_CURRENT_VERSION]->(v), (v)-[:SC_VERSION_OF]->(n)"
                                  ;; I'd parameterise resourcetype if I could,
                                  ;; but they can't be used with node labels.
                                  resourcetype
                                  ;; Do it this way to prevent a Bolt error for duplicate map key
                                  (generate-parent-params (append system-attributes
                                                                  `(("ScCreateddate" . ,created-date)
                                                                    ("ScUID" . ,sanitised-uid)
                                                                    ("ScCanonicalpath" . ,canonical-path))))
                                  ;; This odd-looking chunk of code autogenerates a set of references
                                  ;; to the query-parameter map.
                                  ;; Its output is a comma-separated list of `attrname: $attrname` pairs,
                                  ;; which Neo4j will connect with the parameter-list that we include
                                  ;; in the transaction request.
                                  (generate-version-params validated-attributes))))
              ;; If we got this far, we have a valid resource type and valid attribute names;
              ;; make it happen
              (log-message :debug (format nil "Creating a '~A' resource with query '~A' and attributes ~A"
                                          resourcetype query collected-attributes))
              (handler-case
                ;; Use a progn so we can explicitly return the UID that was actually applied
                (progn
                  (neo4cl:bolt-transaction-autocommit db query :parameters collected-attributes)
                  ;; Return the new UID
                  sanitised-uid)
                ;; Catch selected errors as they come up
                (neo4cl:client-error
                  (e)
                  (if (and
                        ;; If it's specifically an integrity error, call this out
                        (equal (neo4cl:category e) "resourcetype-Schema")
                        (equal (neo4cl:title e) "ConstraintValidationFailed"))
                    (progn
                      (log-message :error (format nil "~A.~A: ~A"
                                                  (neo4cl:category e)
                                                  (neo4cl:title e)
                                                  (neo4cl:message e)))
                      (error 'integrity-error :message (neo4cl:message e)))
                    ;; Otherwise, just resignal it
                    (let ((text (format nil "Database error ~A.~A: ~A"
                                        (neo4cl:category e)
                                        (neo4cl:title e)
                                        (neo4cl:message e))))
                      (log-message :error text)
                      (error 'client-error :message text))))))))))))


(defgeneric store-dependent-resource (db resourcetype-schema uri-parts attributes creator-uid)
  (:documentation "Create a dependent resource, at the end of the path given by URI. Its parent resource must exist, and the relationship must be a valid dependent relationship."))

(defmethod store-dependent-resource ((db neo4cl:bolt-session)
                                     (resourcetype-schema hash-table)
                                     (uri-parts list)
                                     (attributes list)
                                     (creator-uid string))
  (log-message :debug (format nil "Attempting to create a dependent resource at path ~{/~A~}" uri-parts))
  (let* ((relationship (car (last (butlast uri-parts))))
         (parent-parts (butlast uri-parts 2))
         (parent-type (nth (- (length parent-parts) 2) parent-parts))
         (dest-type (car (last uri-parts)))
         (dest-uid (sanitise-uid (cdr (assoc "ScUID" attributes :test 'equal))))
         (relationship-attrs (or (get-relationship resourcetype-schema
                                                   parent-type
                                                   relationship
                                                   dest-type)
                                 (get-relationship resourcetype-schema
                                                   "any"
                                                   relationship
                                                   dest-type)))
         (validated-attributes (validate-resource-before-creating resourcetype-schema dest-type attributes))
         (resource-path (append parent-parts
                                (list relationship
                                      dest-type
                                      (cdr (assoc "ScUID" validated-attributes :test #'string=))))))
    (cond
      ;; Sanity check: required parameters
      ((not dest-uid)
       (let ((message "The 'ScUID' parameter must be supplied"))
         (log-message :debug message)
         (error 'client-error :message message)))
      ;; Sanity check: is the specified parent path a canonical one?
      ((null (canonical-path-p parent-parts resourcetype-schema))
       (let ((message "This is not a canonical path to the parent resource"))
         (log-message :debug message)
         (error 'client-error :message message)))
      ;; Sanity check: existence of parent resource
      ((null (resource-exists-p db parent-parts))
       (let ((message "Parent resource does not exist"))
         (log-message :debug message)
         (error 'client-error :message message)))
      ;; Sanity-check: is there a relationship between the parent and child resource types?
      ((null relationship-attrs)
       (let ((message (format nil "There is no relationship ~A from ~A to ~A"
                              relationship parent-type dest-type)))
         (log-message :error message)
         (error 'undefined-relationship :message message)))
      ;; Sanity check: dependency between parent and child resource types
      ((not (equal "dependent" (reltype relationship-attrs)))
       (let ((message
               (format nil "Target resource-type '~A' doesn't depend on the parent type '~A' for relationship '~A': ~A"
                       dest-type parent-type relationship (a-listify relationship-attrs))))
         (log-message :debug message)
         (error 'client-error :message message)))
      ;; Sanity check: is this a dependent resource type?
      ((not (dependent (gethash dest-type resourcetype-schema)))
       (let ((message "This is not a dependent resource type"))
         (log-message :debug message)
         (error 'client-error :message message)))
      ;; Sanity check: if it's a 1:1 relationship,
      ;; does the parent already have one with an instance of this type?
      ((and (equal (cardinality relationship-attrs) "1:1")
            ;; Look for this parent having this relationship with any other dependent resource
            (resource-exists-p db (butlast uri-parts)))
       #+(or)
       ;; FIXME: this produces a Bolt deserialisation error under some circumstances.
       (> (length
            (cdr (assoc "count"
                        (car (neo4cl:bolt-transaction-autocommit
                               db
                               (format nil "MATCH ~A<-[r:~A]-() RETURN count(r)"
                                       (uri-node-helper parent-parts
                                                        :path ""
                                                        :marker "n")
                                       relationship)))
                        :test #'equal))
            0))
       (error 'integrity-error :message
              (format nil"~{~A~^/~} already has a ~A ~A relationship with a resource of type ~A"
                      parent-parts
                      (cardinality relationship-attrs)
                      relationship
                      dest-type)))
      ;; Duplicate prevention: does the target resource already exist?
      ((resource-exists-p db resource-path)
       (error 'integrity-error :message (format nil "Resource ~{/~A~} already exists" resource-path)))
      ;; Passed the initial sanity-checks; try to create it.
      (t
        (log-message :debug "Sanity checks passed. Attempting to create the resource.")
        ;; Report on the attributes for debugging
        (log-message :debug (format nil "Validated attributes: ~A" validated-attributes))
        ;; Constraints are fine; create it
        (flet ((generate-version-params (params)
                 (format nil "~{~A: ~:*$~A~^, ~}"
                         (mapcar #'car
                                 (remove-if (lambda (el) (equal "ScUID" (car el)))
                                            params)))))
          (let* ((createddate (get-universal-time))
                 (params (append validated-attributes
                                 `(("ScVersion" . ,createddate))))
                 ;; This is quite a query, so let's break it down:
                 ;; - Get a reference to the user-account creating this thing (c).
                 ;; - Create the parent-node (n), containing UID and ScCreateddate, and link it to (c).
                 ;; - Create the version-node containing all the resourcetype-specific attributes (v),
                 ;;   link to it from (n) as a version, and link (c) as the creator of this version.
                 ;; - Create a second link from (n) to (v), denoting (v) as the _current_ version.
                 ;; - Create a link back from (v) to (n), so you can find what this is a version of.
                 (query-string (format nil "MATCH ~A, (c:People {ScUID: $creator_uid}) CREATE (n)-[:~A]->(s:~A { ScUID: $ScUID, ScCreateddate: $ScCreateddate, ScCanonicalpath: $ScCanonicalpath } )-[:SC_CREATOR]->(c), (s)-[:SC_HAS_VERSION]->(v:ScVersion { ~A })-[:SC_CREATOR]->(c), (s)-[:SC_CURRENT_VERSION]->(v), (v)-[:SC_VERSION_OF]->(s)"
                                       (uri-node-helper parent-parts
                                                        :path ""
                                                        :marker "n")
                                       ;; I'd parameterise these if I could,
                                       ;; but parameters can't be used with node labels.
                                       relationship
                                       dest-type
                                       ;; This odd-looking chunk of code autogenerates a set of references
                                       ;; to the query-parameter map.
                                       ;; Its output is a comma-separated list of `attrname: $attrname` pairs,
                                       ;; which Neo4j will connect with the parameter-list that we include
                                       ;; in the transaction request.
                                       (generate-version-params params))))
            (neo4cl:bolt-transaction-autocommit db
              query-string
              :parameters (append params
                                  `(("ScCreateddate" . ,createddate)
                                    ("creator_uid" . ,creator-uid)
                                    ("ScCanonicalpath" . ,(format nil "~{/~A~}" resource-path)))))
            ;; Explicitly return the new UID
            dest-uid))))))


(defgeneric move-dependent-resource (db resourcetype-schema uri newparent)
  (:documentation "Take an existing dependent resource, and give it a new parent, where both are identified by their URI paths."))

(defmethod move-dependent-resource ((db neo4cl:bolt-session)
                                    (resourcetype-schema hash-table)
                                    (uri string)
                                    (newparent string))
  (log-message :debug
               (format nil "Attempting to move dependent resource ~A to new parent ~A"
                       uri newparent))
  (let* ((uri-parts (get-uri-parts uri))
         (dest-parts (get-uri-parts newparent))
         ;; Reassemble the parent path by removing the dependent resource from the source path.
         ;; This is so we can delete this relationship after creating the new one.
         (current-parent-path (uri-node-helper (butlast uri-parts 3)
                                               :path ""
                                               :marker "b"))
         ;; It's a dependent resource, so the length of this path
         ;; will always be longer than 2:
         (current-relationship (car (last (butlast uri-parts 2))))
         (target-type (car (last (butlast uri-parts)))) ; Dependent resourcetype
         (target-uid (car (last uri-parts)))  ; Dependent UID
         (new-relationship-type (car (last dest-parts)))
         ;; The new parent may have a 2-element path,
         ;; in which case we don't need to extract the last 2 elements:
         (new-parent-type (car (if (> (length dest-parts) 2)
                                 (last (butlast dest-parts 2))
                                 dest-parts)))
         (new-relationship-details
           (get-relationship resourcetype-schema
                             new-parent-type
                             new-relationship-type
                             target-type))
         ;; Define this here because we use it at both the start and the end
         (new-path (append dest-parts (list target-type target-uid))))
    (cond
      ;; Sanity check: is the current path canonical?
      ((null (canonical-path-p uri-parts resourcetype-schema))
       (log-message :warn "Source path is non-canonical")
       (error 'client-error "Path to the existing resource is not canonical."))
      ;; Sanity check: is the new path canonical?
      ((null (canonical-path-p new-path resourcetype-schema))
       (log-message :warn "New-parent path is non-canonical")
       (error 'client-error "Path to the new parent resource is not canonical."))
      ;; Sanity check: does this path already exist?
      ((resource-exists-p db (append dest-parts (list target-type target-uid)))
       (log-message :warn "This path already exists")
       (error 'integrity-error :message "Path already exists; refusing to create a duplicate."))
      ;; Sanity check: does the target resource exist?
      ((null (resource-exists-p db uri-parts))
       (log-message :debug (format nil "Target resource ~A does not exist" uri))
       (error 'client-error :message "Target resource does not exist"))
      ;; Sanity-check: does the new parent exist?
      ((null (resource-exists-p db (butlast dest-parts)))
       (progn
         (log-message :debug (format nil "Parent resource ~{/~A~} does not exist"
                                     (butlast dest-parts)))
         (error 'client-error :message "Parent resource does not exist")))
      ;; Sanity check: is the new relationship a valid one?
      ((not new-relationship-details)
       (let ((message
               (format nil "New parent-type ~A does not have relationship ~A to target resource ~A"
                       new-parent-type new-relationship-type target-type)))
         (log-message :debug message)
         (error 'undefined-relationship :message message)))
      ;; Sanity check: is the new relationship dependent?
      ((not (equal "dependent" (reltype new-relationship-details)))
       (progn
         (log-message
           :debug
           (format
             nil
             "Target resource ~A does not depend on the new parent-type ~A for relationship ~A"
             target-type new-parent-type new-relationship-type))
         (error 'client-error
                :message
                (format
                  nil
                  "Target resource-type ~A doesn't depend on the parent type ~A"
                  target-type new-parent-type))))
      ;; Sanity-checks passed; let's do it
      (t
        (log-message :debug "Sanity-checks have passed. Attempting to move the resource.")
        (let* ((new-parent-path (uri-node-helper (butlast dest-parts) :marker "m"))
               (sourcepath
                 (uri-node-helper (append
                                    (butlast uri-parts 3)
                                    (list current-relationship
                                          target-type
                                          target-uid))
                                  :path ""
                                  :marker "t"))
               (destpath (format nil "~A-[:~A]->(:~A {ScUID: '~A'})"
                                 new-parent-path
                                 new-relationship-type
                                 target-type
                                 target-uid)))
          (log-message
            :debug
            (format nil "Moving target ~A to new parent ~A" sourcepath destpath))
          ;; Create the new relationship
          (neo4cl:bolt-transaction-autocommit
            db
            (format nil "MATCH ~A MATCH ~A CREATE (m)-[:~A]->(t)"
                    new-parent-path
                    sourcepath
                    new-relationship-type)))
        ;; Confirm that the new relationship is actually present.
        ;; If the MATCH clause matched nothing, it'll return OK.
        ;; We want to check this every time, and bail out if we detect that it failed.
        ;;
        ;; NOTE created from the `new-path` definition in the `let` block
        (unless (resource-exists-p db new-path)
          (error 'integrity-error :message (format nil "New path ~{/~A~} was not created." new-path)))
        ;; Update the canonical path stored in the resource
        (recursively-canonicalise-resource db new-path resourcetype-schema)
        ;; Delete the old relationship, using all but the last two elements of the source path
        (neo4cl:bolt-transaction-autocommit
          db
          (format nil "MATCH ~A-[r:~A]->(t:~A {ScUID: '~A'}) DELETE r"
                  current-parent-path
                  current-relationship
                  target-type
                  target-uid))))))

(defun process-filter (filter resourcetype-schema rtype)
  "Process a single filter from a GET parameter, expecting a dotted cons.
  Assumes uri-node-helper was called with its default marker, which is 'n',
  and that the version is given the variable 'v'.
  Returns NIL when the cdr of the filter is NIL.
  Helper-function for `process-filters`."
  (declare (type list filter)
           (type hash-table resourcetype-schema) ; Specific to checking enums
           (type string rtype))     ; Specific to checking enums
  (log-message :debug (format nil "Attempting to process filter ~A for resourcetype ~A" filter rtype))
  ;; Sanity-check: is this an empty filter expression?
  ;; These can legitimately be sent via badly-written search pages, for example.
  (cond
    ;; Empty filter
    ((and (listp filter)
          (or (null (cdr filter))
              (equal (cdr filter) "")))
     (log-message :debug (format nil "Empty filter ~A; ignoring" (car filter)))
     nil)
    ;; The filter's non-empty; carry on
    ((and (listp filter)
          (cdr filter)
          (stringp (cdr filter)))
     (log-message :debug (format nil "Filter ~A looks OK; attempting to process it" (car filter)))
     (let* ((name (car filter))
            ;; Note that the attribute is an object,
            ;; so we need to inspect its class for type-matching.
            (attribute (get-attribute (gethash rtype resourcetype-schema) name))
            ;; Does the value start with "!" to indicate negation?
            (negationp (string= "!" (cdr filter) :end2 1))
            ;; Get the value of the expression.
            ;; If it's negated, drop the leading `!`.
            (value (if negationp
                     (subseq (cdr filter) 1)
                     (cdr filter))))
       (log-message :debug (format nil "De-negated value: '~A'" value))
       ;; Log whether negation was detected
       (if negationp
         (log-message :debug (format nil "Negation detected. negationp = ~A" negationp))
         (log-message :debug "Negation not detected. Proceeding in the affirmative."))
       (if attribute
           (log-message :debug (format nil "Fetched attribute with name ~A." (name attribute)))
           (log-message :debug (format nil "Didn't find an attribute with name ~A." name)))
       (format
         nil
         "~A~A"
         ;; Are we negating it?
         (if negationp "NOT " "")
         ;; Infer the operator
         (cond
           ;; Outbound links
           ;; Simple format: relationship/path/to/target
           ((equal name "SCoutbound")
            (let* ((parts (get-uri-parts value))
                   (relationship (first parts)))
              (log-message :debug (format nil "Outbound link detected: ~A" value))
              ;; FIXME: needs sanity-checking that (not (equal (mod (length parts) 3) 1))
              ;; because that would be an outbound relationship to a relationship,
              ;; which makes no sense on the face of it.
              ;; Strictly, we could allow this by appending `->()` to it, for a test that it has
              ;; that kind of relationship to _anything_, which _does_ make sense.
              ;; In fact, `uri-node-helper` may well do  this automatically for us, so there may be
              ;; nothing to do beyond documenting the behaviour.
              (format nil "(n)-[:~A]->~A" relationship (uri-node-helper (cdr parts) :marker ""))))
           ;; Inbound links
           ;; Simple format: /path/to/source/relationship
           ((equal name "SCinbound")
            (log-message :debug (format nil "Inbound link detected: ~A" value))
            ;; Do basically the same thing as get-uri-parts, *except*
            ;; don't sanitise the wildcard #\* character.
            (uri-node-helper (loop for val in (cdr (cl-ppcre:split "/" value))
                                   for i from 1
                                   collecting (if (and (equal 2 (mod i 3))
                                                       (string= "*" val))
                                                val
                                                (sanitise-uid val)))
                             :marker "n"))
           ;; UID matching with regex
           ((and (equal name "ScUID")
                 (regex-p value))
            (format nil "n.ScUID =~~ '~A'" value))
            ;; UID exact match
            ((equal name "ScUID")
             (format nil "n.ScUID = '~A'" value))
           ;; Regex match for string-types
           ;; Full regex reference: https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
           ((and attribute
                 (member (type-of attribute) '(schema-rtype-attr-varchar schema-rtype-attr-text))
                 (regex-p value))
            (format nil "v.~A =~~ '~A'" name value))
           ;;
           ;; Simple existence check for a versioned attribute
           ((and attribute
                 (string= "exists" value))
            (format nil "exists(v.~A)" name))
           ;;
           ;; Enum attribute
           ((and
              attribute
              (eq (type-of attribute) 'schema-rtype-attr-varchar)
              (attrvalues (get-attribute (gethash rtype resourcetype-schema) name)))
            (format nil "v.~A IN [~{'~A'~^, ~}]" name (cl-ppcre:split "," value)))
           ;;
           ;; Default case: exact text match for a versioned attribute
           (t
             (format nil "v.~A = '~A'" name value))))))
    (t
      (log-message :warn "Invalid filter")
      nil)))

(defun process-filters (filters resourcetype-schema rtype)
  "Take GET parameters, and turn them into a string of Neo4j WHERE clauses.
  Expects an alist, as returned by `tbnl:get-parameters*`"
  (declare (type list filters)
           (type hash-table resourcetype-schema)
           (type string rtype))
  (log-message :debug (format nil "Attempting to process filters ~A" filters))
  (let ((result (remove-if #'null (mapcar (lambda (filter)
                                            (process-filter filter resourcetype-schema rtype))
                                          filters))))
    (log-message :debug (format nil "Result of filter processing: ~A" result))
    (if result
      (let ((response (format nil " WHERE ~{ ~A~^ AND~}" result)))
        (log-message :debug (format nil "Output from process-filters: ~A." response))
        response)
      "")))


(defgeneric resource-exists-p (session path)
            (:documentation
              "Indicate whether there is a resource in the database
              corresponding to the specified path.
              Return an integer representing the value of `ScCreateddate` if such a resource exists, and nil if not.
              `path` must be a list of strings, not a single string containing the URI.
              This is a very minimal counterpart to get-resources, which was previously
              dual-purposed to this effect."))

(defmethod resource-exists-p ((session neo4cl:bolt-session)
                              (path list))
  (log-message
    :debug
    (format nil "Checking for the existence of a resource at path '~{/~A~}'." path))
  (let ((query (format nil "MATCH ~A RETURN n.ScCreateddate AS createddate" (uri-node-helper path))))
    (log-message :debug (concatenate 'string "Using query-string: " query))
    (let ((result (neo4cl:bolt-transaction-autocommit session query)))
      ;; If there was a result, return its createddate.
      ;; Otherwise, default to nil.
      (when result (cdr (assoc "createddate" (car result) :test #'equal))))))


(defgeneric resource-version-exists-p (session path version resourcetype-schema)
  (:documentation "Return a boolean indicating whether this version of this resource
                   is present in the database."))

(defmethod resource-version-exists-p ((session neo4cl:bolt-session)
                                      (path list)
                                      (version integer)
                                      (resourcetype-schema hash-table))
  ;; Sanity check: is this path canonical?
  (unless (canonical-path-p path resourcetype-schema)
    (error 'client-error "The supplied path is not canonical."))
  (log-message :debug (format nil "Probing for presence of version ~D of resource ~{/~A~}"
                              version path))
  (let ((result (neo4cl:bolt-transaction-autocommit
                  session
                  (format nil "MATCH ~A-[:SC_HAS_VERSION]-(v:ScVersion { ScVersion: $version }) RETURN v.ScVersion AS exists"
                          (uri-node-helper path))
                  :parameters `(("version" . ,version)))))
    (cdr (assoc "exists" (car result) :test #'equal))))

(defgeneric list-current-version (session path resourcetype-schema)
  (:documentation "Return the current version of a resource, as an integer representing its creation date.
                  If there's no current version, return nil."))

(defmethod list-current-version ((session neo4cl:bolt-session)
                                 (path list)
                                 (resourcetype-schema hash-table))
  ;; Sanity-checks to make sure the request appears to be sane
  (cond
    ;; Could it plausibly lead to an individual resource?
    ((not (= 2 (mod (length path) 3)))
     (error 'client-error :message "Invalid path to a resource"))
    ;; Are all the elements strings?
    ((not (every #'stringp path))
     (error 'client-error :message "Path to a resource must be a list of strings, and no other type."))
    ;; Is this a valid canonical path?
    ((null (canonical-path-p path resourcetype-schema))
     (error 'client-error :message "This is not a canonical path."))
    ;; Passed those; I guess we're good to go.
    (t
     (let ((query-string (format nil "MATCH ~A-[:SC_CURRENT_VERSION]->(v:ScVersion) RETURN v.ScVersion AS version"
                                 (uri-node-helper path))))
       (log-message :debug (format nil "Using query ~A" query-string))
       (cdr (assoc "version"
                   (car (neo4cl:bolt-transaction-autocommit session query-string))
                   :test #'equal))))))


(defgeneric list-resource-versions (session path resourcetype-schema)
  (:documentation
    "Fetch a list of versions of the specified resource, plus the current version.
     Return an alist, whose keys are keywords, for ease of comparison:
     :VERSIONS -> list of dotted pairs:
     - integer = datestamp ID of the version
     - string = version-comment (if recorded), or nil if no comment recorded.
     - resourcetype-schema = hash-table, containing the resourcetype-oriented schema
     :CURRENT-VERSION -> integer, this being the datestamp ID of the current version."))

(defmethod list-resource-versions ((session neo4cl:bolt-session)
                                   (path list)
                                   (resourcetype-schema hash-table))
  (log-message :debug (format nil "Fetching a list of versions for ~{/~A~}" path))
  `((:VERSIONS . ,(mapcar (lambda (row) (cons (cdr (assoc "version" row :test #'equal))
                                              (or (cdr (assoc "versioncomment" row :test #'equal))
                                                  "")))
                          (neo4cl:bolt-transaction-autocommit
                            session
                            (format nil "MATCH ~A-[:SC_HAS_VERSION]->(v:ScVersion) RETURN v.ScVersion AS version, v.ScVersioncomment AS versioncomment ORDER BY v.ScVersion"
                                    (uri-node-helper path)))))
    (:CURRENT-VERSION . ,(list-current-version session path resourcetype-schema))))


(defgeneric get-resource-version (session
                                   resourcetype-schema
                                   path
                                   attributes
                                   &key version)
  (:documentation "Fetch attribute-values for a specified version of a resource.
                   Return value is a hash-table containing the requested attributes.
                   The `attributes` parameter is a list of strings, those being the names of attributes to return."))

(defmethod get-resource-version ((session neo4cl:bolt-session)
                                 (resourcetype-schema hash-table)
                                 (path list)
                                 (attributes list)
                                 &key version)
  (declare (type (or null integer) version))
  ;; Sanity check
  (unless (canonical-path-p path resourcetype-schema)
    (error 'client-error :message "This is not a canonical path to the resource."))
  ;; Debug logging
  (log-message :debug (format nil "Fetching attributes for resource ~{/~A~}" path))
  (if version
      (log-message :debug (format nil "Fetching version ~D." version))
      (log-message :debug "Fetching latest version"))
  (if attributes
      (log-message :debug (format nil "Client requested attributes: ~{~A~^, ~}." attributes))
      (log-message :debug "Client did not request any attributes."))
  ;; Proceed
  (log-message :debug (format nil "Fetching resource of type ~A" (car (last path 2))))
  ;; Predefine this query in a separate `let` clause,
  ;; so we can log it before using it in the next one.
  (let* ((rtype-attr-list (map 'list
                               #'name
                               (attributes (gethash (car (last path 2)) resourcetype-schema))))
         (attrs-to-fetch
           ;; Format this for use in the RETURN clause of a Cypher query,
           ;; returning each version-attribute with its unqualified name,
           ;; instead of as v.<name>.
           (format nil "~{v.~A AS ~:*~A~^, ~}"
                   (append
                     ;; ScVersion is a system-generated attribute, so we have to specify that here
                     '("ScVersion")
                     ;; Select whatever subset of the requested attributes matches
                     ;; the set of attribute-names available for this resourcetype
                     (remove-duplicates
                       (intersection attributes rtype-attr-list :test #'equal)
                       :test #'equal))))
         (query (if version
                    (format nil "MATCH ~A-[:SC_HAS_VERSION]->(v:ScVersion { ScVersion: $version }) RETURN ~A"
                            (uri-node-helper path) attrs-to-fetch)
                    (format nil "MATCH ~A-[:SC_CURRENT_VERSION]->(v:ScVersion) RETURN ~A"
                            (uri-node-helper path) attrs-to-fetch))))
    (log-message :debug (format nil "Attributes available for this resourcetype: ~{~A~^, ~}." rtype-attr-list))
    (log-message :debug (format nil "Using query-string: ~A" query))
    (let ((result (neo4cl:bolt-transaction-autocommit
                    session
                    query
                    :parameters `(("version" . ,version)))))
      (when result
        ;; Extract the hash-table of node properties, as returned by neo4cl
        (let ((properties (make-hash-table :test #'equal)))
          (mapcar (lambda (pair)
                    (log-message :debug (format nil "Setting property ~A to value ~A"
                                                (car pair) (cdr pair)))
                    (setf (gethash (car pair) properties)
                          (cdr pair)))
                  (car result))
          properties)))))


(defgeneric get-resources (session
                            resourcetype-schema
                            path
                            &key filters version attributes)
  (:documentation
    "Search for resources in a manner determined by the modulo-3 length of the URI.
     Return value is a list of hash-tables containing the properties of  each node  matching the path.
     The optional 'filters' parameter is for refining the search results;
     its expected value is a string, as returned by `process-filters`.
     'version' applies only to fetching a specific resource, and must be either an integer or nil,
     where nil signifies 'latest'."))

(defmethod get-resources ((session neo4cl:bolt-session)
                          (resourcetype-schema hash-table)
                          (path list)
                          &key filters version attributes)
  (declare (type (or null string) filters)
           (type (or null integer) version))
  (if attributes
      (log-message :debug (format nil "Client requested attributes: ~{~A~^, ~}." attributes))
      (log-message :debug "Client did not request any attributes."))
  (flet ((fetch-properties (resource version-date)
           "Get the attribute-values for the resource at this path."
           (log-message :debug
                        (format nil "Fetching version-data from the database for ~A"
                                (canonical-path resource)))
           (log-message :debug "Flush the log-buffer")
           ;; If no attribute-values were set when the resource was created, it won't have a version.
           ;; Create a stand-in.
           (let ((properties (or (get-resource-version session
                                                       resourcetype-schema
                                                       (get-uri-parts (canonical-path resource))
                                                       attributes
                                                       :version version)
                                 (make-hash-table :test #'equal))))
             ;; Move the version's createddate value to the versionid
             (setf (gethash "ScVersion" properties) version-date)
             ;; Add the resource's createddate
             (setf (gethash "ScCreateddate" properties)
                   (createddate resource))
             ;; Augment the node properties with the canonical path
             (setf (gethash "ScCanonicalpath" properties) (canonical-path resource))
             ;; Add the UID, because it's not already known to the client
             ;; in two of the three cases handled by get-resources
             (setf (gethash "ScUID" properties) (uid resource))
             ;; Set the resourcetype, because it's not always known either
             (setf (gethash "ScType" properties) (resourcetype resource))
             ;; Now return the updated hash-table
             (log-message :debug "Returning the hash-table")
             properties)))
    (cond
      ;; All resources of a given type
      ((equal (mod (length path) 3) 1)
        (log-message :debug (format nil "Fetching all resources of type ~{/~A~}" path))
        ;; This is a little duplicative, but it's how I got filters to work
        (let ((query (format nil "MATCH ~A-[:SC_CURRENT_VERSION]->(v:ScVersion) ~A RETURN n.ScUID AS uid, n.ScCreateddate AS createddate, n.ScCanonicalpath AS canonicalpath, v.ScVersion as version"
                             (uri-node-helper path
                                              :path ""
                                              :marker "n")
                             (or filters ""))))
          (log-message :debug (format nil "Querying database: ~A"
                                      (cl-ppcre:regex-replace "\~" query "~~")))
          (mapcar (lambda (node)
                    (fetch-properties (make-instance 'resource-metadata
                                                     :resourcetype (car (last path))
                                                     :uid (cdr (assoc "uid" node :test #'equal))
                                                     :createddate (cdr (assoc "createddate" node :test #'equal))
                                                     :canonical-path (cdr (assoc "canonicalpath" node :test #'equal)))
                                      (cdr (assoc "ScVersion" node :test #'equal))))
                  (neo4cl:bolt-transaction-autocommit session query))))
      ;; Resources matching a path to a UID
      ((equal (mod (length path) 3) 2)
       (log-message :debug
                    (format nil "Fetching the resources matching the path ~{/~A~} and version ~A"
                            path (or version "latest")))
       (let ((query
               (if version
                   ;; If a version was specified, include it in the path
                   (format nil "MATCH ~A-[:SC_HAS_VERSION]->(v:ScVersion { ScVersion: $version }) RETURN n.ScUID AS uid, n.ScCreateddate AS createddate, n.ScCanonicalpath AS ScCanonicalpath, v.ScVersion AS version"
                           (uri-node-helper path
                                            :path ""
                                            :marker "n"))
                   ;; If a version wasn't specified, fall back to the current version
                   (format nil "MATCH ~A-[:SC_CURRENT_VERSION]->(v:ScVersion) ~A RETURN n.ScUID AS uid, n.ScCreateddate AS createddate, n.ScCanonicalpath AS canonicalpath, v.ScVersion AS version"
                           (uri-node-helper path
                                            :path ""
                                            :marker "n")
                           (or filters "")))))
         (log-message :debug (format nil "Querying database: ~A"
                                     (cl-ppcre:regex-replace "\~" query "~~")))
         ;; Return a list of resources corresponding to that path.
         (mapcar (lambda (node)
                   (fetch-properties
                     (make-instance 'resource-metadata
                                    :resourcetype (car (last path 2))
                                    :uid (cdr (assoc "uid" node :test #'equal))
                                    :createddate (cdr (assoc "createddate" node :test #'equal))
                                    :canonical-path (cdr (assoc "canonicalpath" node :test #'equal)))
                     (cdr (assoc "version" node :test #'equal))))
                 ;; Fetch the resources to process
                 (neo4cl:bolt-transaction-autocommit
                   session
                   query
                   :parameters (when version `(("version". ,version)))))))
      ;; All resources with a particular relationship to this one
      (t
        (log-message :debug (format nil "Fetching all resources on the path '~{/~A~}'" path))
        ;; Pre-generate the query-string, so we can log it for diagnostics
        (let ((query (format nil "MATCH ~A-[:SC_CURRENT_VERSION]->(v:ScVersion) ~A RETURN labels(n) AS labels, n.ScUID AS uid, n.ScCreateddate AS createddate, n.ScCanonicalpath AS canonicalpath, v.ScVersion AS version ORDER BY n.ScCanonicalpath ASC"
                             (uri-node-helper path
                                              :path ""
                                              :marker "n")
                             (or filters ""))))
          (log-message :debug (concatenate 'string "Using query-string: "
                                           (cl-ppcre:regex-replace "\~" query "~~")))
          ;; Extract each node, and augment its property list
          (mapcar (lambda (node)
                    (fetch-properties
                      (make-instance 'resource-metadata
                                     :resourcetype (cdr (assoc "labels" node :test #'equal))
                                     :uid (cdr (assoc "uid" node :test #'equal))
                                     :createddate (cdr (assoc "createddate" node :test #'equal))
                                     :canonical-path (cdr (assoc "canonicalpath" node :test #'equal)))
                      (cdr (assoc "version" node :test #'equal))))
                  ;; Get the raw data
                  (neo4cl:bolt-transaction-autocommit session query)))))))


(defgeneric get-dependent-resources (db resourcetype-schema sourcepath)
  (:documentation "Return a list of the resources that depend critically on this one.
The returned list contains 3-element lists of relationship, type and UID."))

(defmethod get-dependent-resources ((db neo4cl:bolt-session)
                                    (resourcetype-schema hash-table)
                                    (sourcepath list))
  (unless (canonical-path-p sourcepath resourcetype-schema)
    (error 'client-error :message "This is not a canonical path to a resource."))
  (let ((rtypename (car (last sourcepath 2))))
    (log-message :debug (format nil "Searching for resources dependent on parent ~{/~A~}" sourcepath))
    (log-message :debug (format nil "Fetching dependent relationships from resourcetype ~A" rtypename))
    ;; Attempt to fetch the resourcetype
    (let ((rtypedef (gethash rtypename resourcetype-schema)))
      (if rtypedef
        ;; Get all dependent relationships outbound from this resourcetype
        (let ((dependent-rels
                ;; Relationships specifically dependent from this type
                (append
                  (map 'list
                       #'name
                       (remove-if-not
                         (lambda (foo)
                           (equal "dependent" (reltype foo)))
                         (relationships rtypedef)))
                  ;; Relationships dependent from "any" type
                  (map 'list
                       #'name
                       (remove-if-not
                         (lambda (foo)
                           (equal "dependent" (reltype foo)))
                         (relationships (gethash "any" resourcetype-schema)))))))
          (log-message :debug (format nil "Got list of dependent types: ~A" dependent-rels))
          ;; Get all nodes to which this node has outbound relationships of those types
          (when dependent-rels
            (let ((query-string (format nil "MATCH ~A-[r]->(b) WHERE type(r) IN [~{'~A'~^, ~}] RETURN type(r) AS type, labels(b) AS labels, b.ScUID AS uid ORDER BY b.ScUID ASC"
                                        (uri-node-helper sourcepath
                                                         :path ""
                                                         :marker "n")
                                        dependent-rels)))
              (log-message :debug (format nil "Generated query-string '~A'" query-string))
              ;; We should probably return the result
              (mapcar
                (lambda (row)
                  ;; List elements: relationship, target type, target UID
                  (list (cdr (assoc "type" row :test #'equal))
                        (cdr (assoc "labels" row :test #'equal))
                        (cdr (assoc "uid" row :test #'equal))))
                (neo4cl:bolt-transaction-autocommit db query-string)))))
        (error 'client-error
               :message (format nil "No such resourcetype ~A in this resourcetype-schema"
                                rtypename))))))


(defgeneric add-resource-version (session resourcetype-schema path attributes &key comment)
  (:documentation "Create a new version of a resource's attributes, applying the supplied updates.
                   Return an integer identifying the version that's current after this operation.
                   Only creates a new version if something was actually changed,
                   so it's possible that it will return the existing current-version identifier."))

(defmethod add-resource-version ((session neo4cl:bolt-session)
                                 (resourcetype-schema hash-table)
                                 (path list)
                                 (attributes list)
                                 &key comment)
  ;; Strategy:
  ;; - Fetch attributes for current version.
  ;; - Update attributes with those supplied.
  ;; - Create a new version, setting it as the current version.
  ;; - Delete the SC_CURRENT_VERSION relationship with the old current version.
  (log-message :debug (format nil "Attempting to create new version for resource ~{/~A~}" path))
  (log-message :debug (format nil "Supplied attributes: ~A" attributes))
  (when (null (canonical-path-p path resourcetype-schema))
    (error 'client-error :message "This is not a valid canonical path to a resource."))
  (let* ((current-version (list-current-version session path resourcetype-schema))
         ;; get-resource-version returns a hash-table.
         ;; In the event that we've struck a resource with no versions
         ;; (this can happen during system setup) just create a dummy hash-table.
         (current-attrs
           (if current-version
             ;; If there _is_ a current version, fetch the attribute values stored in it.
             (get-resource-version
               session
               resourcetype-schema
               path
               ;; Extract the list of attribute-names available for this resourcetype,
               ;; as a comma-separated list of strings.
               (map 'list #'name (attributes (gethash (car (butlast path)) resourcetype-schema)))
               :version current-version)
             ;; If there's no current version, just return a blank hash-table
             (make-hash-table :test #'equal)))
         ;; Distil and assemble the list of attributes to set for this new version
         (supplied-attrs (remove-if (lambda (f) (equal (car f) "ScUID"))
                                    (validate-resource-before-creating
                                      resourcetype-schema
                                      (car (last (butlast path)))
                                      attributes)))
         ;; Use a new hash-table, because this makes things simpler overall
         (new-attrs (make-hash-table :test #'equal)))
    (when supplied-attrs
      (log-message
        :debug
        (format nil "Checking supplied attributes ~{~A~^, ~} against the current version"
                supplied-attrs))
      (log-message :debug (format nil "Type of new-attrs: ~A. Type of current-attrs: ~A."
                                  (type-of new-attrs) (type-of current-attrs)))
      ;; Populate the new hash-table with the current attribute-values
      (maphash (lambda (key value)
                 (setf (gethash key new-attrs) value))
               current-attrs)
      ;; Update the new table with the supplied attributes
      (log-message :debug "Update the new table with the suppied attributes")
      (mapcar (lambda (attr)
                (setf (gethash (car attr) new-attrs) (cdr attr)))
              supplied-attrs)
      ;; Are we actually changing anything?
      (if (equalp new-attrs current-attrs)
        ;; No change.
        ;; Log that fact, and return the existing version identifier.
        (progn
          (log-message :debug "No change to make, so not creating a new version")
          current-version)
        ;; At least one thing changed; we're creating a new version.
        ;; Stash the version-id here, so we can reliably return this value to the client.
        ;; To avoid duplicate version IDs, check whether the timestamp (in seconds) matches the
        ;; latest version. If it does, sleep just long enough to guarantee that we'll be in the
        ;; next calendar second when we grab the timestamp.
        ;; Note that this has at least one race-condition involving parallel requests from
        ;; separate clients.
        (let ((version-id (progn (when (and current-version
                                            (= current-version (get-universal-time)))
                                   (sleep 1.1))
                                 (get-universal-time)))
              (attr-alist (list))) ; Create an alist for the Bolt transaction
          ;; Update the version identifier.
          ;; We're doing this here, to avoid b0rking the did-it-change comparison,
          ;; but before generating the alist.
          ;; We *could* do it after creating the alist, but this is a nice and simple way
          ;; to avoid duplicate entries for either of these things.
          (setf (gethash "ScVersion" new-attrs) version-id)
          (when comment (setf (gethash "ScVersioncomment" new-attrs) comment))
          ;; Populate the alist with the contents of the updated new-attrs hash-table
          (maphash (lambda (key value)
                     (push (cons key value) attr-alist))
                   new-attrs)
          ;; Now do the actual update
          (let* ((query-path (uri-node-helper path :path "" :marker "n"))
                 (delete-current-query (format nil "MATCH ~A-[r:SC_CURRENT_VERSION]->() DELETE r"
                                               query-path))
                 (add-version-query
                   ;; We do two things here:
                   ;; - create the new version
                   ;; - set it as the new current version.
                   ;; Because of the latter, we need to delete the previous current version first.
                   (format nil "MATCH ~A CREATE (n)-[:SC_HAS_VERSION]->(v:ScVersion { ~A }), (n)-[:SC_CURRENT_VERSION]->(v)"
                           query-path
                           (format nil "~{~A: $~A~^, ~}"
                                   (let ((acc (list)))
                                     (mapcar (lambda (param)
                                               (push (car param) acc) (push (car param) acc))
                                             attr-alist)
                                     acc)))))
            ;; Delete the old current-version link
            (log-message :debug (format nil "Deleting previous current-version with query ~A"
                                        delete-current-query))
            (neo4cl:bolt-transaction-autocommit session delete-current-query)
            ;; Save the updated version
            (log-message :debug (format nil "Applying statement ~A with arguments ~A"
                                        add-version-query attr-alist))
            (neo4cl:bolt-transaction-autocommit session
                                                add-version-query
                                                :parameters attr-alist))
          ;; Return the version-identifier of the newly-created version
          version-id)))))


(defgeneric set-resource-version (session resourcetype-schema path version)
  (:documentation "Set the current version of the specified resource to the specified one."))


(defmethod set-resource-version ((session neo4cl:bolt-session)
                                 (resourcetype-schema hash-table)
                                 (path list)
                                 (version integer))
  (log-message :debug (format nil
                              "Attempting to set current-version of resource ~{/~A~} to ~D"
                              path version))
  ;; Predefine this, because we're going to use it a few times
  (let ((query-base (format nil "MATCH ~A" (uri-node-helper path))))
    ;; Let's do some sanity checks
    (cond
      ;; Is this a plausibly valid path?
      ((not (and (= 2 (mod (length path) 3))
                 (every #'stringp path)))
       (error 'client-error :message "This was not a valid path."))
      ;; Is that a valid resourcetype?
      ((not (gethash (car (last path 2)) resourcetype-schema))
       (error 'client-error :message "This is not a valid resourcetype."))
      ;; Is this a valid canonical path?
      ((null (canonical-path-p path resourcetype-schema))
       (error 'client-error :message "This is not a canonical path to the resource."))
      ;; Does that version even exist?
      ((not (cdr (assoc "existsp"
                        (car
                          (neo4cl:bolt-transaction-autocommit
                            session
                            (format nil "~A RETURN exists((n)-[:SC_HAS_VERSION]->(:ScVersion { ScVersion: $version})) AS existsp" query-base)
                            :parameters `(("version" . ,version))))
                        :test #'equal)))
       (error 'client-error :message "Target version does not exist"))
      ;; Looks good; let's go ahead
      (t
       ;; First, delete any existing current versions
       (log-message :debug "Deleting previous SC_CURRENT_VERSION relationship")
       (neo4cl:bolt-transaction-autocommit
         session
         (format nil "~A-[r:SC_CURRENT_VERSION]->() DELETE r" query-base))
       ;; Next, create this new one
       (log-message :debug "Setting new version")
       (neo4cl:bolt-transaction-autocommit
         session
         (format nil "~A-[r:SC_HAS_VERSION]->(v:ScVersion { ScVersion: $version}) CREATE (n)-[:SC_CURRENT_VERSION]->(v) RETURN True"
                 query-base)
         :parameters `(("version" . ,version)))
       ;; Explicitly return something positive
       t))))


(defgeneric delete-resource-by-path (session targetpath resourcetype-schema &key recursive version)
            (:documentation
              "Delete a relationship or resource according to the URI supplied.
              :recursive confirms that you intend to delete all resources depending on the one identified in the path."))

(defmethod delete-resource-by-path ((session neo4cl:bolt-session)
                                    (targetpath list)
                                    (resourcetype-schema hash-table)
                                    &key recursive version)
  (log-message :debug (format nil "Attempting to delete resource ~{/~A~}" targetpath))
  (log-message :debug (format nil "The recursive flag was~A set" (if recursive "" " not")))
  (if version
      (log-message :debug (format nil "Version ~D was specified" version))
      (log-message :debug "No version was specified; attempting to delete the whole thing."))
  ;; Is this even a valid canonical path to a resource?
  (if (canonical-path-p targetpath resourcetype-schema)
      ;; Was a version specified?
      (if version
          ;; Just delete that one version... if it exists
          ;; FIXME: test for existence of the version
          (let ((versions (list-resource-versions session targetpath resourcetype-schema))
                (query (format nil "MATCH ~A-[:SC_HAS_VERSION]->(v:ScVersion {ScVersion: $version}) DETACH DELETE v"
                               (uri-node-helper targetpath)))
                (set-current-query (format nil "MATCH ~A-[:SC_HAS_VERSION]->(v:ScVersion {ScVersion: $version}) CREATE (n)-[:SC_CURRENT_VERSION]->(v)"
                                           (uri-node-helper targetpath))))
            ;; If the requested version is the current version, first set a new current version
            (when (equal version (cdr (assoc :CURRENT-VERSION versions)))
              (let ((new-current-version
                      (apply #'max
                             (remove-if (lambda (candidate) (= candidate version))
                                        (mapcar #'car (cdr (assoc :versions versions)))))))
                (log-message :debug "Requested version is the current version. Updating current version first.")
                (neo4cl:bolt-transaction-autocommit session
                                                    set-current-query
                                                    :parameters `(("version" . ,new-current-version)))))
            (log-message
              :debug
              (format nil "Deleting version ~D of resource ~{/~A~} with query '~A'"
                      version targetpath query))
            (neo4cl:bolt-transaction-autocommit session query :parameters `(("version" . ,version))))
          ;; No version specified; we're nuking the whole thing
          ;; Do any other resources depend critically on this one?
          (let ((dependents (get-dependent-resources session resourcetype-schema targetpath)))
            (if dependents
                ;; Yes: it has dependents.
                ;; Was the recursive argument supplied?
                (if recursive
                    ;; Yes. Delete the dependents, passing the value of the recursive argument
                    (progn
                      (log-message :debug "Dependent resources are present, and recursive deletion was requested.")
                      (mapcar
                        (lambda (d)
                          (let ((newpath (append targetpath d)))
                            (log-message
                              :debug
                              (format nil "Recursing through delete-resource-by-path with new path ~A"
                                      newpath))
                            (delete-resource-by-path session newpath resourcetype-schema :recursive t)))
                        dependents)
                      ;; Having deleted the dependents, delete the resource itself
                      (let ((querystring (format nil "MATCH ~A OPTIONAL MATCH (n)-[:SC_HAS_VERSION]->(v) DETACH DELETE n, v"
                                                 (uri-node-helper targetpath))))
                        (log-message :debug (format nil "Deleting target resource '~A' with query '~A'"
                                                    targetpath querystring))
                        (neo4cl:bolt-transaction-autocommit session querystring)))
                    ;; Dependents, but no recursive argument. Bail out.
                    (error 'integrity-error
                           :message
                           "Other resources depend critically on this one, and recursive was not specified."))
                ;; No dependents: remove it.
                (let ((query (format nil "MATCH ~A OPTIONAL MATCH (n)-[:SC_HAS_VERSION]->(v) DETACH DELETE n, v"
                                     (uri-node-helper targetpath))))
                  (log-message
                    :debug
                    (format nil "No dependents. Deleting resource ~A with query '~A'" targetpath query))
                  (neo4cl:bolt-transaction-autocommit session query)))))
      (error 'client-error :message "This is not a valid canonical path to a resource.")))


(defgeneric delete-resource-attributes (db path attributes resourcetype-schema)
  (:documentation "Delete attributes from a resource."))

;;; FIXME: validate the attrs, at least to ensure they're escaped
(defmethod delete-resource-attributes ((db neo4cl:bolt-session)
                                       (path list)
                                       (attributes list)
                                       (resourcetype-schema hash-table))
  (unless (canonical-path-p path resourcetype-schema)
    (error 'client-error :message "This is not a canonical path to a resource."))
  (log-message
    :debug
    (format nil "Attempting to delete attributes '~{~A~^, ~}' from the resource at path '~{~A~^/~}'"
            attributes path))
  (neo4cl:bolt-transaction-autocommit
    db
    (format nil "MATCH ~A REMOVE ~{n.~A~^, ~};"
            (uri-node-helper path :path "" :marker "n")
            attributes)))
