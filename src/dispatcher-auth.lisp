;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


(defun auth-dispatcher-v1 ()
  "Authentication and sessions API handler, API version 1."
  (log-message :debug "Handling auth request")
  (let* ((sub-uri (get-sub-uri (tbnl:request-uri*) (uri-base-api tbnl:*acceptor*)))
         (uri-parts (cddr (get-uri-parts sub-uri)))
         (auth-session (when (tbnl:cookie-in "sessionid")
                         (fetch-session (session-server tbnl:*acceptor*)
                                        (tbnl:cookie-in "sessionid")))))
    (cond
      ;; Are we even doing authentication?
      ((null (auth-server tbnl:*acceptor*))
       (setf (tbnl:return-code*) tbnl:+http-internal-server-error+)
       (setf (tbnl:content-type*) "text/plain")
       "Authentication is not enabled")
      ;; Login - create session
      ((and
         (equal (first uri-parts) "sessions")
         (equal (tbnl:request-method*) :POST)
         (tbnl:post-parameter "username")
         (tbnl:post-parameter "password"))
       (log-message :debug "Client requested login")
       ;; Note that we're sanitising the username in exactly the same way as for UIDs:
       ;; 1. By design, usernames _are_ UIDs, so it only makes sense.
       ;; 2. This mitigates a vector of attack on the auth server.
       (let ((username (sanitise-uid (tbnl:post-parameter "username"))))
         (log-message :debug (format nil "Attempting to authenticate user '~A'." username))
         ;; Check whether we even _have_ a People resource with this UID.
         (let ((user-exists
                 (let* ((neosession (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*)))
                        (result (resource-exists-p neosession (list "People" username))))
                   (neo4cl:disconnect neosession)
                   result)))
           ;; Diagnostic logging
           (if user-exists
             (log-message :debug (format nil "Found People resource with UID '~A'" username))
             (log-message :debug (format nil "Failed to find People resource with UID '~A'" username)))
           ;; Validate the credentials.
           ;; Do this even if we don't have a corresponding resource, to mitigate the effectiveness
           ;; of a timing-based user-enumeration attack.
           (if (and
                 (not (equal "ScSystem" username))
                 (check-user-creds (auth-server tbnl:*acceptor*)
                                   username
                                   (or (tbnl:post-parameter "password") ""))
                 ;; This needs to come afterward, to ensure we validate the credentials either way
                 user-exists)
             ;; Success! Create a session, and return its ID as a cookie.
             (let ((session (make-instance 'session
                                           :username username
                                           :domain (cookie-domain tbnl:*acceptor*))))
               (log-message :debug "Client logged in successfully.")
               (store-session (session-server tbnl:*acceptor*) session)
               ;; Return feedback to customer
               (setf (tbnl:return-code*) tbnl:+http-ok+)
               (setf (tbnl:content-type*) "text/plain")
               (tbnl:set-cookie "sessionid"
                                :value (identifier session)
                                ;; Not available for use by Javascript
                                :http-only t
                                ;; Be explicit that this cookie is for this entire subdomain
                                :domain (cookie-domain tbnl:*acceptor*)
                                ;; Use this cookie for all requests in this domain
                                :path "/"
                                ;; FIXME :secure needs to be set to `t` ASAP.
                                :secure nil)
               "OK")
             ;; Failure!
             (progn
               (log-message :debug "Login attempt failed.")
               (setf (tbnl:return-code*) tbnl:+http-bad-request+)
               (setf (tbnl:content-type*) "text/plain")
               "Login failed")))))
      ;; Login - fail due to missing parameters:
      ;; Client didn't supply username and/or password parameter
      ((and
         (equal (first uri-parts) "sessions")
         (equal (tbnl:request-method*) :POST))
       (log-message :debug "Client requested login, but failed to supply username and/or password.")
       (setf (tbnl:return-code*) tbnl:+http-bad-request+)
       (setf (tbnl:content-type*) "text/plain")
       "username and password parameters are both required.")
      ;; GET - fetch details of the current session
      ((and (equal (first uri-parts) "sessions")
            (equal (tbnl:request-method*) :GET))
       (log-message :debug "Client requested details of their session.")
       ;; May as well return JSON either way,
       ;; in case the client tries to parse that before checking the status code.
       (setf (tbnl:content-type*) "application/json")
       (handler-case
         ;; Try to fetch the session  details
         (let ((session-details
                 (fetch-session (session-server tbnl:*acceptor*) (tbnl:cookie-in "sessionid"))))
           ;; Was anything returned? I.e, does this client have a valid session right now?
           (if (null session-details)
             ;; No valid session; tell 'em to authenticate and try again.
             (progn
               (log-message :debug "No session found.")
               (setf (tbnl:return-code*) tbnl:+http-authorization-required+)
               "{}")
             ;; Session is valid; return details
             (progn
               (log-message :debug "Returning session details")
               (setf (tbnl:return-code*) tbnl:+http-ok+)
               (cl-json:encode-json-to-string `(("username" . ,(username session-details)))))))
         (usocket:unknown-error
           (e)
           (progn (setf (tbnl:return-code*) tbnl:+http-internal-server-error+)
                  (setf (tbnl:content-type*) "text/plain")
                  (format nil "~A: ~A" (usocket::usocket-errno e) (usocket::usocket-real-error e))))))
      ;; Logout - delete the session
      ;; This should be an idempotent operation, *ensuring* this session isn't present.
      ((and
         (equal (first uri-parts) "sessions")
         (equal (tbnl:request-method*) :DELETE))
       (log-message :debug "Attempting to log user out.")
       ;; To minimise the potential for a timing attack,
       ;; if the client didn't supply a session-cookie then we substitute
       ;; a UUID modified to be invalid, in its place.
       (let ((session-id (or (tbnl:cookie-in "sessionid")
                             "F33EC42DX916CX4449XB972XAD404CBE0052")))
         (if (tbnl:cookie-in "sessionid")
           (log-message :debug (format nil "Attempting to delete session '~A'" session-id))
           (log-message :debug "No session-cookie found; attempting to delete a decoy session."))
         (delete-session (session-server tbnl:*acceptor*)
                         session-id)
         ;; Either way, we're returning the same response.
         (setf (tbnl:content-type*) "text/plain")
         (setf (tbnl:return-code*) tbnl:+http-ok+)
         (tbnl:set-cookie "sessionid"
                          :value session-id
                          ;; EXPIRED
                          :expires (- (get-universal-time) 60)
                          ;; Not available for use by Javascript
                          :http-only t
                          ;; Be explicit that this cookie is for this entire subdomain
                          :domain (cookie-domain tbnl:*acceptor*)
                          ;; Use this cookie for all requests in this domain
                          :path "/"
                          ;; FIXME :secure needs to be set to `t` ASAP.
                          :secure nil)
         "OK"))
      ;;
      ;; Account management
      ;;
      ;; GET = fetch a listing of all accounts, for admin use.
      ((and
         (equal (first uri-parts) "accounts")
         (equal (tbnl:request-method*) :GET))
       (log-message :info "Request received for an account listing.")
       (if (and auth-session
                (equal (username auth-session) "ScAdmin"))
         ;; Admin user is permitted.
         (progn
           (log-message :debug "Requester is admin; proceeding.")
           (setf (tbnl:return-code*) tbnl:+http-ok+)
           (setf (tbnl:content-type*) "application/json")
           (cl-json:encode-json-to-string
             (fetch-accounts (auth-server tbnl:*acceptor*))))
         ;; Non-admin user: reject this request immediately.
         (progn
           (log-message :warn
                        (format nil "Rejecting request for account listing from non-admin user~A"
                                (if auth-session
                                  (format nil " '~A'"(username auth-session))
                                          "")))
           (setf (tbnl:return-code*) tbnl:+http-forbidden+)
           (setf (tbnl:content-type*) "text/plain")
           "Only the admin user can make this request.")))
      ;;
      ;; Activate/deactivate accounts
      ((and
         (equal (first uri-parts) "accounts")
         (equal (tbnl:request-method*) :PUT)
         (tbnl:parameter "active")
         (tbnl:parameter "username")
         auth-session)
       (log-message :info (format nil "Received (de)activation request for account '~A'"
                                  (username auth-session)))
       (setf (tbnl:content-type*) "text/plain")
       (cond
         ((not (and
                 (stringp (tbnl:parameter "username"))
                 (not (equal (tbnl:parameter "username") ""))))
          (log-message :debug "Invalid value of username parameter.")
          (setf (tbnl:return-code*) tbnl:+http-bad-request+)
          "'username' parameter must be a non-empty string.")
         ;; Deactivate the account
         ((cl-ppcre:scan "^false$" (tbnl:parameter "active"))
          (log-message :debug "Request is to deactivate an account.")
          (deactivate-account (auth-server tbnl:*acceptor*) (tbnl:parameter "username"))
          (setf (tbnl:return-code*) tbnl:+http-ok+)
          "OK")
         ;; Reactivate the account
         ((cl-ppcre:scan "^true$" (tbnl:parameter "active"))
          (log-message :debug "Request is to activate an account.")
          (reactivate-account (auth-server tbnl:*acceptor*) (tbnl:parameter "username"))
          (setf (tbnl:return-code*) tbnl:+http-ok+)
          "OK")
         ;; Unrecognised value of 'active' parameter
         (t
           (log-message :debug "'active' parameter was neither true nor false. Disregarding.")
           (setf (tbnl:return-code*) tbnl:+http-bad-request+)
           "Invalid value for 'active' parameter.")))
      ;;
      ;; Change password
      ((and
         (equal (tbnl:request-method*) :PUT)
         (equal (first uri-parts) "accounts")
         auth-session
         ;; The reason for putting all these checks on the "password" arg up front
         ;; is that some web clients will send a value for each of the fields in a form,
         ;; even when there are several forms on the same page.
         ;; When this function actively rejected requests with empty password args,
         ;; it blocked activate/deactivate requests because that clause was never reached.
         (tbnl:post-parameter "password")
         (stringp (tbnl:post-parameter "password"))
         (not (equal (stringp (tbnl:post-parameter "password")) "")))
       (log-message :info (format nil "Received password-change request from '~A'"
                                  (username auth-session)))
       (cond
         ;; Admin requesting change of another user's password
         ((and (tbnl:post-parameter "username")
               (stringp (tbnl:post-parameter "username"))
               (equal (username auth-session) "ScAdmin"))
          (let ((db-session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*))))
            (log-message :debug (format nil "Admin requested change to password for '~A'"
                                        (sanitise-uid (tbnl:post-parameter "username"))))
            ;; May as well set this earlier than later.
            (setf (tbnl:content-type*) "text/plain")
            ;; Is this a real user?
            (if (resource-exists-p db-session
                                   (list "People" (sanitise-uid (tbnl:post-parameter "username"))))
              (progn
                (neo4cl:disconnect db-session)
                (log-message :info
                             (format nil "Attempting to change password for '~A' per admin request."
                                     (sanitise-uid (tbnl:post-parameter "username"))))
                ;; Try to change the password, then respond accordingly.
                (if (change-password (auth-server tbnl:*acceptor*)
                                     (sanitise-uid (tbnl:post-parameter "username"))
                                     (tbnl:post-parameter "password"))
                  ;; Success!
                  (progn
                    (setf (tbnl:return-code*) tbnl:+http-created+)
                    "")
                  ;; Failure!
                  (return-service-error "Password change failed. Please contact the admin.")))
              ;; This was not a real user.
              (progn
                (setf (tbnl:return-code*) tbnl:+http-bad-request+)
                "This is not a valid username."))))
         ;; Default case: valid request from a user to change their own password
         (t
           (let ((db-session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*))))
             (neo4cl:disconnect db-session)
             (log-message :info (format nil "Attempting to change password for '~A'"
                                        (username auth-session)))
             (if (change-password (auth-server tbnl:*acceptor*)
                                  (username auth-session)
                                  (tbnl:post-parameter "password"))
               (progn
                 (setf (tbnl:return-code*) tbnl:+http-created+)
                 "")
               (return-service-error "Password change failed. Please contact the admin."))))))
      ;;
      ;; POST = create a new account (admin-only)
      ((and
         (equal (tbnl:request-method*) :POST)
         (equal (first uri-parts) "accounts"))
       (cond
         ;; Sanity-check: username not specified.
         ((not (and (tbnl:post-parameter "username")
                    (stringp (tbnl:post-parameter "username"))
                    (not (equal (tbnl:post-parameter "username") ""))))
          (return-client-error "non-empty username parameter must be specified."))
         ;; Sanity-check: initial password not specified.
         ((not (and (tbnl:post-parameter "password")
                    (stringp (tbnl:post-parameter "password"))
                    (not (equal (tbnl:post-parameter "password") ""))))
          (return-client-error "non-empty password parameter must be specified."))
         ;; Default case: valid request to create a new account.
         (t
           (log-message :debug (format nil "Attempting to create account '~A'"
                                       (tbnl:post-parameter "username")))
           (handler-case
             (let ((db-session (neo4cl:establish-bolt-session (datastore tbnl:*acceptor*))))
               ;; Ensure a People resource exists for this UID
               (unless (resource-exists-p
                         db-session
                         (list "People" (sanitise-uid (tbnl:post-parameter "username"))))
                 (store-resource db-session
                                 (resourcetype-schema tbnl:*acceptor*)
                                 "People"
                                 `(("ScUID" . ,(sanitise-uid (tbnl:post-parameter "username"))))
                                 "ScAdmin"))
               ;; Now add the auth account
               (add-account (auth-server tbnl:*acceptor*)
                            (sanitise-uid (tbnl:post-parameter "username"))
                            (tbnl:post-parameter "password"))
               (setf (tbnl:return-code*) tbnl:+http-created+)
               (setf (tbnl:content-type*) "text/plain")
               "Created")
             (client-error
               (e)
               (if (cl-ppcre:scan "already contains an account with name" (message e))
                 (return-client-error (format nil "Account ~A already exists."
                                              (sanitise-uid (tbnl:post-parameter "username"))))
                 (return-client-error (message e))))))))
      ;;
      ;; DELETE = remove the account altogether.
      ;; This may involve some trickery where we prevent somebody taking over a deceased account.
      ;; Definitely need to warn users that it's a better idea to just leave it deactivated.
      ((and
         (equal (tbnl:request-method*) :DELETE)
         (equal (first uri-parts) "accounts"))
       (log-message :debug "Handling DELETE request for the accounts endpoint")
       (cond
         ;; No username supplied. This ain't going to work out.
         ((or (null (tbnl:post-parameter "username")))
          (return-client-error "username must be specified as a POST-style parameter."))
         ;; Sanity-checks passed; try to delete the thing
         (t
           (progn
             (log-message :debug (format nil "Attempting to remove account '~A'"
                                         (tbnl:post-parameter "username")))
             (delete-account (auth-server tbnl:*acceptor*)
                             (sanitise-uid (tbnl:post-parameter "username")))
             (setf (tbnl:return-code*) tbnl:+http-no-content+)
             (setf (tbnl:content-type*) "text/plain")
             ""))))
      ;;
      ;; Methods we don't support.
      ;; Take the whitelist approach
      ((not (member (tbnl:request-method*) '(:POST :GET :PUT :DELETE)))
       (method-not-allowed))
      ;; Handle all other cases
      (t
        (log-message :warn (format nil "Bad request received with URI: ~A" (tbnl:request-uri*)))
        (return-client-error "This wasn't a valid request")))))
