;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Methods and functions specifically relating to canonicalisation of resources

(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))

(defun canonical-path-p (path resourcetype-schema)
  "Verify whether the given path is a canonical one.
   Note that it doesn't verify whether it's actually present in the database."
  (declare (type list path)
           (type hash-table resourcetype-schema))
  (multiple-value-bind (canonical-path remainder)
    (canonical-subpath resourcetype-schema path)
    (and (null remainder)
         (equal path canonical-path)
         (not (dependent (gethash (first path) resourcetype-schema))))))


(defgeneric canonical-subpath (resourcetype-schema path &optional canonical-portion)
  (:documentation
    "Given a path to a resource, return multiple values:
     - Primary value: the subset representing a valid canonical path to the target resource at its end.
     - Secondary value: the non-canonical head-portion of the path.
     Does not check whether said path is actually present in the database."))

;; FIXME: define suitable error conditions
;; SECURITY: it can be gamed by passing an invalid canonical-portion argument.
(defmethod canonical-subpath ((resourcetype-schema hash-table)
                              (path list)
                              &optional canonical-portion)
  ;; Check the simple cases first
  (cond
    ;; End of the list
    ((null path)
     (values canonical-portion nil))
    ;; Is the path a list _of strings_?
    ((not (every #'stringp path))
     (error 'bad-argument :message "Path must be a list of strings, and no other type."))
    ;; 2-element lists.
    ;; Should only occur when the path is that of an isolated primary resource.
    ((equal 2 (length path))
     (let* ((uid (car (last path)))
            (resourcetype (car (last path 2)))
            (rtypedef (gethash resourcetype resourcetype-schema)))
       (cond
         ;; Resourcetype doesn't exist
         ((null rtypedef)
          (error 'undefined-resourcetype
                 :message (format nil "Resourcetype ~A is not defined in this resourcetype-schema"
                                  resourcetype)))
         ;; Resourcetype is dependent: the path we've been given is itself only partial.
         ((dependent rtypedef)
          (log-message :warn (format nil "Path ~{/~A~} is headed by a dependent resource." path))
          (values (append (list resourcetype uid) canonical-portion)
                  nil))
         ;; Default: it's valid, and this is a primary resourcetype.
         (t
           (log-message :debug (format nil "Path ~{/~A~} is a primary resource: returning it as the head." path))
           (values (append (list resourcetype uid)
                           canonical-portion)
                   nil)))))
    ;; Sanity-check: is the path-length plausible?
    ;; Should be a series of 1+ type/uid/relationship triples, plus a 2-element target.
    ((and (null canonical-portion)
          ;; We don't need to check that it's >2 here,
          ;; because we already handled the case where length == 2.
          (not (= 2 (mod (length path) 3))))
     (error 'bad-argument :message "Path-length is not possible for a valid path."))
    ;; Valid path-length, and we're at the start of the process,
    ;; but the target type isn't defined.
    ((and (null canonical-portion)
          (null (gethash (car (last path 2)) resourcetype-schema)))
     (error 'undefined-resourcetype :message "Target resourcetype is not defined."))
    ;; We're at the start of this process; prime the pump, and go back around.
    ((null canonical-portion)
     (log-message :debug "Found the tail of the path; recursing upward.")
     (canonical-subpath resourcetype-schema (butlast path 2) (last path 2)))
    ;; Longer-path non-simple cases.
    (t
      (log-message :debug "Processing a dependent resource in the path.")
      (let* ((relationship (car (last path)))
             (uid (car (last path 2)))
             (resourcetype (car (last path 3)))
             (target-type (car canonical-portion))
             ;; The definition of the resourcetype, from the resourcetype-schema
             (rtypedef (gethash resourcetype resourcetype-schema))
             ;; The relationship from that resourcetype to the head of the canonicalised subpath
             (reldef (when rtypedef
                       (or (get-relationship resourcetype-schema
                                             resourcetype
                                             relationship
                                             target-type)
                           (get-relationship resourcetype-schema
                                             "any"
                                             relationship
                                             target-type)
                           (get-relationship resourcetype-schema
                                             resourcetype
                                             relationship
                                             "any")
                           (get-relationship resourcetype-schema
                                             "any"
                                             relationship
                                             "any")))))
        (cond
          ;; The parent resourcetype doesn't exist
          ((null rtypedef)
           (error 'undefined-resourcetype
                  :message (format nil "Resourcetype ~A is not defined in this resourcetype-schema"
                                   resourcetype)))
          ;; The parent resourcetype doesn't have such a relationship to the head of the
          ;; canonical portion
          ((null reldef)
           (error 'undefined-relationship
                  :message (format nil "Resourcetype ~A has no relationship ~A to type ~A"
                                   resourcetype relationship target-type)))
          ;; We found the root
          ((and (not (dependent rtypedef))
                (equal "dependent" (reltype reldef)))
           (log-message :debug "Found the root of this path; returning.")
           (values (append (list resourcetype uid relationship)
                           canonical-portion)
                   (butlast path 3)))
          ;; We found a parent, which is itself dependent
          ((and (dependent rtypedef)
                (equal "dependent" (reltype reldef)))
           (log-message :debug "This resource's parent is also dependent. Recursing upward.")
           (canonical-subpath resourcetype-schema
                              (butlast path 3)
                              (append (list resourcetype uid relationship)
                                      canonical-portion)))
          ;; Default case: it's a valid relationship, but not a dependent one.
          (t
            (log-message :warn "Reached a non-dependent relationship. Returning the canonical portion.")
            (values canonical-portion path)))))))

(defun re-order-path (parent-path)
  "Take a list of strings in the order used by get-canonical-head,
   and re-order it into a top-down list of type/uid/relationship triples, as a single list."
  (declare (type list parent-path))
  (log-message :debug (format nil "Re-ordering list ~{/~A~}" parent-path))
  (when parent-path
    (append (list (second (last parent-path 3))
                  (third (last parent-path 3))
                  (first (last parent-path 3)))
            (re-order-path (butlast parent-path 3)))))


(defgeneric get-canonical-head (db resourcetype-schema target-path node-id &optional parent-path)
  (:documentation "Return the canonical parent-path to a dependent resource."))

;; FIXME Needs lots of error-handling
(defmethod get-canonical-head ((db neo4cl:bolt-session)
                               (resourcetype-schema hash-table)
                               (target-path list)
                               (node-id integer)
                               &optional parent-path)
  (log-message :debug (format nil "Finding canonical head for '~{/~A~}' with node ID ~D."
                              target-path node-id))
  (log-message :debug (format nil "Parent path identified so far: '~{/~A~}'." parent-path))
  (let ((candidate-parent
          ;; There should be at most one; return either one or none.
          (car
            ;; Filter out any incoming relationships that are _not_ dependent.
            (remove-if #'null
                       (mapcar
                         (lambda (candidate)
                           (let* ((rel (cdr (assoc "relationship" candidate :test #'equal)))
                                  (rtype (cdr (assoc "labels" candidate :test #'equal)))
                                  (uid (cdr (assoc "ScUID" candidate :test #'equal)))
                                  (rtypedef (gethash rtype resourcetype-schema))
                                  (reldef (get-relationship resourcetype-schema
                                                            rtype
                                                            rel
                                                            (car (last (or parent-path
                                                                           target-path) 2)))))
                             ;; If the relationship from this one is dependent,
                             ;; return a plist of the parent resourcetype, its UID, and
                             ;; its relationship to the target.
                             (log-message :debug "Checking whether this relationship is dependent")
                             (when (and
                                     rtypedef
                                     reldef
                                     (equal "dependent" (reltype reldef)))
                               (list reldef rtypedef uid))))
                         ;; Query the database for all _incoming_ relationships to the end
                         ;; of the composite path
                         (let ((query-string
                                 (format nil "MATCH ~A~{<-[:~A]-(:~A {ScUID: \"~A\"})~}<-[r]-(t) WHERE id(n) = $node RETURN TYPE(r) as relationship, LABELS(t) AS labels, t.ScUID AS uid"
                                         (uri-node-helper target-path)
                                         parent-path)))
                           ;; Debugging info: is this query even right?
                           (log-message
                             :debug
                             (format nil "Checking for candidate parent resources with query-string '~A'"
                                     query-string))
                           (neo4cl:bolt-transaction-autocommit db
                                                               query-string
                                                               :parameters `(("node" . ,node-id)))))))))
    (if (null candidate-parent)
        (log-message :warn "No candidate parent found.")
        (log-message :debug (format nil "Candidate parent: /~A/~A/~A."
                                    (name (first candidate-parent))
                                    (name (second candidate-parent))
                                    (third candidate-parent))))
    ;; Now inspect the candidates (there should be zero or one) and decide what to do from here.
    (cond
      ;; Head of the line.
      ;; Return the path, after re-ordering it into top-down, type/uid/relationship form
      ((and candidate-parent
            (not (dependent (second candidate-parent))))
       (log-message :debug "Found the head of the path")
       (re-order-path (append parent-path
                              (list (name (first candidate-parent))
                                    (name (second candidate-parent))
                                    (third candidate-parent)))))
      ;; Intermediate, dependent parent. Go around again
      ((and candidate-parent
            (dependent (second candidate-parent)))
       (log-message :debug "Found an intermediate, dependent parent. Going around again.")
       (get-canonical-head db
                           resourcetype-schema
                           target-path
                           node-id
                           (append parent-path
                                   (list (name (first candidate-parent))
                                         (name (second candidate-parent))
                                         (third candidate-parent)))))
      ;; Anything else most likely means this is a dead trail.
      (t
       (log-message
         :error
         "Lost the trail. This resource appears to be an orphan, which shouldn't exist.")
       nil))))


(defgeneric get-canonical-path (db resourcetype-schema path node-id)
            (:documentation "Return the canonical URI paths to a resource path, as list of strings.
                            Uses the Neo4j node ID to disambiguate multiple dependent resources that share a relative path.
                            Canonical means that it starts with a primary resource, and follows only dependent
                            relationships to the target, thus representing its identity."))

(defmethod get-canonical-path ((db neo4cl:bolt-session)
                               (resourcetype-schema hash-table)
                               (path list)
                               (node-id integer))
  (log-message :debug (format nil "Attempting to canonicalise path '~{/~A~}'." path))
  (let* ((target-type (car (last path 2)))
         (target-uid (car (last path)))
         (target-typedef (gethash target-type resourcetype-schema)))
    ;; Is it a primary or dependent type?
    (cond
      ;; Is it a valid type in this resourcetype-schema?
      ((null target-typedef)
       (error 'client-error
              :message (format nil "The resourcetype '~A' is not defined in this resourcetype-schema."
                               target-type)))
      ;;
      ;; Is it a valid path to a single resource?
      ((not (and (> (length path) 1)
                 (= 2 (mod (length path) 3))))
       (error 'client-error :message "This is not a valid path to a single resource."))
      ;;
      ;; Is the path a list _of strings_?
      ((not (every #'stringp path))
       (error 'bad-argument :message "Path must be a list of strings, and no other type."))
      ;;
      ;; Is the nodes parameter a non-empty list of integers?
      ((not (integerp node-id))
       (error 'bad-argument :message "node-id must be an integer"))
      ;;
      ;; It's a primary resourcetype; reconstruct the path as a string and return it.
      ((null (dependent target-typedef))
       (log-message :debug (format nil "~{/~A~} is a primary resource; returning directly." path))
       ;; Return the resource's path, discarding any indirection that precedes it
       ;; in the supplied path.
       ;; I.e, if the path follows several relationships to eventually reach a primary resource,
       ;; just return the resource at the end of that path.
       (list target-type target-uid))
      ;;
      ;; If we got here, it's a valid dependent type.
      ;; Now we go forensic on its ass.
      (t
       (log-message :debug (format nil "~{/~A~} is a dependent resource. Tracing upward." path))
       ;; First, trace backwards along the supplied path to find the canonical subset.
       (multiple-value-bind (canonical-subpath path-remainder)
         (canonical-subpath resourcetype-schema path)
         ;; Head of the subpath is a primary resource
         (if (not (dependent (gethash (car canonical-subpath) resourcetype-schema)))
             canonical-subpath
             ;; This strategy takes into account the fact that Neo4j will exclude the MATCH path
             ;; from the list of inbound connections to the target resource, when assembling its
             ;; return value:
             ;; The obvious approach is to use a single Cypher query, but that won't work because we
             ;; need to check whether each resource and relationship on the path is a dependent one.
             ;; Second, trace backwards along the supplied path, checking for other branches
             ;; that produce canonical paths.
             (append
               (get-canonical-head
                 db
                 resourcetype-schema
                 (append path-remainder
                         (list (first canonical-subpath) (second canonical-subpath)))
                 node-id)
               canonical-subpath)))))))

(defgeneric canonicalise-resource (db path resourcetype-schema)
            (:documentation "Update the 'ScCanonicalpath' attribute in the specified resource."))

(defmethod canonicalise-resource ((db neo4cl:bolt-session)
                                  (path list)
                                  (resourcetype-schema hash-table))
  (log-message :debug (format nil "Canonicalising the resource at path ~{/~A~}" path))
  (if (canonical-path-p path resourcetype-schema)
    (let ((query-string (format nil "MATCH ~A SET n.ScCanonicalpath = $PATH"
                                (uri-node-helper path :marker "n"))))
      (log-message
        :debug (format nil "Canonicalising resource with query '~A' and path ~{/~A~}"
                       query-string path))
      (neo4cl:bolt-transaction-autocommit
        db
        query-string
        :parameters `(("PATH" . ,(format nil "~{/~A~}" path)))))
    (error 'bad-argument :message (format nil "Invalid path ~{/~A~}" path))))

(defgeneric recursively-canonicalise-resource (db path resourcetype-schema)
            (:documentation "Update the 'ScCanonicalpath' attribute in the specified resource *and* all its dependents, recursively until you run out of dependents."))

(defmethod recursively-canonicalise-resource ((db neo4cl:bolt-session)
                                              (path list)
                                              (resourcetype-schema hash-table))
  (log-message :debug (format nil "Recusively canonicalising resource ~{/~A~}" path))
  ;; Canonicalise this resource
  (canonicalise-resource db path resourcetype-schema)
  ;; Now canonicalise its dependents
  (dolist (dependent (get-dependent-resources db resourcetype-schema path))
    (recursively-canonicalise-resource db (append path dependent) resourcetype-schema)))

(defgeneric recanonicalise-all-the-resources (db resourcetype-schema)
  (:documentation "Systematically recompute the canonical path for every resource in the database.
                   You probably only want to do this if your installation pre-dates precanonicalisation."))

(defmethod recanonicalise-all-the-resources ((db neo4cl:bolt-session)
                                             (resourcetype-schema hash-table))
  (log-message :debug "Recanonicalising all the resources")
  (let ((typenames (list))) ; Accumulator for names of resourcetypes
    ;; Populate the list with primary resourcetypes
    (maphash (lambda (key val)
               (unless (dependent val)
                 (push key typenames)))
             resourcetype-schema)
    ;; Iterate over a sorted copy of the list of typenames
    (dolist (typename (sort typenames #'string<))
      ;; Iterate over each instance of the resourcetype at hand
      (dolist (resource
                (let ((query-string (format nil "MATCH (n:~A) RETURN n.ScUID AS uid" typename)))
                  (neo4cl:bolt-transaction-autocommit db query-string)))
        ;; Update `ScCanonicalpath` for that instance
        (log-message :debug
                                 (format nil "Recursively re-canonicalising ~A ~A"
                                         typename (cdr (assoc "uid" resource :test #'equal))))
        (recursively-canonicalise-resource
          db
          (list typename (cdr (assoc "uid" resource :test #'equal)))
          resourcetype-schema)))))
