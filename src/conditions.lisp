;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


;; API conditions

(define-condition integrity-error (error)
  ((message :initarg :message
            :reader message))
  (:report (lambda (condition stream)
             (format stream "Integrity error: ~A" (message condition))))
  (:documentation "Signals an attempt to do something that would violate the integrity model of syscat and, by implication, the application built on it."))

(define-condition client-error (error)
  ((message :initarg :message
            :reader message))
  (:report (lambda (condition stream)
             (format stream "Client error: ~A" (message condition))))
  (:documentation "The client made an invalid request, e.g. required parameters were missing."))


;; Internal conditions

(define-condition undefined-resourcetype (error)
  ((message :initarg :message
            :reader message))
  (:report (lambda (condition stream)
             (format stream "Undefined resourcetype: ~A" (message condition))))
  (:documentation "This function was called with an undefined resourcetype among its arguments."))

(define-condition undefined-relationship (error)
  ((message :initarg :message
            :reader message))
  (:report (lambda (condition stream)
             (format stream "Undefined relationship: ~A" (message condition))))
  (:documentation "This function was called with an undefined relationship among its arguments."))

(define-condition bad-argument (error)
  ((message :initarg :message
            :reader message))
  (:report (lambda (condition stream)
             (format stream "Bad argument: ~A" (message condition))))
  (:documentation "This function was called with an invalid argument."))

(define-condition misconfiguration (error)
  ((message :initarg :message
            :reader message
            :type text))
  (:report (lambda (condition stream)
             (format stream "Misconfiguration: ~A" (message condition))))
  (:documentation "Indicates a fatal problem with the supplied configuration."))

(define-condition bad-schema-definition (error)
  ((message :initarg :message
            :reader message
            :type text))
  (:report (lambda (condition stream)
             (format stream "Bad schema definition: ~A" (message condition))))
  (:documentation "The supplied schema contains a fatally invalid definition."))


;; Database-related conditions

(define-condition bad-data-in-database (error)
  ((message :initarg :message
            :reader message
            :type text))
  (:report (lambda (condition stream)
             (format stream "Bad data in the database: ~A" (message condition))))
  (:documentation "There's a problem with the datathat was retrieved from the database."))
