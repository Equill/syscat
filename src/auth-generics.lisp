;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory


;;;; Generic functions for authentication and user-management

(in-package :syscat)


(defgeneric check-user-creds (server username password)
  (:documentation "Validate the supplied credentials, returning a boolean to indicate success/failure.
                   Perform no other actions.
                   `base-dn` must exclude the 'ou=users' RDN, because that gets automatically prepended."))


(defgeneric add-domain (server)
  (:documentation "Ensures this server's domain is defined on the authentication server.
This will be a no-op in some cases, e.g. an externally-managed LDAP server."))

(defmethod add-domain (server)
  "Default method for cases where a more specific method has not been defined."
  nil)


(defgeneric add-account (server username password)
  (:documentation "Create a user under that subdomain.
Note: the password must be pre-hashed. This method does *not* perform any password hashing."))

(defmethod add-account (server username password)
  "Default method for cases where a more specific method has not been defined."
  nil)


(defgeneric fetch-accounts (server)
  (:documentation "Fetch a listing of all accounts on the authentication server."))

(defmethod fetch-accounts (server)
  "Default method for cases where a specific method has not been defined."
  nil)


(defgeneric change-password (server username password)
  (:documentation "Update the password for a user under that subdomain."))

(defgeneric deactivate-account (server username)
  (:documentation "Stop an account being used, without actually deleting. Should also remove all sessions associated with this account."))

(defgeneric reactivate-account (server username)
  (:documentation "Restore an account's authentication privileges."))

(defgeneric delete-account (server username)
  (:documentation "Permanently remove a user account from the authentication server."))
