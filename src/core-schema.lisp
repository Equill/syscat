;   Copyright Kat Sebastian <kat@electronic-quill.net>
;
;   Licensed under the AGPL-3.0 License
;   - for details, see LICENSE.txt in the top-level directory

;;;; Configs for the server to use

(in-package #:syscat)

(declaim (optimize (compilation-speed 0)
                   (speed 2)
                   (safety 3)
                   (debug 3)))


;; Define the core schemas, without which Syscat won't work properly
(defparameter *core-schema*
  (make-incoming-subschema-version
    :name "core"
    :resourcetypes
    (list (make-incoming-rtypes
            :name "any"
            :description "Special-case meta-resource, representing an instance of any type of resource. This is used for defining relationships where either the source or target could be, well, any resourcetype. The server refuses to create an instance of this resourcetype."
            :dependent nil
            :attributes ())
          (make-incoming-rtypes
            :name "Organisations"
            :description "Any kind of organisation: professional, social or other."
            :dependent nil
            :attributes (list (make-incoming-rtype-attrs
                                (list :name "description"
                                      :type "text"
                                      :description "Notes about this particular organisation."))))
          (make-incoming-rtypes
            :name "People"
            :description "Real people, imaginary people, security roles, members of an external organisation... if they're a person, this is the type."
            :dependent nil
            :attributes (list (make-incoming-rtype-attrs
                                (list :name "displayname"
                                      :type "varchar"
                                      :maxlength 256
                                      :description "The human-friendly version of their name, to be displayed in the UI."))
                              (make-incoming-rtype-attrs
                                (list :name "notes"
                                      :type "text"
                                      :description "Notes about this person."))))
          (make-incoming-rtypes
            :name "Pronouns"
            :description "The pronouns by which a person prefers to be addressed."
            :dependent nil
            :attributes (list (make-incoming-rtype-attrs
                                (list :name "text"
                                      :type "varchar"
                                      :maxlength 48
                                      :description "The full, non-URL-safe text of the pronoun set. E.g, They/them."))))
          (make-incoming-rtypes
            :name "Files"
            :description "Metadata about files uploaded by users. The files themselves are stored separately, using the sha3-256 checksum as the filename."
            :dependent nil
            :attributes (list (make-incoming-rtype-attrs
                                (list :name "title"
                                      :type "varchar"
                                      :maxlength 512
                                      :description "The requested filename, recorded verbatim instead of having to be sanitised for URI-safety."))
                              (make-incoming-rtype-attrs
                                (list :name "notes"
                                      :type "text"
                                      :description "Notes about this file."))))
          (make-incoming-rtypes
            :name "MediaTypes"
            :description "As defined in https://www.iana.org/assignments/media-types/media-types.xhtml. Formerly known as MIME-types."
            :dependent nil
            :attributes (list (make-incoming-rtype-attrs
                                 '(:name "name"
                                   :type "varchar"
                                   :description "The un-sanitised name for this type, e.g. image/jpeg."))))
          (make-incoming-rtypes
            :name "MediatypeTypes"
            :description "The first portion of a MediaType name, e.g. image for image/jpeg."
            :dependent nil
            :attributes nil)
          (make-incoming-rtypes
            :name "VrfGroups"
            :description "VRF Groups, as allocated by an organisation."
            :dependent t
            :attributes (list (make-incoming-rtype-attrs
                                (list :name "description"
                                      :type "text"
                                      :description "Helpful notes about what this group is for."))))
          (make-incoming-rtypes
            :name "Ipv4Subnets"
            :description "IPv4 Subnets, as allocated rather than as configured."
            :dependent t
            :attributes (list (make-incoming-rtype-attrs
                                (list :name "description"
                                      :type "text"
                                      :description "Who or what this subnet is allocated for, and possibly why."))
                              (make-incoming-rtype-attrs
                                (list :name "netaddress"
                                      :type "varchar"
                                      :maxlength 15
                                      :description "The network address of the subnet."))
                              (make-incoming-rtype-attrs
                                (list :name "prefixlength"
                                      :type "integer"
                                      :minimum 1
                                      :maximum 32
                                      :description "The prefix length of the subnet - an integer between 1 and 32."))))
          (make-incoming-rtypes
            :name "Ipv4Addresses"
            :description "IPv4 Addresses. Unqualified, so really only useful for allocating."
            :dependent t
            :attributes (list (make-incoming-rtype-attrs
                                (list :name "address"
                                      :type "varchar"
                                      :maxlength 15
                                      :description "The actual address, in dotted-quad notation, independent of any sanitisation required to make a URL-safe UID."))
                              (make-incoming-rtype-attrs
                                (list :name "description"
                                      :type "text"
                                      :description "What this address is allocated to, and possibly why."))))
          (make-incoming-rtypes
            :name "Ipv6Subnets"
            :description "IPv6 Subnets, as allocated rather than as configured."
            :dependent t
            :attributes (list (make-incoming-rtype-attrs
                                (list :name "description"
                                      :type "text"
                                      :description "Who or what this subnet is allocated for, and possibly why."))
                              (make-incoming-rtype-attrs
                                (list :name "netaddress"
                                      :type "varchar"
                                      :description "The network address of the subnet."))
                              (make-incoming-rtype-attrs
                                (list :name "prefixlength"
                                      :type "integer"
                                      :minimum 1
                                      :maximum 64
                                      :description "The prefix length of the subnet - an integer between 1 and 64."))))
          (make-incoming-rtypes
            :name "Ipv6Addresses"
            :description "IPv6 Addresses. Unqualified, so really only useful for allocating."
            :dependent t
            :attributes (list (make-incoming-rtype-attrs
                                (list :name "address"
                                      :type "varchar"
                                      :description "The actual address, in canonicalised form, independent of any sanitisation required to make a URL-safe UID."))
                              (make-incoming-rtype-attrs
                                (list :name "description"
                                      :type "text")))
            :description "What this address is allocated to, and possibly why."))
    :relationships (list
                     (make-incoming-rels :name "SC_CREATOR"
                                         :source-types '("any")
                                         :target-types '("People")
                                         :cardinality "many:1"
                                         :description "When creating resources, Syscat automatically links all resources to their creator.")
                     (make-incoming-rels :name "HAS_PRONOUNS"
                                         :source-types '("People")
                                         :target-types '("Pronouns")
                                         :cardinality "many:many"
                                         :description "These are defined as a separate resourcetype partly because some people accept more than one set, and partly to make it easier to add more as necessary.")
                     (make-incoming-rels :name "HAS_ORG_MEMBER"
                                         :source-types '("Organisations")
                                         :target-types '("Organisations" "People")
                                         :cardinality "many:many"
                                         :description "Denotes who belongs to this organisation. Counterpart to MEMBER_OF_ORG.")
                     (make-incoming-rels :name "MEMBER_OF_ORG"
                                         :source-types '("Organisations" "People")
                                         :target-types '("Organisations")
                                         :cardinality "many:many"
                                         :description "Denotes membership of an organisation. Counterpart to HAS_ORG_MEMBER.")
                     (make-incoming-rels :name "CONTAINS_VRF_GROUPS"
                                         :source-types '("Organisations")
                                         :target-types '("VrfGroups")
                                         :cardinality "1:many"
                                         :reltype "dependent"
                                         :description "The Virtual Routing and Forwarding circuits that you've allocated within your organisation's network for IPAM purposes, as distinct from what may or may not actually be configured on the devices themselves.")
                     (make-incoming-rels :name "CONTAINS_IPV4_SUBNETS"
                                         :source-types '("Organisations" "VrfGroups" "Ipv4Subnets")
                                         :target-types '("Ipv4Subnets")
                                         :cardinality "1:many"
                                         :reltype "dependent"
                                         :description "That IPv4 subnet has been allocated under this thing.")
                     (make-incoming-rels :name "CONTAINS_IPV6_SUBNETS"
                                         :source-types '("Organisations" "VrfGroups" "Ipv6Subnets")
                                         :target-types '("Ipv6Subnets")
                                         :cardinality "1:many"
                                         :reltype "dependent"
                                         :description "That IPv6 subnet has been allocated under this thing.")
                     (make-incoming-rels :name "CONTAINS_IPV4_ADDRESSES"
                                         :source-types '("Ipv4Subnets")
                                         :target-types '("Ipv4Addresses")
                                         :cardinality "1:many"
                                         :reltype "dependent"
                                         :description "That address has been allocated from within this subnet.")
                     (make-incoming-rels :name "CONTAINS_IPV6_ADDRESSES"
                                         :source-types '("Ipv6Subnets")
                                         :target-types '("Ipv6Addresses")
                                         :cardinality "1:many"
                                         :reltype "dependent"
                                         :description "That address has been allocated from within this subnet.")
                     (make-incoming-rels :name "SC_UPLOADED"
                                         :source-types '("People")
                                         :target-types '("Files")
                                         :cardinality "1:many"
                                         :reltype "any"
                                         :description "That file was uploaded by a client logged in with this account.")
                     (make-incoming-rels :name "HAS_MEDIA_TYPE"
                                         :source-types '("Files")
                                         :target-types '("MediaTypes")
                                         :cardinality "many:1"
                                         :reltype "any"
                                         :description "This file has that MediaType.")
                     (make-incoming-rels :name "MEDIA_TYPE_OF"
                                         :source-types '("MediaTypes")
                                         :target-types '("Files")
                                         :cardinality "1:many"
                                         :reltype "any"
                                         :description "That file is of this MediaType.")
                     (make-incoming-rels :name "HAS_MEDIATYPE_TYPE"
                                         :source-types '("MediaTypes")
                                         :target-types '("MediatypeTypes")
                                         :cardinality "many:1"
                                         :reltype "any"
                                         :description "This mediatype falls under that type.")
                     (make-incoming-rels :name "MEDIATYPE_TYPE_OF"
                                         :source-types '("MediatypeTypes")
                                         :target-types '("MediaTypes")
                                         :cardinality "1:many"
                                         :reltype "any"
                                         :description "That mediatype falls under this type."))))
