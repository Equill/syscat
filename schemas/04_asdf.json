{
  "name": "asdf",
  "description": "ASDF, Another System Definition Facility, is a build-system for Common Lisp.",
  "dependencies": [ "spdx", "syscat-base" ],
  "resourcetypes": [
    {
      "name": "AsdfDistributions",
      "dependent": false,
      "description": "A set of ASDF systems, such as Quicklisp.",
      "attributes": [
        {
          "name": "Description",
          "description": "The intended purpose and/or audience for this distribution. Explaining the criteria for inclusion/exclusion would also be useful.",
          "type": "text"
        }
      ]
    },
    {
      "name": "AsdfDistributionVersions",
      "dependent": true,
      "description": "A specific version, edition or revision of an ASDF distribution.",
      "attributes": []
    },
    {
      "name": "AsdfSystems",
      "dependent": false,
      "description": "An ASDF system, which may or may not correspond completely to a single package. The definitive reference is at https://asdf.common-lisp.dev/asdf.html",
      "attributes": [
        {
          "name": "Name",
          "description": "The name of the system when it doesn't need to be URL-safe. This is needed because forward-slashes are legal elements of ASDF system names.",
          "type": "varchar",
          "maxlength": 1024
        },
        {
          "name": "LongName",
          "description": "A longer, friendlier name for the system.",
          "type": "text"
        },
        {
          "name": "Description",
          "description": "A description of the system; what it does and what it's for. This should be copied verbatim from the system definition.",
          "type": "text"
        }
      ]
    },
    {
      "name": "AsdfSystemVersions",
      "dependent": true,
      "description": "A specific version of an ASDF system, in the format described in https://asdf.common-lisp.dev/asdf.html#Version-specifiers-1",
      "attributes": []
    }
  ],
  "relationships": [
    {
      "name": "BUILD_SYS_FOR_SOFTWARE",
      "source-types": [ "AsdfSystems" ],
      "target-types": [ "Applications", "Libraries" ],
      "cardinality": "many:1",
      "reltype": "any",
      "description": "This build-system builds some part (possibly all) of that project.",
      "complemented_by": "BUILT_BY_SYSTEM"
    },
    {
      "name": "BUILT_BY_SYSTEM",
      "source-types": [ "Applications", "Libraries" ],
      "target-types": [ "AsdfSystems" ],
      "cardinality": "1:many",
      "reltype": "any",
      "description": "That build-system is used to build part or all of this project. Multiple systems may be used to build different parts of the project.",
      "complemented_by": "BUILD_SYS_FOR_PROJECT"
    },
    {
      "name": "HAS_ASDF_SYSTEM_VERSION",
      "source-types": [ "AsdfSystems" ],
      "target-types": [ "AsdfSystemVersions" ],
      "cardinality": "1:many",
      "reltype": "dependent",
      "description": "That is one version of this ASDF System.",
      "complemented_by": "VERSION_OF_ASDF_SYSTEM"
    },
    {
      "name": "HAS_ASDF_DISTRO_VERSION",
      "source-types": [ "AsdfDistributions" ],
      "target-types": [ "AsdfDistributionVersions" ],
      "cardinality": "1:many",
      "reltype": "dependent",
      "description": "This distribution has that specific version.",
      "complemented_by": "VERSION_OF_ASDF_DISTRIBUTION"
    },
    {
      "name": "VERSION_OF_ASDF_SYSTEM",
      "source-types": [ "AsdfSystemVersions" ],
      "target-types": [ "AsdfSystems" ],
      "cardinality": "many:1",
      "reltype": "any",
      "description": "This denotes one specific version of that ASDF system.",
      "complemented_by": "HAS_ASDF_SYSTEM_VERSION"
    },
    {
      "name": "VERSION_OF_ASDF_DISTRO",
      "source-types": [ "AsdfDistributionVersions" ],
      "target-types": [ "AsdfDistributions" ],
      "cardinality": "many:1",
      "reltype": "any",
      "description": "This is one version of that ASDF System.",
      "complemented_by": "HAS_ASDF_DISTRO_VERSION"
    },
    {
      "name": "TRACKED_BY_ASDF_DISTRO",
      "source-types": [ "GitBranches" ],
      "target-types": [ "AsdfDistributions" ],
      "cardinality": "many:many",
      "reltype": "any",
      "description": "That ASDF distribution tracks this source-code repository branch.",
      "complemented_by": "TRACKS_REPO_BRANCH"
    },
    {
      "name": "TRACKS_REPO_BRANCH",
      "source-types": [ "AsdfDistributions" ],
      "target-types": [ "GitBranches" ],
      "cardinality": "many:many",
      "reltype": "any",
      "description": "This ASDF distribution tracks that source-code repository branch.",
      "complemented_by": "TRACKED_BY_ASDF_DISTRO"
    }
  ]
}
