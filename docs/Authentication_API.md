# Authentication API

This incorporates both authentication per se, and account management.

As you might expect, this is only relevant when the site security policy involves authentication. If the policy is "open", then this API can safely be ignored, and needn't be included in a proxy server's configuration.


# Authentication and session creation/destruction

Sessions are managed via a `sessionid` cookie, whose value is a V4 UUID.

Once you've authenticated and received that cookie, you need to supply it along with a suitable `Host:` header with each request.

The endpoint is `/auth/v1/sessions`


## POST: Authenticate and create a session

```
POST /auth/v1/sessions
```

Mandatory parameters:

- `username`
    - String
- `password`
    - String
    - It's usually best to url-encode this, to avoid problems with special characters in the password.


Mandatory headers:

- `Host:`
    - This is needed to send/receive cookies for the correct domain.
    - Ensure it matches the domain, because this isn't as automatic as you might expect.


Return values:

- Status code: 200
- Body: "OK"
- Cookie: `sessionid`
    - Expected value: a V4 UUID

E.g, to create a session manually for command-line testing:

```
$ curl -X POST -c /tmp/cookies -d 'username=<username>' -data-urlencode 'password=<passwd>' \
    --header 'Host: sctest.onfire.onice' http://192.0.2.1:4950/auth/v1/sessions

OK
```

Remember to specify the `Host:` header in all subsequent requests, to make sure this cookie is then sent back to the server to identify your session.

Cookies are the only supported method for passing session IDs. The only realistic alternative is for clients to supply it as a query parameter, which has too many security and practical drawbacks.

*Note:* when you're using `curl`, the `-c` option tells it to create a new cookie file and populate it with anything returned from this request. This means it _deletes_ an existing file. You need to use `-b` for subsequent requests; this tells `curl` to read the file and update it as necessary.


## GET: Fetch the details of a live session

For Unix/Linux users, this is analogous to the `whoami` command: it verifies which account you're logged into.

```
GET /auth/v1/sessions
```

Parameters: none.

Mandatory cookies:

- `sessionid`


Mandatory headers:

- `Host:`


Return values:

- Status code: 200
- Body: a JSON object, whose sole property is `username`.


Example:

```
$ curl -c /tmp/cookies --header 'Host: sctest.onfire.onice' http://192.0.2.1:4950/auth/v1/sessions

{"username":"ScAdmin"}
```


## DELETE: Destroy a session

End the session by de-registering it from the server and invalidating the session cookie.

```
DELETE /auth/v1/sessions
```

Parameters: none.

Mandatory cookies:

- `sessionid`


Mandatory headers:

- `Host:`


Return values:

- Status code: 200
- Body: "OK"
- Cookies:
    - `sessionid`: expiry date is a time in the recent past.


# Account management

Pretty much what you'd expect, with one wrinkle that's a consequence of an important design decision: each account corresponds to a `People` resource.

When creating an account, the server either connects it to an existing `People` resource, or creates one on the spot. Depending on how you look at it, either you're canonicalising the person with an account, or canonicalising the account with a person. This ensures, among other things, that each account can be associated with the resources it was used to create.


## POST: Create a new account

Note that you need to be logged in as `ScAdmin` to do this.

```
POST /auth/v1/accounts
```

Mandatory parameters:

- `username`
    - Name of the new account.
    - If this corresponds to an existing `People` resource, it will effectively create a login for that person.
- `password`
    - Initial password for the new account.
    - Do remind the user to change that account to a value unknown to anybody else.


Mandatory headers:

- `Host:`
    - This is needed to send/receive cookies for the correct domain.


Mandatory cookies:

- `sessionid`
    - This must identify a current session associated with the `ScAdmin` account.


Return values:

- Status code: 201
- Body: "Created"


Example:

```
$ curl -X POST -c /tmp/cookies -d 'username=<new account name> -d 'password=<password>'' \
    --header 'Host: sctest.onfire.onice' http://192.0.2.1:4950/auth/v1/accounts

OK
```


## PUT

Unusually for this API, this method has a couple of uses.


### Change the password for an account

```
PUT /auth/v1/accounts
```

Mandatory parameters:

- `password`
    - It's recommended to url-encode this, to avoid issues with special characters.


Optional parameters:

- `username`
    - If you're logged in as `ScAdmin`, you can use this parameter to change the password on somebody else's account.


Mandatory headers:

- `Host:`
    - This is needed to send/receive cookies for the correct domain.


Mandatory cookies:

- `sessionid`
    - This must identify a current session associated with the `ScAdmin` account.


Return values:

- Status code: 201
- Body: ""


Example:

```
$ curl -X PUT -c /tmp/cookies -d 'password=<new password>' -d 'username=<forgetful user>' \
    --header 'Host: sctest.onfire.onice' http://192.0.2.1:4950/auth/v1/accounts
```


### Deactivate or reactivate an account

```
PUT /auth/v1/accounts
```

Mandatory parameters:

- `username`
- `active`
    - Must be one of "true" or "false" (case-sensitive).


Mandatory headers:

- `Host:`
    - This is needed to send/receive cookies for the correct domain.


Mandatory cookies:

- `sessionid`
    - This must identify a current session associated with the `ScAdmin` account.


Return values:

- Status code: 200
- Body: "OK"


This can only be done by the `ScAdmin` user.

Note that when you deactivate an account, all active sessions for that account are removed immediately after deactivation.


Example:

```
$ curl -X PUT -c /tmp/cookies -d 'username=<account name>' -d 'active=true' \
    --header 'Host: sctest.onfire.onice' http://192.0.2.1:4950/auth/v1/accounts

OK
```


### Delete an account

```
DELETE /auth/v1/accounts
```

Mandatory parameters:

- `username`


Mandatory headers:

- `Host:`
    - This is needed to send/receive cookies for the correct domain.


Mandatory cookies:

- `sessionid`
    - This must identify a current session associated with the `ScAdmin` account.


Return values:

- Status code: 204
- Body: ""


This can only be done by the `ScAdmin` user.

Note that this removes authentication for the specified username, but leaves the corresponding `People` resource intact.


Example:

```
$ curl -X DELETE -c /tmp/cookies -d 'username=<account to remove>' \
    --header 'Host: sctest.onfire.onice' http://192.0.2.1:4950/auth/v1/accounts

OK
```


### List all the accounts

A GET request performed by the `ScAdmin` user will return a list of objects in JSON format. It takes the guesswork out of managing existing user accounts.
```
GET /auth/v1/accounts
```

Mandatory headers:

- `Host:`
    - This is needed to send/receive cookies for the correct domain.


Mandatory cookies:

- `sessionid`
    - This must identify a current session associated with the `ScAdmin` account.


Return values:

- Status code: 204
- Body: JSON list of objects


Example:

```
$ curl -s -b /tmp/cookies.txt -H 'Host: sctest.onfire.onice' http://192.0.2.1:4950/auth/v1/accounts | jq .
[
  {
    "name": "ScAdmin",
    "createddate": 3897040366,
    "active": true
  }
]
```
