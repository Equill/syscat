# Syscat core schema

When Syscat is started up and doesn't detect an existing schema, it installs one automatically, which covers the very core essentials. Some are assumed to be there by the server, and it'll break if they're not; others are just so obvious that they'll be needed virtually every time.

This is a handy summary of what's in it: resource-types, their attributes, and the relationships that can be created from some to others. It glosses over the cardinality of relationships, and whether they're dependent ones; by the time you need this, you'll be querying the API for the definitive version anyway.

Speaking of which, you _can_ get this information with a get request to `/schema/v1` on a fresh installation. However, it seems like a good idea to provide a convenient reference for exactly what you can rely on already being there, and thus what you can _always_ build on without having to add it yourself or rely on somebody else having added it.


# Resourcetypes

To recap, these are like classes in object-oriented programming - they're the _types of_ things you can create.


## any

Special-case meta-resource, representing an instance of any type of resource. This is used for defining relationships where either the source or target could be, well, any resourcetype. The server refuses to create an instance of this resourcetype.

- Attributes: none.


## Organisations

Any kind of organisation: professional, social or other.

- Attributes:
    - `description` - Notes about this particular organisation.


## People

Real people, imaginary people, security roles, members of an external organisation... if they're a person, this is the type.

- Attributes:
    - `displayname` - The human-friendly version of their name, to be displayed in the UI.
    - `notes` - Notes about this person.


## Pronouns

The pronouns by which a person prefers to be addressed.

- Attributes:
    - `text` - The full, non-URL-safe text of the pronoun set. E.g, They/them.


## Files

Metadata about files uploaded by users. The files themselves are stored separately, using the sha3-256 checksum as the filename.

- Attributes:
    - `title` - The requested filename, recorded verbatim instead of having to be sanitised for URI-safety.
    - `notes` - Notes about this file.
    - `mimetype` - The detected MIME-type of this file, i.e. the description used for attaching files to emails or transferring to/from webservers.
        - Read-only: this attribute _cannot_ be updated via the HTTP client API.
    - `sha3256sum` - The SHA3-256 checksum of the file. Chosen for resistance against length-extension collisions.
        - Read-only: this attribute _cannot_ be updated via the HTTP client API.


## VrfGroups

VRF Groups, as allocated by an organisation.

- Attributes:
    - `description` - Helpful notes about what this group is for.


## Ipv4Subnets

IPv4 Subnets, as allocated rather than as configured.

- Attributes:
    - `description` - Who or what this subnet is allocated for, and possibly why.
    - `netaddress` - The network address of the subnets.
    - `prefixlength` - the prefix length of the subnet - an integer between 1 and 32.


## Ipv4Addresses

Ipv4Addresses. Unqualified, so really only useful for allocating.

- Attributes:
    - `description` - What this address is allocated to, and possibly why.


## Ipv6Subnets

IPv6 Subnets, as allocated rather than as configured.

- Attributes:
    - `description` - Who or what this subnet is allocated for, and possibly why.
    - `netaddress` - The network address of the subnets.
    - `prefixlength` - the prefix length of the subnet - an integer between 1 and 128.


## Ipv6Addresses

Ipv6Addresses. Unqualified, so really only useful for allocating.

- Attributes:
    - `description` - What this address is allocated to, and possibly why.


# Relationships

These are first-class things in their own right.

You can add new source and/or destination resourcetypes to an existing relationship, which comes in handy when you add new resourcetypes.


## `SC_CREATOR`

When creating resources, Syscat automatically links all resources to their creator.

- Source resourcetypes: 
    - `any`
- Target resourcetypes:
    - People


## `HAS_PRONOUNS`

These are defined as a separate resourcetype partly because some people accept more than one set, and partly to make it easier to add more as necessary.

- Source resourcetypes:
    - People
- Target resourcetypes:
    - Pronouns


## `HAS_ORG_MEMBERS`

Denotes who belongs to this organisation. Counterpart to `/People/MEMBER_OF_ORG/Organisations`.

- Source resourcetypes:
    - Organisations
- Target resourcetypes:
    - People


## `MEMBER_OF_ORG`

Denotes membership of an organisation. Counterpart to `/Organisations/HAS_ORG_MEMBERS/People`.

- Source resourcetypes:
    - People
- Target resourcetypes:
    - Organisations


## `CONTAINS_VRF_GROUPS`

The Virtual Routing and Forwarding circuits that you've allocated within your organisation's network for IPAM purposes, as distinct from what may or may not actually be configured on the devices themselves.

- Source resourcetypes:
    - Organisations
- Target resourcetypes:
    - VrfGroups


## `CONTAINS_IPV4_SUBNETS`

That IPv4 subnet has been allocated under this thing.

- Source resourcetypes:
    - Organisations
    - VrfGroups
    - Ipv4Subnets
- Target resourcetypes:
    - Ipv4Subnets


## `CONTAINS_IPV6_SUBNETS`

That IPv6 subnet has been allocated under this thing.

- Source resourcetypes:
    - Organisations
    - VrfGroups
    - Ipv6Subnets
- Target resourcetypes:
    - Ipv6Subnets


## `CONTAINS_IPV4_ADDRESSES`

- Source resourcetypes:
    - Ipv4Subnets
- Target resourcetypes:
    - Ipv4Addresses


## `CONTAINS_IPV6_ADDRESSES`

- Source resourcetypes:
    - Ipv6Subnets
- Target resourcetypes:
    - Ipv6Addresses
