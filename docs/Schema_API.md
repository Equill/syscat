# Schema API reference

# General overview

This API is for uploading schema definitions, and for querying the server about existing definitions.

Schemas define two types of thing:

- Resourcetypes
    - The types of resources that can be created on the server.
    - This includes the attributes of each resourcetype, and the datatype for each attribute.
- Relationships
    - The connections you can make from one resource to another.

This document only covers the API and related behaviour of the server; for details of how to create a schema definition, see [Defining a schema](Defining_a_schema.md).


## Schema versioning

Schemas have a versioning system, which can be useful for testing changes with the confidence of knowing you can roll back to a working version at any time.

Note that this works in a very different way to versioning of resources. Instead of recording snapshots as changes are made, it allows you to create new starting-points. When you upload new definitions of resourcetypes and/or relationships, these are merged into the "current" schema, with no mechanism for reverting them. For this reason, I recommend maintaining a version-controlled repository of the schema-files in use.

Version IDs are just timestamps of the date/time when that version was created (not when it was last updated) in Unix epoch time: seconds since midnight, 1970/01/01.

Schema changes also don't retroactively change existing data, as happens with a schema change in a relational database. On one hand, this gives you the reassurance that you won't lose any existing data if you redefine a resourcetype to omit an existing attribute; on the other hand, renaming an existing attribute involves a couple of extra steps of modifying the database's contents.


# POST: upload schema definitions

Use this method to install resourcetype and/or relationship definitions, and to create a new version to work with.

To do this, make a POST request to the URI `/schema/v1/`, with a payload of name "schema" whose value is a JSON-formatted file. The server returns `201/Created` on success.

The contents of that JSON document will be added to the schema, as long as they add new things; any existing resourcetypes, resourcetype-attributes and relationships will not be updated. This is a progressive, succeed-where-possible process, not an atomic operation; any duplicates will be discarded without warning to the client.

The intent of this feature is to enable clients to build on top of the core schema, and to mix-and-match subschemas from different sources, so users working in the same problem domain can share the common elements of their schemas for better interoperability.


To do this via `curl`, for example:

```
curl --data-urlencode schema@webcat.json -X POST http://192.0.2.1:4950/schema/v1/
```

Note that if the file (re)defines an existing resourcetype, Syscat will not create a duplicate or change anything about the existing one, but it will add any _new_ attributes to the existing one.

Similarly, if it defines new source or destination resourcetypes for an existing relationship, these will be added. The `any` type has special behaviour here: if it was defined as the only source resourcetype when the relationship was created, no new types can be added; the same applies to the list of target resourcetypes.


### Create a new schema version

If you add the parameter `create=true` to a POST request to this API endpoint, the server will create a new schema version and pre-populate it with the core schema. This can be done without uploading a new subschema, so can be used to create a fresh start with just the core schema.

You can optionally combine this with the `schema` attribute, to create a new version and immediately apply that set of definitions to it.


# GET - retrieve schema details

You can request three types of information about the schema, via this API:

- Schema versions
    - Find out what versions are present, so you can roll forward or back, or selectively delete versions that you're confident you won't need to roll back to.
- Resourcetype definitions
    - These include the outbound relationships _from_ a given resourcetype.
- Relationship definitions
    - Due to the fact that relationships are defined with *sets* of source and destination resourcetypes, it's often useful to be able to directly examine relationship definitions in their own right.


## Schema versions

You can fetch a list of the schema versions by appending the parameter `version=list` to a GET request to this API.

The return value is a JSON object of this form:

```
{
  "versions": [
    3835709813,
    3835709407,
    3835709370
  ],
  "current-version": 3835709813
}
```


## Resourcetype definitions

The endpoint base URI is `/schema/v1/resourcetypes`.

`GET /schema/v1/resourcetypes` will return a description of all resources.

`GET /schema/v1/resourcetypes/<resourcetype-name>` will return the description of a single resourcetype.

By default, it will return a list of JSON objects. Note that this format is _not_ the same as the format required for uploading a subschema, as it's intended for clients to use in interactions with the server. When requesting a full listing, you can also append the parameter `format=html` - this may be useful for documenting the current schema.

Among the information about each resourcetype, is a list of the relationships that can be created from this type to others. This list combines _three_ sets of permissible relationships:

- _from_ instances of this exact resourcetype, _to_ instances of another specific resourcetype.
- _from_ instances of _any_ other resourcetype, _to_ instances of this one.
- _from_ instances of this resourcetype, _to_ instances of any other.

The latter two are defined by relationships to and from the `any` resourcetype. The combined set is also (and most importantly) consulted when the server receives a request to create a relationship.

For a practical example:

```
$ curl -s http://sctest.onfire.onice/schema/v1/resourcetypes/Pronouns | jq .
{
  "name": "Pronouns",
  "dependent": null,
  "description": "The pronouns by which a person prefers to be addressed.",
  "attributes": [
    {
      "name": "text",
      "type": "varchar",
      "description": "The full, non-URL-safe text of the pronoun set. E.g, They/them.",
      "readonly": null,
      "maxlength": 48,
      "values": null
    }
  ],
  "relationships": null
}
```


## Relationship definitions

This works in a very similar way to the `resourcetypes` endpoint:

`GET /schema/v1/relationships` returns a list of all relationships.

`GET /schema/v1/relationships/<relationship-name>` returns the description of that one relationship.

The return format is always JSON, in a form intended to be useful to front-end UIs and tooling.

For a practical example:

```
$ curl -s http://sctest.onfire.onice/schema/v1/relationships/CONTAINS_IPV4_SUBNETS | jq .
{
  "name": "CONTAINS_IPV4_SUBNETS",
  "source-types": [
    "Ipv4Subnets",
    "VrfGroups",
    "Organisations"
  ],
  "target-types": [
    "Ipv4Subnets"
  ],
  "cardinality": "1:many",
  "reltype": "dependent",
  "description": "That IPv4 subnet has been allocated under this thing."
}
```


# PUT - Switch between schema versions

If there's more than one schema version in the database, you can roll back or forward between them with a PUT request, using the `version=<version number>` argument, e.g:

```
curl -X PUT http://192.0.2.1:4950/schema/v1?version=3835709407
```

Server responses:

- On success: 200/OK
    - It's an idempotent operation, which doesn't create anything new, so 201/Created is not applicable here.
    - "Success" includes cases where the requested version was already the current one.
- If the requested version doesn't exist: 404/Not Found


# DELETE - remove a schema version

To remove a schema version, use an HTTP GET request against the Schema API, with the argument `version=<integer>`. On success, the server will respond with 200/OK.

Example with `curl`:

```
curl -X PUT http://192.0.2.1:4950/schema/v1?version=3835709407
```

If the requested version is the current version, the highest remaining version will be automatically set as the new current version.
