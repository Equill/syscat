# Runtime configuration of the Restagraph server

The server expects its configuration in the form of environment variables, which makes it equally easy to manage via shell scripts, Docker Swarm or Kubernetes (glossing over the issue of secrets management for passwords).

It has defaults for many of them, making them optional unless you either need to change the value or be positively sure about what value it has. Mandatory parameters are marked as such.

The included `docker-compose` files give you examples that you can copy and modify to suit most cases, but it's always good to have a definitive reference.


## Restagraph server

The server itself.

Note that all filepaths need to end in a forward-slash (`/`) to indicate that they're directories.


### `LISTEN_ADDR`

The address on which the server should listen.

- Type: string
- Default: "0.0.0.0"


### `LISTEN_PORT`

The port on which the server should listen.

- Type: integer
- Default: 4950


### `COOKIE_DOMAIN`

The domain used when setting a session cookie, in FQDN format: subdomain.example.com

If `LDAP_BASE_DN` is not specified, it will be derived by turning each element of the cookie domain into a domain component, e.g "dc=subdomain,dc=example,dc=com"

- Type: string
- Default: subdomain.example.com


### `FILES_LOCATION`

Where uploaded files are stored for the long term.

- Type: string
- Default: "/tmp/restagraph-files"


### `FILES_TEMP_LOCATION`

When files are uploaded, they're first written to a temporary location, where Restagraph inspects them to establish their MIME-type, checksum value and so on. After copying them to `FILES_LOCATION`, they're deleted from this directory. This directory can therefore be much smaller.

- Type: string
- Default: "/tmp/restagraph-files-tmp/"


### `SCHEMAPATH`

If the server finds no schema in the database on startup, it will install the core schema, then check for this environment variable.

If this variable is set, and if it contains the path to a directory that exists on the filesystem, the server will attempt to load each file in it with a `.json` suffix as a subschema definition. It processes those files in alphabetical order, to allow for backward references to resourcetypes defined in other subschemas.


### `TEMPLATE_PATH`

Restagraph uses templates to generate a few bits of output, like the schema. This tells it where to find them.

- Type: string
- Default: "/templates/"


### `DEBUG`

Enables debug-level logging, when set to "true".

- Type: string
- Default: disabled.


### `SC_RECANONICALISE`

On startup, systematically recalculate and set the `ScCanonicalpath` attribute on every single resource in the database. The server will not start responding to HTTP requests until after this process is complete.

You *should* only need this if upgrading to 0.15.0 from a previous version.

- Type: string
    - In fact, the server only checks *whether* the variable is present.
- Default: disabled.


### Logs

These go to `STDOUT`, and that is currently hard-coded.

I mention this here because sooner or later somebody's going to come looking for that option. If that's you, please contact the maintainer and let them know we have a real-world use-case for being able to configure it.


## Neo4j

Where the data is stored.

If you're using this reference in combination with one of the `docker-compose` files, remember that this document only describes how to configure the Restagraph server itself. For details about how to configure the Neo4j Docker container, see [the Docker Hub page for Neo4j](https://hub.docker.com/_/neo4j).


### `NEO4J_HOSTNAME`

The hostname or address for connecting to Neo4j.

- Type: string
- Default: "192.0.2.1"


### `NEO4J_PORT`

The port on which Neo4j accepts Bolt connections. Note that the HTTP API is not used by Restagraph, and we can't (yet) use Bolt over TLS.

- Type: integer
- Default: 7687


### `BOLT_AUTH_SCHEME`

Identifies which authentication scheme to use when connecting to Neo4j. Only relevant if you've actually gone and set up Neo4j with unauthenticated access.

Normally, you don't need to bother setting this.

- Type: string
- Valid options: "none" or "basic"
    - If "none" is specified, only the username is relevant, and the default is almost certainly correct.
    - If "basic" is specified, the default username is still probably correct, but you do want to specify a password.
- Default: "basic"


### `NEO4J_USER`

The username to use when connecting to Neo4j.

- Type: string
- Default: "neo4j"



### `NEO4J_PASSWORD_FILE` (mandatory unless `NEO4J_PASSWORD` is supplied)

The password to use when connecting to Neo4j.

This is for using `docker secrets` to supply the password, in which case it needs to be read from a file in `/run/secrets`. This environment variable just gives the path to that file.

If this variable is set, it takes precedence over `NEO4J_PASSWORD`, to ensure that the more secure option is set.

- Type: string


### `NEO4J_PASSWORD` (mandatory unless `NEO4J_PASSWORD_FILE` is supplied)

The password to use when connecting to Neo4j.

This works fine in a development environment, but for production you really want to use `docker secrets` or some equivalent, because an attacker with access to a Swarm management node can use `docker inspect` to read the environment variables for this stack.

- Type: string


## Access-control

This entire section can be skipped if you want the "open" security policy that enables unauthenticated access to everything. If you're just running a personal instance, or evaluating the rest of what Restagraph can do, ignore this entire section - Restagraph will default to "open".

There are two backend options for authentication and session-management, with trade-offs to consider:

- LDAP for authentication, and Redis for session-management.
    - Tested with OpenLDAP, but should theoretically work with ActiveDirectory as well, because they speak the same protocol from this angle.
    - LDAP enables strong security policies that prevent one subdomain messing with another's authentication data, but is more of a pain to configure.
    - Weaker support for password-hashing functions.
- Neo4j for both.
    - Much simpler to set up, but can't provide an additional layer of access-control to stop subdomains editing each other's auth data.
    - Supports PBKDF for password-hashing, and potentially even stronger functions in the future.

Note that it shouldn't be possible for one domain to interfere with the auth data of another. Access-controls in the backend just provide an additional layer of defence.

Your choice determines whether you need to set the LDAP and Redis variables, the `AUTH_NEO4J` ones, or none of them.


### `ACCESS_POLICY`

Selects one of the predefined access policies.

- Type: string
- Acceptable values:
    - "open" means no authentication is required; anybody can make any request. Convenient for personal instances, and for exploring server functionality.
    - "readonly" means that only GET requests are permitted. Mostly useful for keeping the site's information available to users while you investigate some unexpected change.
    - "write-authenticated" means that anybody can make a GET request, but only authenticated users can make POST, PUT and DELETE requests, and that the user's UID is recorded as the creator of anything they add.
    - "authenticated-only" means that all requests must be from an authenticated user. All non-authenticated requests will be rejected.
- default: "open"


### `LDAP_HOST`

The address for connecting to the LDAP server.

- Type: string
- Default: "192.0.2.1"


### `LDAP_PORT`

The port on which the LDAP server is listening. Unfortunately, LDAP/TLS is not (yet) supported.

- Type: integer
- Default: 389


### `LDAP_BASE_DN`

The base or root DN of the LDAP server to which we're authenticating.

If it isn't specified, it will be derived by turning each element of the cookie domain into a domain component, e.g "dc=subdomain,dc=example,dc=com".

- Type: string
- Default: "dc=subdomain,dc=example,dc=com"


### `REDIS_HOST`

The address for connecting to the Redis key-value store, which manages sessions.

- Type: string
- Default: "192.0.2.1"


### `REDIS_PORT`

The port on which Redis is accepting connections.

- Type: integer
- Default: 6379


### `AUTH_NEO4J_HOSTNAME` (mandatory for Neo4j auth-management)

The address for connecting to the _authentication_ Neo4j server.

- Type: string


### `AUTH_NEO4J_PORT`

The port on which the _authentication_ Neo4j server is listening.

- Type: integer
- Default: 7687


### `AUTH_NEO4J_USER`

You really only need to override this if you're using Neo4j Enterprise Edition and have created a separate username.

- Type: string
- Default: "neo4j"


### `AUTH_NEO4J_PASSWORD` (mandatory for Neo4j auth-management unless `AUTH_NEO4J_PASSWORD_FILE` is set)

- Type: string


### `AUTH_NEO4J_PASSWORD_FILE`

Enables integration with `docker secrets` when using Docker in swarm mode.

- Type: string


### `AUTH_SCADMIN_PASSWORD`

Sets the initial password for the `ScAdmin` user.

- Type: string
- Default: "thisisabadpassword"


### `AUTH_SCADMIN_PASSWORD_FILE`

Sets the initial password for the `ScAdmin` user. If set, takes precedence over `AUTH_SCADMIN_PASSWORD`.

- Type: string
